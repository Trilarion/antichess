# AntiChess Manual

Antichess is a variant of chess in which the goal is to either lose all of your pieces (except your king) or force your
opponent to checkmate you.

AntiChess is a stand-alone application that provides a Graphical User Interface, displaying a chessboard and pieces
with which the user plays the game. The user can play against the computer in a timed game by making moves. In addition,
the user can load and save games in order to later continue with the game.

## User Interface

Under the game menu there are several options.

- New Game: Start a new game using the “new game” command. Under “new game”, choose either to play a 2-player or
  4-player Antichess game.
- Save: Save a game in its current state using the save command.
- Load: Load a game that was already saved using the load command.
- Options: Change the options.
- Exit Game: Exit the game.

Note: Because we have not defined a game file format for 4-player game board, currently the program cannot save or load a
4-player game. // TODO where is the file format defined, can we simply define one for all kind of players

### New Game Dialog

Select the type of game, the piece color, the time limits for the game and the style of the game (if playing against
AntiChess).

Antichess can be set to search moves up to 10 different levels. The greater the search depth, the harder is the game
for the human player because the AI makes better moves. The style box is disabled if the game is a human-human game.

The user can choose to play on a normal game or a cylindrical board.

Once the user hits ok, the game board is displayed with the pieces arranged according to normal chess convention.

![2 Player new game dialog](2PlayerMenu322x362_high.jpg)
![4 Player new game dialog](4playersettings.gif)
![2 Player new game](2PlayerGame580x480_high.jpg)
![4 Player new game](4PlayerGame573x480_high.jpg)
![4 Player game in progress](4playergameinprogress.gif)

### Options

- Highlight valid moves. Every time the user made a move to a valid square the square would get highlighted. 
- Show Valid Moves : Given any piece being moved, the board will show the player all the valid moves that can be made
  with that piece. All the squares that he can move to will have green circles show up in their middle.
- Showing Movable Pieces: During the course of the game, as each player is deciding his move makes his move,
  he/she can click on any of the pieces to find out which pieces can be moved. Red Circles show up in the squares where
  the movable pieces are.
- Clock : displays the clocks that keep track of the time for both users. 
- Display Time : displays the exact time at which the user made the move. This time is printed with each of the moves made.  
- Set Time : Sets the time of the game. If the user enters 3 minutes for both sides, the game will be over in 6 minutes.
- Beep : Every time a player makes a move, a beep sound is produced.

### Help

The help menu has a comprehensive overview of the game and its rules. It explains the functionality and provides answers
to the most commonly asked questions. It also explains the most common strategies used.

## Game Play

Playing the game by “drag-and -drop” pieces around as long as the move is valid. If an invalid move is tried to make,
the piece will remain on the initial square. To make a valid move the user has to be aware of the rules of the game.
The board shows the time remaining for each player as well as the moves made so far. The current player to make to move
will have his name highlighted. The game keeps track of the user's timers. It displays the time remaining for each of
the users. It also displays all the moves that have been made.

### Undo

The user has the option to undo the moves using the “Undo move” command.

### End Game

Once the playing time equals the duration that the users set for the game is reached, the game is over. A dialog box
saying “Black (or other colors) has won the game” appears. In case the game was finished in some other way (draw) an
appropriate message will be displayed for that as well. Once the game is over, the user cannot make any more moves in
that game. He can start a new game or exit the program completely.

## Game Rules

### 2-Player

- "White" moves first. The players alternate in making one move at a time until the game is completed.
- A move is the transfer by a player of one of his pieces from one square to another square,
  which is either vacant or occupied by an opponent's piece.
- No piece except the knight may cross a square occupied by another piece. That is, only the knight may jump over other
  pieces.
- A piece played to a square occupied by an opponent's piece captures that piece as part of the same move.
  The captured piece is immediately removed from the board.
- All the pieces move exactly as they do in standard chess. An excellent description of how each piece moves and
  captures is at http://www.princeton.edu/~jedwards/cif/intro.html

Additional rules for Antichess:

- A player is forced to capture an opponent's piece whenever possible.
- If several of the opponent's pieces can be taken, the player is free to choose which piece to take.
  See the exceptions to this rule.
- The special moves of castling and en passant are also allowed.
- When a pawn moves to the last row on the opposing side, it turns into a queen. (In standard chess, such a pawn can be turned into any piece of the player's choice.)
- Once per game, each player may switch the location of two of its pieces. This switch is considered a move.
  _The switch is subject to the same rules as all the other moves, e.g. you may not switch into a check.
  Each player is only allowed to perform this move once per game._

### 4-Player

The rules for 4-player is very similar to 2-player except for the following changes:

- Switch moves are not allowed
- Castling moves are not allowed
- Enpassant is not allowed

## Development

Each Game consists of a Board of Squares and Pieces, which are the basic objects in our system. A Player can make a Move,
indicating the starting and destination Squares. The user (HumanPlayer) will be able to play against a MachinePlayer.
The MachinePlayer plays according to a game strategy that performs a search using an Evaluator and using Moves generated
from Piece. The function of the evaluator is to favor a board position over others. The Timer keeps track of how much
time each Player uses to make a Move. A GameFile stores a saved game which can be retrieved later to resume the game.

### Domains

- Board: 8x8-square board on which the game is played (larger for 4 player games with different boundary conditions)
- Piece: Colored chess piece that can be one of the following: King, Queen, Bishop, Knight, Rook, Pawn
- Square: Two characters representing a particular square on the board, identified by a column letter (“a” to “h”) and
  a row number (1 to 8)
- Move: Pair consisting of start and end squares, representing the start and destination squares of a piece that is
  moved.
- UI: Agent responsible for displaying the chessboard and accepting inputs from the user
- GameFile: Saved game file
- Rule: Rule that specifies legal moves
- Game: An Antichess game
- Player: A player
  + HumanPlayer: Human player
  + MachinePlayer: Computer player
- Strategy: Search algorithm
- Evaluator: Function that rates how favorable a given board position is
- Timer: Timer to keep track of how much time a player has left in the game

### Relations

- game: An Antichess game
- file: Saved game file of a game
- players: Players of a game
- board: The board the game is being played on
- move: Move a player makes
- timer: Timer for a player
- strategy: Game strategy used by a MachinePlayer
- evaluator: Static evaluator used by Strategy and Creator
- creates: Moves a Creator produces
- rules: Game rules
- start: Starting square of a move
- destination: Destination square of a move
- pieces: Pieces on a board
- squares: Squares on a board
- location: Position of a piece on the board
- content: Occupant of a square 

### Problem Object Model

[Problen Object Model](problemObjectModel.pdf)
[Module Dependency Diagram](MDD.jpg)

### Overview

**Piece** has information about particular pieces, such as the color (player), type and location. In addition, it contains all
the information about the rules of the game, how the pieces will be moved and captured. Piece will be an abstract class,
containing the general rules that apply to all pieces. Subclasses of Piece will be a specific piece with information
unique to it, including a direction vector that allows methods in Piece to generate possible moves for a given piece.
Subclasses that have unique moves such as King and Pawn will override methods in Piece. In this way, we can always add
a new piece without affecting other pieces, and Piece will be implemented in a way that is most general for most of the
pieces.

// TODO is this the best way? maybe rules should be separated more from Pieces (after all rules changes between chess and
antichess and 2-player and 4-player chess), maybe it depends on the rules (movement rules might be okay)

**Board** maintains information about the topology of the chessboard and keeps track of a collection of pieces and their
locations. Board has absolutely no information about the rules of the game, or how the pieces move. It only acts as an
accounting book for the game. It has a weak dependency on Piece, as it does not perform any operations on Piece, but it
can return a reference to a piece. Every Piece will have a Board field pointing to the board it is on, so that it can
ask for other Pieces and calculate what moves it can make.

// TODO if we make Piece less potent, we could concentrate the valid moves somewhere else (they aren't Piece specific)

The UI will make use of a **GameController**, which will handle the game play of Antichess. GameController keeps track
of a current Board position use two Timers, one for each player. GameController will also be able to load and save game.

Each Player can be either a HumanPlayer or a MachinePlayer, and they will make Moves during the game, and will be timed by
the Timer. The MachinePlayer will play according to an Engine (a search engine) and an Evaluator to determine a
favorable move. The Piece will have information for checking whether Moves entered are valid. The GameController
will validate the Moves by either players before updating the Board (?). The Piece will have the necessary rules and
information to update the Board given a Move.

### Piece and Board

Piece contains all information about its location, color (player), and type. It also contains all the rules of the game.
It is responsible for performing the validating of moves, and the generation of valid moves. Since we have decided
to make Piece abstract, the subclasses King, Queen, Bishop, Knight, Rook and Pawn will have to be instantiated. The Piece
class itself will have the most general implementation, for example, generation of Switching moves, and generation of
moves using a direction vector.

The subclasses will then have their own direction vector (?) so that each piece has its own way of moving, and if the
direction vector is inadequate, the particular piece simply has to override methods from Piece and perform its own
validation and generation of moves.

In Piece, the method *getNormalMoves()* finds a list of capture moves and a list of non-capture moves by using the
direction vector to ray-trace the possible movements in various directions.
// TODO why differentiating in capturing and non-capturing moves?

Pawn will override *getNormalMoves()* and find its own list of possible capture and non-capture moves, including enpassant
and queen promotion. The Move objects that it generates will contain information about the move. When the move is
applied later, Pawn will handle its own move making, by overriding the makeMove() method of Piece. In this way, it
can make its own unique moves without affecting other pieces.
// TODO that sounds overly complicated, it looks as if Move then calls Piece.makeMove again, what is Move doing then?

Similarly, King will also override getNormalMoves(). It will first call the getNormalMoves() in the Piece parent class
to get the list of capture and non-capture moves. Then it will see if castling is possible, and if yes, add the
castling moves to the list of non-capture moves. Again, the Move objects will contain the necessary information for
castling so that King will override the makeMove() method in Piece and apply castling. In this way, King can make its
unique moves without affecting other pieces.

Furthermore, each Piece has a reference to a Board. The Board is weakly dependent on Piece, as it only maintains the
positional information of the pieces. The Piece, when it is checking or generating a move, will depend strongly on the
Board to get information about other Pieces, especially Pieces that it can capture or switch with.

The topology of the board may not be fixed over time (?). Our board is initialized with an array representing the topology
of the actual game board. We can easily have a larger board, and even board with holes and irregular shapes. We just
have to pass in a rectangular array of 0s and 1s representing the invalid and valid squares respectively.
This additional feature would allow us to cope with any changes in the properties of the board to be used in the game.

### Moves

Moves are simply two locations: the starting location and the destination location. Move will be the main medium
through which players interact with the program and update the Board. When a player enters a Move, we will use Piece
to validate the Move, and then update the Board accordingly. The MachinePlayer will be able to call a method in Piece
to get a list of valid Moves so that it can perform a search through them and make a legal move.

// TODO it's tempting to do more with Move and actually have capturing or creation of pieces on Board, idea would be that
moves are decoupled from the rules (once they are verified)

When a Move is made, the Move object will be updated with information about changes that it made to the board.
This would allow us to undo a Move that was applied. This is especially useful when the computer player is doing a
search, so that it can apply moves without having to keep a copy of the board at each level, thus reducing extraneous
copying and processing.

// TODO Is this the best way? Aren't copies stored nonetheless? Moves should be more simple and should allow being undone
but boards need to be stored anyway, I think.

### Validation of legal moves

To separate out the details of validating legal moves from the rest of the game, we have decided to have the Piece
class contain all the necessary information to perform checks for legality of moves, and to give the possible moves
that can be made by a piece or a player on a given board. Within the Piece, we will have a general rule set that
applies to all pieces. This includes the implementation of following: switching moves, and generating general moves
given a direction vector.

// TODO would rather separate validity from pieces

The most general rule for the pieces is that each of them has a direction vector indicating the different possible
directions it can make from a given square. For example, the Rook moves in a horizontal and vertical manner,
a Bishop diagonal, a Queen and King in all 8 directions, and a Knight in 8 L-shaped directions. All these can be
represented using direction vectors. In addition, the King and Knight can only move one step, whereas Queen, Rook
and Bishop can move as far in a direction as long as they are not blocked. We have another field called “Slide” to
indicate this property. Capture moves for these pieces are simply destination squares that contain opponent pieces
and are reachable from the current position of these pieces.

// TODO that may be the wrong abstraction, for example pawns move and capture different, maybe that should be part of piece, getting valid moves

Since the Pawn moves and captures in a very different way, we have decided to implement the move checking and
generation separately within a subclass.

// TODO which subclass? where? in piece?

The King has to be able to check if it is attacked by any other pieces to see if a side is in check. To perform this,
the King simply has to access the board to get all the opponent pieces, and to ask each of them if they can attack the
current location the King is at.

// TODO maybe this could be done differently, i.e. when a move is made a check test could be made

We could have a formal
language to describe rules, and have them stored in files. The program will then only have to parse these files to
obtain the rules. Modifications only need to be made to these rule files. Although this would be extremely flexible
and extensible, the difficulty lies with coming up with a good convenient notation.

### Design of search algorithm

Machine player will depend on Engine to choose an appropriate move among many candidates. Furthermore, Engine will
depend on Piece and Evaluator to create legal moves and arrive at new board positions. Evaluator will be called at
these board positions to return an associated value. Since we wanted that our Machine player to be able to think
when it was not his turn, we decided to make the Engine with non-static methods for simplicity, because we will
create a new machine player to think when it is our opponent's turn.

// TODO not clear the Engine must be non-static?

While preliminary design included only a basic mini-max algorithm with no Alpha-Beta pruning, the final version of
the search engine and evaluator use many heuristics and optimizations. Because performance is such an important design
force for any search algorithm, we thought that trading some design flexibility for performance was worth. To minimize
the number of objects to be created, we preferred methods calls over recursively calling object creating code for our
Alpha-Beta algorithm.

Since Board can mutate through move and undo-move method calls, we are creating significantly less number of objects
compared to a naïve search engine that copies board positions before searching each node. Only immutable move objects
are being created, as opposed to creating a single board object for each possible move on a given board position.
With move and undoMove method calls, we can use a single board position to visit the whole search tree.

In addition to minimizing object creation during search, the board is using many techniques to improve Alpha-Beta
efficiency. These techniques include board hashing with Zobrist keys, quiescent search, and principal variation search.
The following is a discussion of these techniques taken from chess programming literature.

### Transposition Table and Zobrist Keys

Firstly, we are using a transposition table that hashed board positions using Zobrist keys. This table is used to not
having to evaluate a board if we have evaluated it before to a depth that is less than our current required depth.
Furthermore, even if our current required search depth is more than the old search depth, we can use old search values
to improve move ordering on the Alpha-Beta move list to improve cut-off performance. We swap the old best move to be
the first move to be considered.

Zobrist keys identify a board position, however this does not have to be unique, because we make sure that the move
returned by the hashed board is actually contained among the valid moves in the board position. Zobrist keys could
collide depending on enpassant, castling, or swap information, but even in case of a collision it is very likely that
old best move is among the best moves in the new position so it is reasonable to place that move to the beginning of
the Alpha-Beta move list. We use a “always replace” hash scheme when we encounter the same position again with a
different depth. A “replace if deeper or same depth” scheme might be better, but we assumed that this second scheme
would automatically be the case is a game where on each new board position our required depth is actually larger than
the old depth.

### Implementation of Zobrist Keys

We start by making a multi-dimensional array of 64-bit elements, each of which contains a random number. We create a
64-bit random number using Random.getLong() method that is already implemented in the Java API. The table has three
dimensions: type of piece, color of piece, and location of piece. On the program startup, this array is filled with
random numbers. To create a zobrist key for a position, we set our key to zero, then go find every piece on the board,
and XOR (via the "^" operator) "zobrist[pc][co][sq]" into our key. If the position is white to move, we leave it alone,
but if it's black to move, we XOR a 64-bit constant random number into the key.

This Zobrist key technique creates keys that are NOT related to the position being keyed. If a single piece or pawn is
moved, you get a key that is completely different, so these keys do not tend to clump up or collide frequently.
They are ideal for use as hash keys.

Another nice thing is that we can manage these keys incrementally. If, for example, we have a white pawn on e5,
the key has had "zobrist[PAWN][WHITE][E5]" XOR'd into it. If we XOR this value into the key again, due to the way
that XOR works, the pawn is deleted from the key.

What this means is that if we have a key for the current position, and want to move a white pawn from e5 to e6,
you XOR in the "white pawn on e5" key, which removes the pawn from e5, and XOR in the "white pawn on e6" key,
which puts a white pawn on e6. We are guaranteed to get the same key that you would get if we started over and
XOR'd all of the keys for all of the pieces together.

The utility of this is that you can create a zobrist key at the root of the search, and keep it current throughout the
course of the search by updating it in board. We have a mutable zobrist key that updates when board changes.

### Quiescent search

Antichess contains many forcing tactical sequences. If opponent takes your bishop, and you have the opportunity to
capture it, then it is obvious that you should capture it. Alpha-beta search is not particularly tolerant of this
kind of nuance. If a depth parameter is passed to the function, when the depth gets to zero, it is done, even if
there is forced capture move ahead.

A method of dealing with this is called "quiescent search". When alpha-beta runs out of depth, rather than calling
"Evaluate()", a quiescent search function is called instead. This function evaluates the position, while being careful
to avoid overlooking extremely obvious tactical conditions.

Essentially, a quiescent search is an evaluation function that takes into account some dynamic possibilities.
Our implementation of quiescent search searches only captures. Unlike chess, where captures are not compulsory,
this method turns out to be very useful in Antichess.

If it were possible to make a more accurate quiescent search with no loss in speed, our program would be stronger than
it was before. However, there is a tradeoff being made if we try to make our quiescent search powerful at the expense
of time. If making the quiescent search smarter costs us some number of plies of full-width search, it may not be worth
it to make it smarter. Our current implementation only increases depth by one up until it reaches a board position with
no captures.

### Principal Variation Search (PVS)

PVS is a small performance improvement out of the alpha-beta algorithm. Any node in an alpha-beta search belongs to
one of three types:

- Alpha node. Every move we search will have a value less than or equal to alpha, meaning that none of the moves in
  here will be any good, probably because the starting position is bad for the side to move.
- Beta node. At least one of the moves will return a score greater than or equal to beta.
- Principal variation (PV) node. One or more of the moves will return a score greater than alpha (a PV move),
  but none will return a score greater than or equal to beta.

Sometimes we can figure out what kind of node we are dealing with early on. If the first move we search fails high
(returns a score greater than or equal to beta), we have clearly got a beta node. If the first move fails low
(returns a score less than or equal to alpha), assuming that our move ordering is pretty good, we probably have
an alpha node. If the first move returns a score between alpha and beta, we probably have a PV node.

Of course, we could be wrong in two of the cases. Principal Variation Search makes the assumption that if we find a
PV move when you are searching a node, we have a PV node. It assumes that our move ordering will be good enough that
we won't find a better PV move, or a fail-high move (which would cause this to become a beta node), amongst the other
moves.

Once we have found a move with a score that is between alpha and beta, the rest of the moves are searched with the goal
of proving that they are all bad. It is possible to do this a bit faster than a search that worries that one of the
remaining moves might be good. If the algorithm finds out that it was wrong, and that one of the subsequent moves was
better than the first PV move, it has to search again, in in the normal alpha-beta manner. This variation happens
sometimes, and it is a waste of time, but in chess literature we have read, this issue was not often enough to
counteract the savings gained from doing the "bad move proof" search referred to earlier.

### User Input / Output

How a move is made, and how output is displayed.

Antichess has a GUI and a TextUI that the user can use to play the game. After looking at the GUI's for various chess
programs we made a note of the minimal functionality our GUI should have and brainstormed about the optional features
we might add later on . We followed the usability heuristics while designing the GUI especially the “Less is More”
heuristic. The preliminary version of the GUI looks like as shown in the figure 2.

There is a menu bar on at the top of the GUI. According to Fitt's Law this minimizes the distance the user has to cover
to get to it. There are 2 options on the menus on the menu bar; game and help . The Game menu provides the user several
options. First of all he can start a new game using the “start game” command. He can save a game in its current state
using the save command. He can also load a game that he has already saved using the load command. There is an option
to exit the game.

Once he starts playing the game he can just “drag-and -drop” his pieces around as long as the move is valid. If he
tires to make an invalid move however, he will not be able to do so. His piece will remain on the initial square.

There will be two modes for moving the pieces around. The above mentioned is one of the modes. In the other mode the
user will click on the square that his piece is on, click on the square that he wants to move to and piece would just
be moved to the desired position as long as the move is valid.

The user can customize the game by choosing the “options” command. Clicking on the options command will bring up a
dialog box that displays a number of options and the user can select one or more of those.

We initially considered making an options menu that would display all the possible options as menu items but there is
a drawback to this approach. The user can only select one option at a time. If he wants to select multiple options he
would have to go to back to the menu each time. Our approach gets rid of this issue.

Besides the options mentioned in the User's manual, there also some other options that we might add later.

These include:

- Reverse board: Reverses the sides of the board.
- Show AI: displays the depth to which the search tree was searched and the number of moves considered.

To minimize the memory load for the user, the game will display all the moves that have been made. It will also display
the pieces that have been captured for each of the players.

Antichess is played with time restrictions. The game will keep track of the user's timers and display the time remaining
for each of the users. Each player is allocated a fixed amount of time T to make all the moves, e.g., 5 minutes.
The default time is set to 5 minutes but the user has the option to change it. The clock for each player is set
to T minutes at the start of the game. If it is player A's turn to move, A's clock counts down until the move is made.
While it is B's turn to move, A's clock is suspended, and B's clock runs down. If a player's clock runs down to zero
while the game is in progress, the player loses. This ensures that the game is over in less than 2*T minutes, because
by then at least one of the clocks has run down to zero.

Once this happens the game is over and an appropriate message is displayed.

### Timer

The Timer will be controlled by GameController. Timer will have an initial value. Each player will have a Timer to keep
track of his time left. Once the turn for a player starts, the GameController will start the Timer for that player.
Once the turn ends, GameController will end the timer. When the turn switches to the other player, GameController will
perform the same for the other Player. If the Timer hits zero, GameController will declare that the player to whom that
Timer belongs has lost. Timer will essentially take note of the system time once it starts, and then update the time
remaining periodically.

### Evaluator

One of the critical features of our program would be the Evaluator class. Evaluator assigns a particular value to a
board position. This assignment should not look ahead one more depth and get additional costly move information,
rather it should use the current positioning of the pieces on the board to come up with a quick weighted sum value.
The final evaluator can be called at three different qualities.

The first one is most sophisticated and weights Pieces according to their positions on the board. Each piece uses a
different matrix for weighting its position on the board. Furthermore, pawns get isolated pawn, double pawn, and
passed pawn penalty heuristics through extra calculations. Rook values also take into account whether they are on
open files(columns with no pawns) or whether they are on a semi-open file (columns with only enemy pawns). Additional
penalties are added for these cases to rook values. Bishop, knight, and queen pieces are also weighted depending on
their board position. King is neither weighted nor uses any king security heuristics because in Antichess getting
checkmated requires much extra help from our opponent that we did not think was worth putting into our evaluator as
a heuristic. Since the special move can be especially useful in late game Antichess situations, saving it is added as a
bonus on this first evaluator quality.

The reason for emphasis on pawn heuristic is that they are harder to get rid off than other pieces due to their
limited movement. It might be better to get to positions where opponents pawns are isolated so they can be blocked
by our own pieces. Similarly, double pawns of same color on a single column block each other. In these positions,
the movement of these pawns will be further limited and they are less likely to be captured. Finally, if a pawn has
no other enemy pawns that stop him from advancing to queen, there is a danger that it might easily advance to queen.
This possibility is added as a passed pawn penalty heuristic.

The reason for rook heuristics is because rooks might easily be forced to capture a sequence of enemy pieces if their
mobility increases. A similar heuristic for queen was not added because queen's mobility is so high that it can easily
choose to capture a piece that would force opponent to capture the queen in the next turn.

The square weights of each piece assigned higher values if the pieces were on the border of the board because their
mobility was decreased. A decreased mobility implies less opportunity for capture. These added heuristics values are
modular and can easily be changed to implement different styles of play.

The second option of quality uses a weighted sum of pieces where the number of pieces are retrieved from the board and
multiplied by values set by the evaluator itself. No heuristics are applied.

The third evaluator option uses a method from board class that gets the relative difference of pieces on the board.
We wanted to compare whether option two and three had any performance tradeoffs.

The cylindrical board evaluator uses almost the same functions except that the pieces get different weights depending
on their board position because there is to horizontal end to the board.

We are confident that our first quality evaluation returns a strong understanding of the position because of the extra
heuristics. Second and third quality evaluations might increase the depth of search but first level evaluation seems to
beat these two basic evaluators even though they might search deeper.

## Alternatives Considered and Improvements over Preliminary Design

### Board representation: choice of Square or Piece

Our initial design considered use of a Board consisting of 64 Square objects. Each Square will support a getIsPiece
method that will enable the calling code to determine if a piece is held at that Square. If the Square is a piece,
then the calling code can also use methods like getType and getColor to obtain information about the piece.

We thought that this representation of a board will be more efficient when we are generating possible moves for a
particular piece. We can ask the Board whether a square is empty or not, and decide if a piece can move into that
square or capture an opponent piece over there.

However, this means that the Square object will be holding extraneous information even if it is not holding a piece.
Moreover, the Square object is to be treated as if it were a piece since we are to get information about a piece from
the Square directly.

We decided that we should have a Piece object instead of a Square object. The Board can still maintain an array of
Piece objects for each possible location, with each element representing a possible square on the board. A null in
the array would indicate that the position is empty. Otherwise a Piece object will be contained in that array position.
So the Board will have either an 8x8 2-dimension array of a 64-sized array of Piece, and each position will either
point to a Piece object or be null if it is empty.

In this way, we eliminated the need for a Square object which holds extraneous information even when it is not a piece.

### Validation and generation of moves

Our initial design considered the use of a separate Validator to check if a move is valid and to generate possible
moves for a particular piece. Thus the Validator will have a set of PieceRuleSet, one for each piece. When the
Validator is called, it will be given a Board and the position of the piece that is to be moved. The Validator
then dispatches the task of move generation to the corresponding PieceRuleSet.

As we have decided to have a Piece class, as discussed in the previous section, the use of a separate Validator seems
to be complicating the design of the system. We are separating each PieceRuleSet from its corresponding Piece, and
the Validator still has to accept as an argument what Piece to generate moves for, and then use the correct
PieceRuleSet. Ideally, a Piece should contain all information related to the Piece, including the rules governing what
moves it can make.

Hence we have decided to remove the Validator object altogether. Instead, all the rules will be held in the Piece
objects. Rules specific to different pieces will be held in the subclasses of Piece. Piece will just contain the
general move generation and checking methods applicable to all piece types, and subclasses will be able to override
these methods to implement special moves or perform additional checking.

This change eliminates use of extra classes to store information that should have been included in a particular class.

### Checking if the King in check

Our first implementation of the code that checks if a King is in check assumes knowledge of what moves other pieces
can make, and then backtrack from the King’s position to see if there are any opponent pieces in squares from which
they can attack the King’s current position. For example, if the King is in a particular location, it will check the
surrounding 4 horizontal and vertical squares to see if a Rook or Queen is there, and the surrounding 4 diagonal
squares to see if a Bishop or Queen is there, and then check for further squares in these directions.

This design is quite inflexible. Suppose the rules for how a Queen moves is changed. Then not only do we have to
change the implementation for Queen, we now also have to change the implementation of King.

We have decided that the King should not assume any knowledge of how other pieces move. Instead, if we want to know
if a King is in check, what we should do is to ask all opponent pieces if they can attack the location the King is
currently positioned at. In this way, the King does not need to know anything about how other pieces move. If the
rules of another piece change, we just have to change the implementation for that piece, and the implementation of
the King needs not be changed at all.

This change allows the design to be more flexible and increases modularity.

### Generating Move or Board for search algorithm

Our initial decision for the search algorithm was to generate possible board positions from a given board, and then
search through them. In doing so, we would be creating many boards as we deepen into each level of the search.
Since the Board object holds a collection of pieces, creating new Board objects at each branch of the search would be
very costly.

Instead of generating possible board positions, we have decided to generate Move objects. A Move simply contains
information of the start and destination of a move. Throughout our search, we will simply have a mutable board.
When we search down a branch of a particular move, we just apply the move to the board and proceed to the next level.
When we backtrack, we will just undo the move to restore the original state of the board.

Since Move contains only the essential information, we expect creation of Move objects to be more efficient then
creation of Board objects. Although we have to apply a move and be able to undo a move, such operations will
modify at most 4 locations on the board. If we were to generate new boards, we would have to copy over all the
locations and pieces on the board, and doing so has much more overheads and redundancies.

This change would improve performance without affecting the modularity and flexibility of our design.

### Undo Move

Previously, Board and Piece classes both have intrinsic and extrinsic fields. Intrinsic fields do not change after
initialization, and extrinsic fields get updated through out the game. An example to an intrinsic field for Board
is the topology field. On the other hand, an extrinsic field, the locations of the pieces get changed as the game
proceeds.

In order to provide functionality to undo a move, we realized that we have to keep a copy of fields that it modifies
within Board and Pieces whenever a Move is applied to the board. Otherwise, it is not possible to revert back the move.

The most naïve way is to keep copies of Board and Pieces at every search level and once the search is completed from
the lower levels, we can revert back to the copies. However, our design of the Move class will have enough information
in it, such that we can modify the Board back to what it was before the search started.

### 4-Player Antichess Game

In order to implement the 4-player Antichess game, we need not change our design at all.

Our Board supports variable topology, so we just create a new 14x14 topology array with four 3x3 invalid squares at
each corner, and create a Board with this topology.

In Piece, we will check the board type, and if the Board is a 4-player board, we will disable Castling, Switching,
and En Passant. We also have to extend Pawn to handle horizontal Pawns in addition to the original vertical Pawns.

We also improved the UIs to allow the user to choose if he wants to play a 2-player or a 4-player game, and to display
the appropriate board.

GameController now just need to keep track of the number of players and loop four players instead of the original two
through the player in the appropriate order.

## Design Changes

This sections describes the changes we made to our design after the amendments were issued.

### Cylindrical Board

To support the cylindrical board, we only had to make minimal changes to our Board and Piece.

We had to add in a field “BoardType” that keeps track of whether the Board is a regular or a cylindrical board.

For the Piece, we simply have to change a few lines of code for the methods responsible for the generation of moves.
Previously, when we iterated through possible locations in various directions from a particular piece, we checked if
a desired location is out of bounds before determining if a move to that location is possible. Now, when we are
generating possible moves for a cylindrical, if a desired location is out of bounds column-wise, we simply wrap
around to the other side of the board.

Because of the modularity of our design, the only classes that we need to modify are Board, Piece. In addition,
we modified subclasses of Piece: King and Pawn, as they override the move generation methods to generate their
own special moves.

## Test strategy

We have decided to use primarily blackbox and integration testing to test our Antichess program. Testing individual
modules through glass-box testing would be very time-consuming and probably unnecessary if all the rules of the game
were properly tested during the final integration tests. Glassbox testing seemed redundant at this stage of
development since none of our code is optimized where it would behave differently for different number or
combinations of pieces on board. To this end, we thought that putting most effort on doing a single, but
throughout end-to-end integration check of all Antichess rules would provide the most benefit out of testing.

### Unit Testing

Initially, we tested the fundamental components of our design, Piece, Board, and Move classes, separate from other
modules. The basic observer and mutator methods in those classes, such as get and set methods were tested using Junit
test drivers apart from the rules of the game through run-time assertions. A particular bug in copy constructor for
a board escaped these tests to be found later during integration testing Engine class.

### Integration Testing

After the unit testing of these two modules, we tested the GameController class. It was only up after the implementation
and testing of the GameController through Junit that we were able to introduce test cases for the particular rules of
the game. We decided on a particular game file format to enter our test cases as a board that was initialized at the
GameController. We extended the given game file format to include information needed by our test drivers. At this
entered board position, we started the rules of the game by entering a move and testing validity information.

We believe that these integration tests performed clearly analyze board move creation process such that all
generated moves on a board position are not only valid, but complete.

We later included these integration tests as a system test that would run during each regression testing. The final
version of the code generated no errors on move creation.

### Testing of Validation of Moves

We have tested for movement of all pieces, including black and white, without any forced capture moves. Testing
included impossible moves that would go outside of the board and moves where a piece was blocked another piece,
opponent of self, such that it could not reach the final board position. We tested jumping moves of surrounded
knights, long diagonal bishops and queen moves, as well as rook moves. We also tested pawns for initial two square
or anytime 1 square push.

### Testing of King Moves and King in Check

An explicit number of test cases included testing king in check information, whether a capture was a forced move
even if king was under check. We tested two kings standing next to each other would be a invalid board position.
King under pin was especially tested such that the pinned piece should not move such that it would reveal the king
under check. We checked for long and short castling separately and made sure that any of the squares the king passes
from its start to final destination were not under check. Castling moves where the king or the corresponding rook
had already moved were considered illegal.

### Testing of Pawn Moves

Enpasssant moves for pawns were tested for normal capture initially. One tricky test case was that a swap move from
7th so 5th rank between two pawns should not have resulted in an enpassant move.

Promotion moves of pawns including any capture moves were also tested. We have especially tested for swapped pawns not
landing on first or last rows and that after a swap move a pawn should not be able to push 2 squares. A particular set
of cases only tested capture moves where each individual piece was tested for capturing separately. Eventually, we
tested positions where multiple capture moves were possible. Any of these capture moves should be legal moves.

### Testing of Timer

We tested the Timer class in that we set it for a particular time value and delayed it for a particular period of time.
Finally, we expected the output time to be close to be to the delayed value. The actual value might have differed a bit
from the expectation because of the time taken through disk and CPU operations.

### Testing of Search Engine

The tests for the Nega-Max, variation of Mini-Max, included studies of chess problems where the computer is supposed
to find the winning move order. Exercises ranging from depth 1 to 10 were passed to Nega-Max function initially. Later,
same exercises were asked to Alpha-Beta improvement on Nega-Max where same results were expected. Afterwards, same
exercises were passed to Alpha-Beta with hashing. Finally, when quiescent and PVS search were added same tests were
carried out. The tests also included testing whether different levels of evaluators returned the same results.
Particularly, the sophisticated evaluator should not have returned a different value than the actual expected value.

To further test the implemented search techniques, we let the engine think at the starting board position for a depth
5 or greater to see whether our added techniques have actually shortened thinking time. They should still return
the same move, but in shorter time.

Zobrist keys were tested by passing in all the board tests that were creating so far and checking for collisions.
Furthermore, we tested two positions that looked the same, but differed on enpassant and castling information.
The current implementation of zobrist key should have given the same value because it does not take into account
enpassant and castling information.

We also tested opening book through GUI to see whether the AI makes the first couple of moves instantly.

### Testing of Evaluator

Testing of second and third level evaluator calls was easy because they contain a single weighted sum of pieces with
no weights for their position on the board. The first level evaluator call was tested through glass box testing where
each heuristic was tested in a simple test case. All pieces were tested whether they included the right square weighted
values. Pawn and rook heuristics were tested in individual test cases as well. We believe first level evaluator is
strongly tested.

### Testing Cylindrical Antichess

We have repeated all test suites for normal Antichess on cylindrical boards as well. Furthermore, we have written an
additional set of test cases that were explicitly checking whether the cylindrical properties of the board were
preserved. With these new test case, we tested invalid moves, outside bounds, same spot moves, valid cylindrical
moves, enpassant, capture and capture promotion, castling, and check domains similar to normal Antichess tests we
performed.

### Testing of GameController and TextUI

When the implementation of the GameController was complete, we first tested it separately to make sure that all
its basic functionalities were working.

### Reading and Writing Game File

To test the reading of the game file format, we have created numerous test cases for both regular and cylindrical
boards. A file containing the filenames of all the test cases are read in using a Junit test suite, and then
GameController is used to read every test file one by one.

After parsing the board from the file, we let the GameController reconstruct the board string from its internal fields.
Then in the Junit test, we check whether the contents of the original file read matches the generated board string.

To further verify the correctness of the generated board string, we have an addition move on the line after the
semi-colon of the board string in the file. Suppose we have a file ep1.txt with the following simple contents to
test for en passant:

white
30000 31000
0 0
p b5 b7 1 1
P a5 a4 1 0;
a5-b6;

a5-a6 is the move that the GameController has to make to the board, and then it should produce the resulting board
string as a result of the application of the move. Then Junit test will verify that the output generated into
ep1.actual with ep1.txt2:

black
30000 31000
0 0
P b6 a5 1 1;

### Testing of Saving and Loading

Since saving and loading of game is supported in the user interface, we used the TextUI to test saving and loading
of files. For TextUI we can pipe a string of commands into it and have TextUI save the resulting board string into
another file for checking. Suppose we have the text file testep1.txt containing the following commands :

LoadGame ep1.txt
MakeMove a5-b6
SaveGame ep1.actual
QuitGame

Then we just have to compare the contents of the file ep1.actual to ep1.txt2 which contains the expected output.

### Testing of GUI

GUI tests included manually played games, and loading up the many test cases that were made for evaluator and search
engine tests. We expected that these files would load up properly. We manually tested each test case written for
search engine and evaluator classes on the GUI as well and expected the same results as before. We also saved game
position into a file and compared it to what we expect it to be. This might not be the best way to test, but it is
exhaustive and ensures that all saving and loading of files work correctly.

In played games, we tested for black win, white win, and draw situations where we played a game and won,drew, and lost
accordingly. We tested loses on time as well. We found and fixed a bug that would not let the player play a valid swap
move.

We tested drag&drop such that if we click anywhere on a particular piece it would drag from wherever we clicked,
not necessarily from the center of the piece. We tested exiting the program manually.

We tested all options in the settings and their combinations, different time controls, different AI players, all
possible moves highlighting, and timer information on move output.

The GUI passed all these tests with no crashes and we are confident in correct workings of our simple GUI.

### Test Results

The following input domains were particularly well tested on both cylindrical and normal Antichess boards.

For Moves:

- Invalid Moves: forced capture and in Check test fails, outside Bounds
- Valid Moves: Valid Only and Capture
- For Pawn: enpassant, double push, promotion, capture
- For King: castling, check test

All modules have been thoroughly tested in the during both unit and integration tests. Testing has cleared several
dozens of critical bugs from the Piece, Board, Move, Evaluator, Timer, Engine, GameController, TextUI, and GUI classes.
We are confident in the correct workings of these classes.

## REFLECTIONS

### Evaluation

Over the few weeks of our development, we have made many changes to our design. After much prototyping, discussions
and experimentation, we managed to come up with a fairly modular design for the preliminary design.

### Lessons

We have learnt that a good modular design is important as it allows changes to be made to components without affecting
the rest of the system. A good design also contributes to the correctness of implementation greatly, and allows
parallel development of modules.

### Known Bugs and Limitations

The program is currently working fine as far as we have tested it. We have managed to fix bugs we discovered through
our exhaustive test suites which we run every time we make a change to the code.

Our current version of 4-player game does not allow save/load game because we have not defined a new game file format
for 4-player games. In future we could extend the current game file format to support save/load for 4 -player.

Switch Move (the following apply for white pieces):

- When using the switch move, the destination square of a pawn cannot be the first or last rank.
- Pawns can only move two squares forward if it is its first move. A swapped pawn is no longer allowed to move forward two squares, even if its swapped destination is the second rank.
- If the a1 and h1 rooks are swapped, neither is valid for castling later because they have been moved.
- Because the rules of en passant require that the capturing be made on the turn immediately following a 7th to 5th rank pawn pass, there is also no way for an en passant to occur immediately after a swap move from the opponent.
- A switch move is to be displayed in alphanumeric order of the two locations involved.