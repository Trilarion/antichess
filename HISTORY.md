# History of AntiChess

## AntiChess on SourceForge (2003, MIT license)

AntiChess has been created first on [SourceForge](https://sourceforge.net/projects/org.antichess/) in 2003
as an anti chess strategy game written in Java published under the MIT license.

It offered the sources as [archives](https://sourceforge.net/projects/org.antichess/files/org.antichess/1.0/) and
as well as [CVS repository](http://org.antichess.cvs.sourceforge.net/).

It reached version 1.0 in July 2003, and was authored by Tamer Karatekin, Hooria Komal, Hongping Lim
and Baris Yuksel, all from the MIT.

## AntiChess on GitLab (2020, GPL license)

AntiChess has been continued in 2020.

The sources were available on [GitLab](https://gitlab.com/Trilarion/org.antichess.git) and the modified game was
re-licensed under the GPL-3.0 license, which is more restrictive than MIT.

The main author was [Trilarion](https://trilarion.blogspot.com/).