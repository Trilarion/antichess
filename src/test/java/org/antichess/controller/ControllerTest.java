/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.controller;

import junit.framework.TestCase;
import org.antichess.utils.TestUtils;
import org.junit.Assert;

import java.util.List;
import java.util.StringTokenizer;

/**
 *
 */
public class ControllerTest
        extends TestCase {

    final String testfilesFile = "/tests";

    /**
     * Reads a file into gameController, and then get the
     * string output from gameController, and compare results
     **/
    public static void loadFile(String filename) {
        Controller controller = new Controller(filename);
        List<String> l = TestUtils.linesToSortedList(controller.getBoardString());
        List<String> l1 = TestUtils.linesToSortedList(TestUtils.fileRead(filename, false));
        Assert.assertEquals("testing " + filename, l1, l);
    }

    /**
     * Tests reading from file and generating string representation of board
     * Reads a "test/tests" to get a list of filenames to use
     **/
    public void testGameControllerReadWrite() {

        StringTokenizer st = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st.hasMoreElements()) {
            //       if (1==1)break;
            String filename = (String) st.nextElement();
            System.out.println("------ Testing reading from file and generating output ------\n ------ Reading from: " + filename + "-------------");
            loadFile("/" + filename);
            //      break;
        }

    }

}
