/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.controller;

import junit.framework.TestCase;
import org.junit.Assert;

/**
 *
 */
public class TimerTest
        extends TestCase {
    public TimerTest(String name) {
        super(name);
    }

    static void delay(long num_milliseconds, Timer timer) {
        long the_time = System.currentTimeMillis();
        while ((System.currentTimeMillis() - the_time) < num_milliseconds) {
            Assert.assertTrue("timer running", timer.getIsTiming());
        }
    }

    public void testTimer() {
        long tolerance = 200;
        Timer timer = new Timer(5000);
        timer.startTimer();
        Assert.assertTrue("timer running", timer.getIsTiming());
        delay(1000, timer);
        timer.stopTimer();
        //System.out.println ("Timeleft "  + timer.getTimeLeft());
        Assert.assertEquals("timer", 4000, timer.getTimeLeft(), tolerance);
        Assert.assertFalse("timer not running", timer.getIsTiming());

        //mutate
        timer.setTimeLeft(20000);
        timer.startTimer();
        delay(1000, timer);
        timer.stopTimer();
        Assert.assertEquals("timer", 19000, timer.getTimeLeft(), tolerance);
        Assert.assertFalse("timer not running", timer.getIsTiming());
    }

}
