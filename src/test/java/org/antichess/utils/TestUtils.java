/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.utils;

import org.antichess.model.Board;
import org.antichess.model.pieces.PieceImplementation;
import org.antichess.model.pieces.PieceType;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;

/**
 * org.antichess.Utils for testing.
 */
public final class TestUtils {

    private TestUtils() {
    }

    /**
     * Converts a string of line into a List of sorted strings
     **/
    public static List<String> linesToSortedList(String s) {
        StringTokenizer st = new StringTokenizer(s, "\n");
        List<String> l = new ArrayList<>();
        while (st.hasMoreElements()) {
            String str = (String) st.nextElement();
            if (!str.isEmpty())
                if (str.charAt(str.length() - 1) == ';')
                    str = str.substring(0, str.length() - 1);
            l.add(str);
        }
        Collections.sort(l);
        //System.out.println (l);
        return l;
    }

    /**
     * This function only returns all text before the first ";"
     * and sets comments to contain all text after the first ";"
     **/
    public static String fileRead(String filename, boolean readAll) {
        if (filename == null)
            throw new RuntimeException("No file specified");

        StringBuilder answer = new StringBuilder();

        try {
            BufferedReader in = new BufferedReader(new FileReader(TestUtils.class.getResource(filename).getFile()));
            // read each line until the end of the file and parse it into the script
            for (String line = in.readLine(); line != null; line = in.readLine()) {

                answer.append(line);

                if (!line.isEmpty())
                    if (line.charAt(line.length() - 1) == ';' && !readAll) {
                        break;
                    }

                answer.append("\n");

            }

        } catch (Exception e) {
            throw new RuntimeException("File not accessible\n" + filename + "\n" + e);
        }

        //System.out.println ("File read:\n" + answer);
        return answer.toString();
    }

    /**
     * Helper Function
     * Checks whether given piece is in array
     *
     * @requires b!=null && array!=null && p!=null
     * @return true if p is in array
     */
    public static boolean hasInside(Board board, PieceImplementation[][] array, PieceImplementation piece) {
        int color = piece.getColor();
        PieceType type = piece.getType();
        int numberofpieces = board.getPieceCount(color, type.getValue());
        for (int i = 0; i < numberofpieces; i++)
            if (array[type.getValue()][i] != null)
                if ((array[type.getValue()][i]).equals(piece)) return true;
        return false;
    }
}
