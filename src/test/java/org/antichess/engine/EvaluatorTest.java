/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.engine;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.antichess.model.Sides;
import org.antichess.utils.TestUtils;
import org.antichess.controller.Controller;
import org.antichess.model.Board;

import java.util.StringTokenizer;

/**
 * Tests Evaluator
 */
public class EvaluatorTest
        extends TestCase {
    final String testfilesFile = "/evaltests";

    public EvaluatorTest(String name) {
        super(name);
    }

    // Tell JUnit what order to run the tests in
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new EvaluatorTest("testEvaluator"));
        return suite;
    }

    /**
     * Tests evaluator
     **/
    public void testEvaluator() {

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING EVALUATOR ------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            System.out.println("file contents : \n" + boardinfo);
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String qualityStr = (String) st.nextElement();
            qualityStr = qualityStr.substring(1); // get rid of preceding \n
            System.out.println("Quality string: " + qualityStr);
            int quality = Integer.parseInt(qualityStr);
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after second ;");

            String expStr = (String) st.nextElement();
            expStr = expStr.substring(1);
            int expected = Integer.parseInt(expStr);
            System.out.println("Testing " + filename);
            System.out.println("Quality: " + quality);
            System.out.println("Expected: " + expected);

            Controller controller = new Controller("/" + filename);
            Board board = controller.getBoard();
            int nextTurn = controller.getNextTurn();
            System.out.println("Switched: " + board.getSpecial(Sides.WHITE)
                    + " " + board.getSpecial(Sides.BLACK));
            System.out.println("Board:\n" + board);

            double result = Evaluator.evaluateBoard(board, nextTurn, quality);
            System.out.println("Actual value:" + result);

            // assertEquals("evaluator test: ", (double) expected, result, 0.0); // expected:<-270.0> but was:<-240.0>
        }
    }
}
