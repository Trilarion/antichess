/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.engine;

import junit.framework.TestCase;
import org.antichess.model.Sides;
import org.antichess.utils.TestUtils;
import org.antichess.controller.Controller;
import org.antichess.model.Board;
import org.antichess.model.Move;
import org.junit.Assert;

import java.util.Hashtable;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Tests BetterEngine
 */
public class BetterEngineTest extends TestCase {

    /**
     * Tests evaluator
     **/
    public void testEvaluator() {

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead("/newEngineTests", false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING ENGINE ------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            System.out.println("Testing " + filename);

            String boardinfo = (String) st.nextElement();
            System.out.println("file contents : \n" + boardinfo);
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String alphaStr = (String) st.nextElement();
            alphaStr = alphaStr.substring(1); // get rid of preceding \n
            int alpha = Integer.parseInt(alphaStr);
            System.out.println("Alpha: " + alpha);

            String betaStr = (String) st.nextElement();
            betaStr = betaStr.substring(1); // get rid of preceding \n
            int beta = Integer.parseInt(betaStr);
            System.out.println("beta: " + beta);

            String depthStr = (String) st.nextElement();
            depthStr = depthStr.substring(1); // get rid of preceding \n
            int depth = Integer.parseInt(depthStr);
            System.out.println("depth: " + depth);

            String qualityStr = (String) st.nextElement();
            qualityStr = qualityStr.substring(1); // get rid of preceding \n
            System.out.println("Quality string: " + qualityStr);
            int quality = Integer.parseInt(qualityStr);

            String expStr = (String) st.nextElement();
            String expected = expStr.substring(1);
            System.out.println("Expected: " + expected);

            Controller controller = new Controller("/" + filename);
            Board board = controller.getBoard();
            int nextTurn = controller.getNextTurn();
            System.out.println("Switched: " + board.getSpecial(Sides.WHITE)
                    + " " + board.getSpecial(Sides.BLACK));
            System.out.println("Board:\n" + board);

            if (depth == 1) {
                System.out.println("Testcase skipped");
                continue;
            }

            Map<Long, EvaluatedMove> boardTable = new Hashtable<>();

            BetterEngine engine = new BetterEngine(board, boardTable, depth, nextTurn);
            engine.run();
            Move move = engine.getFoundMove();
            System.out.println("Actual value: " + move);

            Assert.assertTrue("betterEngine test failed", (new Move(expected)).equals(move));
        }
    }
}
