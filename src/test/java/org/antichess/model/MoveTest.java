/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model;

import junit.framework.TestCase;
import org.junit.Assert;

/**
 *
 */
public class MoveTest
        extends TestCase {
    public MoveTest(String name) {
        super(name);
    }

    public void testMove() {
        Move move;
        int startRow, startCol;
        int destRow, destCol;
        // Create from string
        move = new Move("d5-h1");
        Assert.assertEquals("row = 4", 4, move.getStartRow());
        Assert.assertEquals("col = 3", 3, move.getStartCol());
        Assert.assertEquals("row = 0", 0, move.getDestRow());
        Assert.assertEquals("col = 7", 7, move.getDestCol());
        Assert.assertEquals("d5-h1", "d5-h1", move.toString());

        // Create from string
        move = new Move("d5-k13");
        Assert.assertEquals("row = 4", 4, move.getStartRow());
        Assert.assertEquals("col = 3", 3, move.getStartCol());
        Assert.assertEquals("row = 12", 12, move.getDestRow());
        Assert.assertEquals("col = 10", 10, move.getDestCol());
        Assert.assertEquals("d5-k13", "d5-k13", move.toString());

        // Create from string
        move = new Move("n20-k13");
        Assert.assertEquals("row = 19", 19, move.getStartRow());
        Assert.assertEquals("col = 13", 13, move.getStartCol());
        Assert.assertEquals("row = 12", 12, move.getDestRow());
        Assert.assertEquals("col = 10", 10, move.getDestCol());
        Assert.assertEquals("n20-k13", "n20-k13", move.toString());

        // Create from integer
        move = new Move(3, 1, 7, 5);
        Assert.assertEquals("row = 1", 1, move.getStartRow());
        Assert.assertEquals("col = 3", 3, move.getStartCol());
        Assert.assertEquals("row = 5", 5, move.getDestRow());
        Assert.assertEquals("col = 7", 7, move.getDestCol());
        //System.out.println (move.toString() + "/");
        Assert.assertEquals("d2-h6", "d2-h6", move.toString());

        // Create from integer
        move = new Move(13, 11, 17, 15);
        Assert.assertEquals("row = 11", 11, move.getStartRow());
        Assert.assertEquals("col = 13", 13, move.getStartCol());
        Assert.assertEquals("row = 15", 15, move.getDestRow());
        Assert.assertEquals("col = 17", 17, move.getDestCol());
        //System.out.println (move.toString());
        Assert.assertEquals("n12-r16", "n12-r16", move.toString());

        // Create from integer
        move = new Move(10, 10, 9, 9);
        Assert.assertEquals("row = 10", 10, move.getStartRow());
        Assert.assertEquals("col = 10", 10, move.getStartCol());
        Assert.assertEquals("row = 9", 9, move.getDestRow());
        Assert.assertEquals("col = 9", 9, move.getDestCol());
        //System.out.println (move.toString());
        Assert.assertEquals("k11-j10", "k11-j10", move.toString());

        // Info

        // only normal is true
        Assert.assertTrue("Normal", move.isNormal());
        Assert.assertFalse("Ep", move.getEp());
        Assert.assertFalse("Pawn", move.getPawn());
        Assert.assertFalse("Promote", move.getPromote());
        Assert.assertFalse("Special", move.isSpecial());
        Assert.assertFalse("Capture", move.isCapture());
        Assert.assertFalse("Castle", move.getCastle());

        move.setEp(true);
        Assert.assertTrue("Ep", move.getEp());
        move.setPawn(true);
        Assert.assertTrue("Pawn", move.getPawn());
        move.setPromote(true);
        Assert.assertTrue("Promote", move.getPromote());
        move.setSpecial(true);
        Assert.assertTrue("Special", move.isSpecial());
        move.setCapture(true);
        Assert.assertTrue("Capture", move.isCapture());
        move.setCastle(true);
        Assert.assertTrue("Castle", move.getCastle());
        Assert.assertFalse("Normal", move.isNormal());

        move.setCapture(false);
        Assert.assertFalse("Capture", move.isCapture());
        move.setPromote(false);
        Assert.assertFalse("Promote", move.getPromote());
        move.setEp(false);
        Assert.assertFalse("Ep", move.getEp());
        move.setSpecial(false);
        Assert.assertFalse("Special", move.isSpecial());
        move.setCastle(false);
        Assert.assertFalse("Castle", move.getCastle());
        move.setPawn(false);
        Assert.assertFalse("Pawn", move.getPawn());
        Assert.assertTrue("Normal", move.isNormal());
    }
}
