/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.antichess.utils.TestUtils;
import org.antichess.controller.Controller;
import org.junit.Assert;

import java.util.List;
import java.util.StringTokenizer;

/**
 * Tests Piece
 */
public class PieceTest
        extends TestCase {
    final String testfilesFile = "/tests";
    final String testEndGameFile = "/testend";

    public PieceTest(String name) {
        super(name);
    }

    // Tell JUnit what order to run the tests in
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new PieceTest("testEndGame"));
        suite.addTest(new PieceTest("testIsValidMove"));
        suite.addTest(new PieceTest("testMakeMove"));
        suite.addTest(new PieceTest("testUndoMove"));
        return suite;
    }

    /**
     * Tests making a move and updating of the board
     **/
    public void testMakeMove() {

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING MAKEMOVE ------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String movestring = (String) st.nextElement();
            movestring = movestring.substring(1); // get rid of preceding \n
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after second ;");

            String expectedStr = (String) st.nextElement();
            boolean expected = ("\ntrue".equalsIgnoreCase(expectedStr));
            System.out.println("Testing " + filename);
            System.out.println("Move: " + movestring);
            System.out.println("Expected: " + expected);

            if (expected) {
                Controller controller = new Controller("/" + filename);
                Board board = controller.getBoard();
                Move move = new Move(movestring);
                int nextTurn = controller.getNextTurn();
                System.out.println("Switched: " + board.getSpecial(Sides.WHITE)
                        + " " + board.getSpecial(Sides.BLACK));

                // Make move
                board.makeMove(move);

                controller.setNextTurn(Helper.getOpponentColor(nextTurn));
                List<String> l2 = TestUtils.linesToSortedList(controller.getBoardString());
                //System.out.println (l);
                List<String> l3 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false));
                //System.out.println (l);
                if (!l3.equals
                        (l2)) {
                    System.out.println(TestUtils.fileRead("/" + filename + "2", false));
                    System.out.println(controller.getBoardString());
                    System.out.println("Expected:");
                    List<String> l1 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false));
                    //System.out.println (l);
                    System.out.println(l1);
                    System.out.println("Actual:");
                    List<String> l = TestUtils.linesToSortedList(controller.getBoardString());
                    //System.out.println (l);
                    System.out.println(l);

                }

                List<String> l = TestUtils.linesToSortedList(controller.getBoardString());
                //System.out.println (l);
                List<String> l1 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false));
                //System.out.println (l);
                Assert.assertEquals("Testing make move " + move + ":\n", l1, l);
            }

        }
    }

    /**
     * Tests undoing a move after making a move and updating of the board
     **/
    public void testUndoMove() {
        //if (1==1) return;
        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING UNDOMOVE ------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String movestring = (String) st.nextElement();
            movestring = movestring.substring(1); // get rid of preceding \n
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after second ;");

            String expectedStr = (String) st.nextElement();
            boolean expected = ("\ntrue".equalsIgnoreCase(expectedStr));
            System.out.println("Testing " + filename);
            System.out.println("Move: " + movestring);
            System.out.println("Expected: " + expected);

            if (expected) {
                Controller controller = new Controller("/" + filename);
                Board board = controller.getBoard();
                Move move = new Move(movestring);
                int nextTurn = controller.getNextTurn();
                System.out.println("Switched: " + board.getSpecial(Sides.WHITE)
                        + " " + board.getSpecial(Sides.BLACK));

                // Make move
                board.makeMove(move);

                controller.setNextTurn(Helper.getOpponentColor(nextTurn));
                List<String> l6 = TestUtils.linesToSortedList(controller.getBoardString());
                //System.out.println (l);
                List<String> l7 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false));
                //System.out.println (l);
                if (!l7.equals
                        (l6)) {
                    System.out.println(TestUtils.fileRead("/" + filename + "2", false));
                    System.out.println(controller.getBoardString());
                    System.out.println("Expected:");
                    List<String> l1 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false));
                    //System.out.println (l);
                    System.out.println(l1);
                    System.out.println("Actual:");
                    List<String> l = TestUtils.linesToSortedList(controller.getBoardString());
                    //System.out.println (l);
                    System.out.println(l);

                }

                List<String> l4 = TestUtils.linesToSortedList(controller.getBoardString());
                //System.out.println (l);
                List<String> l5 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false));
                //System.out.println (l);
                Assert.assertEquals("Testing make move " + move + ":\n",
                        l5,
                        l4);

                // Make undo Move
                board.undoMove(move);

                controller.setNextTurn(nextTurn);

                List<String> l2 = TestUtils.linesToSortedList(controller.getBoardString());
                //System.out.println (l);
                List<String> l3 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename, false));
                //System.out.println (l);
                if (!l3.equals
                        (l2)) {
                    System.out.println(TestUtils.fileRead("/" + filename, false));
                    System.out.println(controller.getBoardString());
                    System.out.println("Expected:");
                    List<String> l1 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename, false));
                    //System.out.println (l);
                    System.out.println(l1);
                    System.out.println((TestUtils.fileRead("/" + filename, false)));

                    System.out.println("Actual:");
                    List<String> l = TestUtils.linesToSortedList(controller.getBoardString());
                    //System.out.println (l);
                    System.out.println(l);
                    System.out.println((controller.getBoardString()));

                }
                List<String> l = TestUtils.linesToSortedList(controller.getBoardString());
                //System.out.println (l);
                List<String> l1 = TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename, false));
                //System.out.println (l);
                Assert.assertEquals("Testing undo move " + move + ":\n",
                        l1,
                        l);

            }

        }
    }

    /**
     * Tests if a move is valid. The move is specified after the first semicolon
     * in the file, and ends with a second semicolon. The expected answer is
     * true/false, and ends with a semicolon too;
     **/
    public void testIsValidMove() {
        //if (1==1) return;

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING VALID MOVE ------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String movestring = (String) st.nextElement();
            movestring = movestring.substring(1); // get rid of preceding \n
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after second ;");

            String expectedStr = (String) st.nextElement();
            boolean expected = ("\ntrue".equalsIgnoreCase(expectedStr));
            System.out.println("Testing " + filename);
            System.out.println("Move: " + movestring);
            System.out.println("Expected: " + expected);

            Controller controller = new Controller("/" + filename);
            Board board = controller.getBoard();
            Move move = new Move(movestring);
            int nextTurn = controller.getNextTurn();
            boolean result = board.isValidMove(move, nextTurn);

            Assert.assertEquals("Testing move " + move, expected, result);
        }
    }

    /**
     * Tests for end game conditions. The first semicolon is the end of boardinfo
     * in the file. The line after board info gives true/false ending with the second
     * semicolon indicating if the game is over or not.
     * The line after that is true/false indicating if the game is a draw or not, and
     * ends with a third semicolon;
     **/
    public void testEndGame() {
        // if (1==1 ) return;

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testEndGameFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING END GAME ------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String expectedStr = (String) st.nextElement();
            boolean expectedEnd = ("\ntrue".equalsIgnoreCase(expectedStr));

            expectedStr = (String) st.nextElement();
            boolean expectedDraw = ("\ntrue".equalsIgnoreCase(expectedStr));

            System.out.println("Testing " + filename);
            System.out.println("Expected end game: " + expectedEnd);
            System.out.println("Expected draw game: " + expectedDraw);

            Controller controller = new Controller("/" + filename);
            Board board = controller.getBoard();

            int nextTurn = controller.getNextTurn();

            boolean resultEndGame = board.isGameOver(nextTurn);

            Assert.assertEquals("Testing end game: ", expectedEnd, resultEndGame);

            if (resultEndGame) {
                boolean resultDraw = board.isGameOver(nextTurn);

            }

        }
    }
}
