/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.antichess.utils.TestUtils;
import org.antichess.controller.Controller;
import org.junit.Assert;

import java.util.StringTokenizer;

/**
 * Tests the Cylindrical board
 */
public class CylinderTest
        extends TestCase {
    final String testfilesFile = "/cylinderTests";
    final String testEndGameFile = "/cylinderEndGameTests";

    public CylinderTest(String name) {
        super(name);
    }

    // Tell JUnit what order to run the tests in
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new CylinderTest("testEndGame"));
        suite.addTest(new CylinderTest("testIsValidMove"));
        suite.addTest(new CylinderTest("testMakeMove"));
        suite.addTest(new CylinderTest("testUndoMove"));
        return suite;
    }

    /**
     * Tests making a move and updating of the board
     **/
    public void testMakeMove() {

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING MAKEMOVE FOR CYLINDRICAL BOARD------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String movestring = (String) st.nextElement();
            movestring = movestring.substring(1); // get rid of preceding \n
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after second ;");

            String expectedStr = (String) st.nextElement();
            boolean expected = ("\ntrue".equalsIgnoreCase(expectedStr));
            System.out.println("Testing " + filename);
            System.out.println("Move: " + movestring);
            System.out.println("Expected: " + expected);

            if (expected) {
                Controller controller = new Controller("/" + filename);
                Board board = controller.getBoard();
                Move move = new Move(movestring);
                //Piece piece= b.getPieceAt (move.getStartCol(), move.getStartRow());
                int nextTurn = controller.getNextTurn();
                System.out.println("Switched: " + board.getSpecial(Sides.WHITE)
                        + " " + board.getSpecial(Sides.BLACK));

                // Make move
                board.makeMove(move);

                controller.setNextTurn(Helper.getOpponentColor(nextTurn));
                if (!TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false)).equals
                        (TestUtils.linesToSortedList(controller.getBoardString()))) {
                    System.out.println(TestUtils.fileRead("/" + filename + "2", false));
                    System.out.println(controller.getBoardString());
                    System.out.println("Expected:");
                    System.out.println(TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false)));
                    System.out.println("Actual:");
                    System.out.println(TestUtils.linesToSortedList(controller.getBoardString()));

                }

                Assert.assertEquals("Testing make move " + move + ":\n",
                        TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false)),
                        TestUtils.linesToSortedList(controller.getBoardString()));
            }

        }
    }

    /**
     * Tests undoing a move after making a move and updating of the board
     **/
    public void testUndoMove() {
        //if (1==1) return;
        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING UNDOMOVE FOR CYLINDRICAL BOARD------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String movestring = (String) st.nextElement();
            movestring = movestring.substring(1); // get rid of preceding \n
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after second ;");

            String expectedStr = (String) st.nextElement();
            boolean expected = ("\ntrue".equalsIgnoreCase(expectedStr));
            System.out.println("Testing " + filename);
            System.out.println("Move: " + movestring);
            System.out.println("Expected: " + expected);

            if (expected) {
                Controller controller = new Controller("/" + filename);
                Board board = controller.getBoard();
                Move move = new Move(movestring);
                //Piece piece= b.getPieceAt (move.getStartCol(), move.getStartRow());
                int nextTurn = controller.getNextTurn();
                System.out.println("Switched: " + board.getSpecial(Sides.WHITE)
                        + " " + board.getSpecial(Sides.BLACK));

                // Make move
                board.makeMove(move);

                controller.setNextTurn(Helper.getOpponentColor(nextTurn));
                if (!TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false)).equals
                        (TestUtils.linesToSortedList(controller.getBoardString()))) {
                    System.out.println(TestUtils.fileRead("/" + filename + "2", false));
                    System.out.println(controller.getBoardString());
                    System.out.println("Expected:");
                    System.out.println(TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false)));
                    System.out.println("Actual:");
                    System.out.println(TestUtils.linesToSortedList(controller.getBoardString()));

                }

                Assert.assertEquals("Testing make move " + move + ":\n",
                        TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename + "2", false)),
                        TestUtils.linesToSortedList(controller.getBoardString()));

                // Make undo Move
                board.undoMove(move);

                controller.setNextTurn(nextTurn);

                if (!TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename, false)).equals
                        (TestUtils.linesToSortedList(controller.getBoardString()))) {
                    System.out.println(TestUtils.fileRead("/" + filename, false));
                    System.out.println(controller.getBoardString());
                    System.out.println("Expected:");
                    System.out.println(TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename, false)));
                    System.out.println((TestUtils.fileRead("/" + filename, false)));

                    System.out.println("Actual:");
                    System.out.println(TestUtils.linesToSortedList(controller.getBoardString()));
                    System.out.println((controller.getBoardString()));

                }
                Assert.assertEquals("Testing undo move " + move + ":\n",
                        TestUtils.linesToSortedList(TestUtils.fileRead("/" + filename, false)),
                        TestUtils.linesToSortedList(controller.getBoardString()));

            }

        }
    }

    /**
     * Tests if a move is valid. The move is specified after the first semicolon
     * in the file, and ends with a second semicolon. The expected answer is
     * true/false, and ends with a semicolon too;
     **/
    public void testIsValidMove() {
        //if (1==1) return;

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING VALID MOVE FOR CYLINDRICAL BOARD------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String movestring = (String) st.nextElement();
            movestring = movestring.substring(1); // get rid of preceding \n
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after second ;");

            String expectedStr = (String) st.nextElement();
            boolean expected = ("\ntrue".equalsIgnoreCase(expectedStr));
            System.out.println("Testing " + filename);
            System.out.println("Move: " + movestring);
            System.out.println("Expected: " + expected);

            Controller controller = new Controller("/" + filename);
            Board board = controller.getBoard();
            Move move = new Move(movestring);
            //Piece piece= b.getPieceAt (move.getStartCol(), move.getStartRow());
            int nextTurn = controller.getNextTurn();
            boolean result = board.isValidMove(move, nextTurn);
            System.out.println("Actual: " + result);
            Assert.assertEquals("Testing move " + move, expected, result);

        }
    }

    /**
     * Tests for end game conditions. The first semicolon is the end of boardinfo
     * in the file. The line after board info gives true/false ending with the second
     * semicolon indicating if the game is over or not.
     * The line after that is true/false indicating if the game is a draw or not, and
     * ends with a third semicolon;
     **/
    public void testEndGame() {
        // if (1==1 ) return;

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testEndGameFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING END GAME FOR CYLINDRICAL BOARD------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, true);
            StringTokenizer st = new StringTokenizer(filecontents, ";");
            String boardinfo = (String) st.nextElement();
            if (!st.hasMoreElements())
                throw new RuntimeException("Nothing found after first ;");

            String expectedStr = (String) st.nextElement();
            boolean expectedEnd = ("\ntrue".equalsIgnoreCase(expectedStr));

            expectedStr = (String) st.nextElement();
            boolean expectedDraw = ("\ntrue".equalsIgnoreCase(expectedStr));

            System.out.println("Testing " + filename);
            System.out.println("Expected end game: " + expectedEnd);
            System.out.println("Expected draw game: " + expectedDraw);

            Controller controller = new Controller("/" + filename);
            Board board = controller.getBoard();

            int nextTurn = controller.getNextTurn();

            boolean resultEndGame = board.isGameOver(nextTurn);

            Assert.assertEquals("Testing end game: ", expectedEnd, resultEndGame);

            if (resultEndGame) {
                boolean resultDraw = board.isGameOver(nextTurn);

            }

        }
    }
}
