/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model;

import junit.framework.TestCase;
import org.antichess.utils.TestUtils;
import org.antichess.controller.Controller;

import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * Tests Zobrist
 */
public class ZobristTest extends TestCase {

    final String testfilesFile = "/allTests";

    /**
     * Tests zobrist
     **/
    public void testZobrist() {
        if (1 == 1) return; // file allTests not available

        Map<Long, String> hm = new HashMap<>();

        StringTokenizer st1 = new StringTokenizer(TestUtils.fileRead(testfilesFile, false), "\n");
        while (st1.hasMoreElements()) {
            String filename = (String) st1.nextElement();
            System.out.println("------- TESTING ZOBRIST ------");
            System.out.println("------- Reading " + filename + " ------");

            String filecontents = TestUtils.fileRead("/" + filename, false);
            StringTokenizer st = new StringTokenizer(filecontents, ";");

            Controller controller = new Controller("/" + filename);
            int nextTurn = controller.getNextTurn();
            Board board = controller.getBoard();
            Zobrist z = board.getZobrist();
            Long key = z.getKey(board, nextTurn);
            String bstring = controller.getBoardString();
//       if (!bstring.equals(filecontents)){
// 	System.out.println (filecontents);
// 	System.out.println (bstring);
// 	throw new RuntimeException ("boardstring different from filecontents");
//       }

            String previousString = hm.get(key);
            if (previousString != null)
                if (!bstring.equals(previousString)) {
                    System.out.println("Duplicate zobrist key for two different boards:");
                    System.out.println(key);
                    System.out.println("Current board: ======");
                    System.out.println(bstring);
                    System.out.println("Previous board: ======");
                    System.out.println(previousString);
                    throw new RuntimeException("Zobrist duplicated for different boards");
                }

            hm.put(key, bstring);

        }
    }
}
