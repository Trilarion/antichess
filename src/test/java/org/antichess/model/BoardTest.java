/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import org.antichess.utils.TestUtils;
import org.antichess.model.pieces.*;
import org.junit.Assert;

/**
 *
 */
public class BoardTest extends TestCase {
    //4x4 square
    private final int[][] topology0 = {{1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1},
            {1, 1, 1, 1}};
    //4x4 square with the middle 4 squares not included
    private final int[][] topology1 = {{1, 1, 1, 1},
            {1, 0, 0, 1},
            {1, 0, 0, 1},
            {1, 1, 1, 1}};

    //8x8 square
    private final int[][] topology2 = {{1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1}};
    //8x8 square with (8,8) not included
    private final int[][] topology3 = {{1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 1},
            {1, 1, 1, 1, 1, 1, 1, 0}};
    private Board board0;
    private Board board1;
    private Board board2;
    private Board board3;
    private Board board4;

    public BoardTest(String name) {
        super(name);
    }

    // Tell JUnit what order to run the tests in
    public static Test suite() {
        TestSuite suite = new TestSuite();
        suite.addTest(new BoardTest("testChecked"));
        suite.addTest(new BoardTest("testSpecial"));
        suite.addTest(new BoardTest("testRegisterPiece"));
        suite.addTest(new BoardTest("testBoardCopy"));
        suite.addTest(new BoardTest("testGetUsable"));
        suite.addTest(new BoardTest("testLastPieceMoved"));
        suite.addTest(new BoardTest("testSetPieceAtandKing"));
        suite.addTest(new BoardTest("testGetDifference"));
        return suite;
    }

    protected void setUp() {
        board0 = new Board(topology0);
        board1 = new Board(topology1);
        board2 = new Board(topology2);
        board3 = new Board(topology3);
        board4 = new Board(topology2);
    }

    public void testChecked() {
        board0.setChecked(Sides.WHITE, true);
        //black should be false initially
        Assert.assertTrue("Board.getChecked: should be true for WHITE", board0.getChecked(Sides.WHITE));
        Assert.assertFalse("Board.getChecked: should be false for BLACK", board0.getChecked(Sides.BLACK));
        //white is true
        board0.setChecked(Sides.BLACK, true);
        Assert.assertTrue("Board.getChecked: should be true for WHITE", board0.getChecked(Sides.BLACK));
        Assert.assertTrue("Board.getChecked: should be true for BLACK", board0.getChecked(Sides.BLACK));
        board0.setChecked(Sides.WHITE, false);
        board0.setChecked(Sides.BLACK, true);
        Assert.assertFalse("Board.getChecked: should be false for WHITE", board0.getChecked(Sides.WHITE));
        Assert.assertTrue("Board.getChecked: should be true for BLACK", board0.getChecked(Sides.BLACK));
    }

    //also checks getPieceAt, getPieceCount, getAliveCount, getPiecesOnBoard, 

    public void testSpecial() {
        board0.setSpecial(Sides.WHITE, true);
        //black should be false initially
        Assert.assertTrue("Board.getSpecial: should be true for WHITE", board0.getSpecial(Sides.WHITE));
        Assert.assertFalse("Board.getSpecial: should be false for BLACK", board0.getSpecial(Sides.BLACK));
        //white is true
        board0.setSpecial(Sides.BLACK, true);
        Assert.assertTrue("Board.getSpecial: should be true for WHITE", board0.getSpecial(Sides.BLACK));
        Assert.assertTrue("Board.getSpecial: should be true for BLACK", board0.getSpecial(Sides.BLACK));
        board0.setSpecial(Sides.WHITE, false);
        board0.setSpecial(Sides.BLACK, true);
        Assert.assertFalse("Board.getSpecial: should be false for WHITE", board0.getSpecial(Sides.WHITE));
        Assert.assertTrue("Board.getSpecial: should be true for BLACK", board0.getSpecial(Sides.BLACK));
    }

    public void testRegisterPiece() {
        //assumes Piece constructors work fine

        //adds white pieces
        PieceImplementation pawn0 = new Pawn('p', board2, 0, 1, Sides.WHITE);
        PieceImplementation pawn1 = new Pawn('p', board2, 1, 1, Sides.WHITE);
        PieceImplementation pawn2 = new Pawn('p', board2, 2, 1, Sides.WHITE);
        PieceImplementation pawn3 = new Pawn('p', board2, 3, 1, Sides.WHITE);
        PieceImplementation pawn4 = new Pawn('p', board2, 4, 1, Sides.WHITE);
        PieceImplementation pawn5 = new Pawn('p', board2, 5, 1, Sides.WHITE);
        PieceImplementation pawn6 = new Pawn('p', board2, 6, 1, Sides.WHITE);
        PieceImplementation pawn7 = new Pawn('p', board2, 7, 1, Sides.WHITE);
        PieceImplementation rook0 = new Rook('r', board2, 0, 0, Sides.WHITE);
        PieceImplementation knight0 = new Knight('n', board2, 1, 0, Sides.WHITE);
        PieceImplementation bishop0 = new Bishop('b', board2, 2, 0, Sides.WHITE);
        PieceImplementation king0 = new King('k', board2, 3, 0, Sides.WHITE);
        PieceImplementation queen0 = new Queen('q', board2, 4, 0, Sides.WHITE);
        PieceImplementation bishop1 = new Bishop('b', board2, 5, 0, Sides.WHITE);
        PieceImplementation knight1 = new Knight('n', board2, 6, 0, Sides.WHITE);
        PieceImplementation rook1 = new Rook('r', board2, 7, 0, Sides.WHITE);

        //Piece constructor implicitly calls registerPiece on board
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(0, 1), pawn0);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(1, 1), pawn1);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(2, 1), pawn2);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(3, 1), pawn3);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(4, 1), pawn4);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(5, 1), pawn5);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(6, 1), pawn6);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(7, 1), pawn7);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(0, 0), rook0);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(1, 0), knight0);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(2, 0), bishop0);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(3, 0), king0);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(4, 0), queen0);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(5, 0), bishop1);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(6, 0), knight1);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(7, 0), rook1);

        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 8, board2.getPieceCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 2, board2.getPieceCount(Sides.WHITE, PieceType.ROOK.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 2, board2.getPieceCount(Sides.WHITE, PieceType.KNIGHT.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 2, board2.getPieceCount(Sides.WHITE, PieceType.BISHOP.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 1, board2.getPieceCount(Sides.WHITE, PieceType.QUEEN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 1, board2.getPieceCount(Sides.WHITE, PieceType.KING.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 0, board2.getPieceCount(Sides.BLACK, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 0, board2.getPieceCount(Sides.BLACK, PieceType.ROOK.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 0, board2.getPieceCount(Sides.BLACK, PieceType.KNIGHT.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 0, board2.getPieceCount(Sides.BLACK, PieceType.BISHOP.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 0, board2.getPieceCount(Sides.BLACK, PieceType.QUEEN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 0, board2.getPieceCount(Sides.BLACK, PieceType.KING.getValue()));

        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 8, board2.getAliveCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 2, board2.getAliveCount(Sides.WHITE, PieceType.ROOK.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 2, board2.getAliveCount(Sides.WHITE, PieceType.KNIGHT.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 2, board2.getAliveCount(Sides.WHITE, PieceType.BISHOP.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 1, board2.getAliveCount(Sides.WHITE, PieceType.QUEEN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 1, board2.getAliveCount(Sides.WHITE, PieceType.KING.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 0, board2.getAliveCount(Sides.BLACK, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 0, board2.getAliveCount(Sides.BLACK, PieceType.ROOK.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 0, board2.getAliveCount(Sides.BLACK, PieceType.KNIGHT.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 0, board2.getAliveCount(Sides.BLACK, PieceType.BISHOP.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 0, board2.getAliveCount(Sides.BLACK, PieceType.QUEEN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 0, board2.getAliveCount(Sides.BLACK, PieceType.KING.getValue()));

        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn0));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn1));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn2));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn3));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn4));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn5));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn6));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn7));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), rook0));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), rook1));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), knight0));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), knight1));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), bishop0));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), bishop1));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), queen0));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), king0));

        //put in other pieces and see how registerPiece handles the situation
        PieceImplementation pawn8 = new Pawn('p', board2, 0, 2, Sides.WHITE);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(0, 2), pawn8);
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 9, board2.getPieceCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 9, board2.getAliveCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn8));

        PieceImplementation pawn9 = new Pawn('p', board2, 0, 2, Sides.WHITE);
        pawn9.setLastLocation(9, 0); //change this so that it is not .equals(pawn8)
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(0, 2), pawn9);
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 9, board2.getPieceCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 9, board2.getAliveCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!pawn9 should be put in",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn9));
        Assert.assertFalse("Board.registerPiece did not update pieceArray correctly!pawn8 should be not in", TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn8));

        PieceImplementation rook2 = new Rook('r', board2, 0, 2, Sides.WHITE);
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(0, 2), rook2);
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 9, board2.getPieceCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 8, board2.getAliveCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 3, board2.getPieceCount(Sides.WHITE, PieceType.ROOK.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 3, board2.getAliveCount(Sides.WHITE, PieceType.ROOK.getValue()));
        Assert.assertFalse("Board.registerPiece did not update pieceArray correctly! pawn9 should have been deleted", TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn9));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly! rook2 should have been put in",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), rook2));

        //put pawn back on the rook
        pawn9 = new Pawn('p', board2, 0, 2, Sides.WHITE);
        pawn9.setLastLocation(9, 0); //change this so that it is not .equals(pawn8)
        Assert.assertEquals("Board.registerPiece did not register right!", board2.getPieceAt(0, 2), pawn9);
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 9, board2.getPieceCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 9, board2.getAliveCount(Sides.WHITE, PieceType.PAWN.getValue()));
        Assert.assertEquals("Board.registerPiece did not update pieceCount right!", 3, board2.getPieceCount(Sides.WHITE, PieceType.ROOK.getValue()));
        Assert.assertEquals("Board.registerPiece did not update aliveCount right!", 2, board2.getAliveCount(Sides.WHITE, PieceType.ROOK.getValue()));
        Assert.assertTrue("Board.registerPiece did not update pieceArray correctly!pawn9 should be put in",
                TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), pawn9));
        Assert.assertFalse("Board.registerPiece did not update pieceArray correctly!rook2 should be not in", TestUtils.hasInside(board2, board2.getPiecesOnBoard(Sides.WHITE), rook2));

    }

    public void testBoardCopy() {
        //adds white pieces
        PieceImplementation pawn0 = new Pawn('p', board3, 0, 1, Sides.WHITE);
        PieceImplementation pawn1 = new Pawn('p', board3, 1, 1, Sides.WHITE);
        PieceImplementation pawn2 = new Pawn('p', board3, 2, 1, Sides.WHITE);
        PieceImplementation pawn3 = new Pawn('p', board3, 3, 1, Sides.WHITE);
        PieceImplementation pawn4 = new Pawn('p', board3, 4, 1, Sides.WHITE);
        PieceImplementation pawn5 = new Pawn('p', board3, 5, 1, Sides.WHITE);
        PieceImplementation pawn6 = new Pawn('p', board3, 6, 1, Sides.WHITE);
        PieceImplementation pawn7 = new Pawn('p', board3, 7, 1, Sides.WHITE);
        PieceImplementation rook0 = new Rook('r', board3, 0, 0, Sides.WHITE);
        PieceImplementation knight0 = new Knight('n', board3, 1, 0, Sides.WHITE);
        PieceImplementation bishop0 = new Bishop('b', board3, 2, 0, Sides.WHITE);
        PieceImplementation king0 = new King('k', board3, 3, 0, Sides.WHITE);
        PieceImplementation queen0 = new Queen('q', board3, 4, 0, Sides.WHITE);
        PieceImplementation bishop1 = new Bishop('b', board3, 5, 0, Sides.WHITE);
        PieceImplementation knight1 = new Knight('n', board3, 6, 0, Sides.WHITE);
        PieceImplementation rook1 = new Rook('r', board3, 7, 0, Sides.WHITE);

        Board board4 = new Board(board3);
        Assert.assertEquals("Board.constructor: not equal to clone", board3.toString(), board4.toString());

    }

    public void testGetUsable() {
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(0, 0));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(0, 1));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(0, 2));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(0, 3));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(1, 0));
        Assert.assertFalse("Board.getUsable: is not working", board1.getUsable(1, 1));
        Assert.assertFalse("Board.getUsable: is not working", board1.getUsable(1, 2));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(1, 3));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(2, 0));
        Assert.assertFalse("Board.getUsable: is not working", board1.getUsable(2, 1));
        Assert.assertFalse("Board.getUsable: is not working", board1.getUsable(2, 2));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(2, 3));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(3, 0));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(3, 1));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(3, 2));
        Assert.assertTrue("Board.getUsable: is not working", board1.getUsable(3, 3));
    }

    public void testLastPieceMoved() {
        //adds white pieces
        PieceImplementation pawn0 = new Pawn('p', board3, 0, 1, Sides.WHITE);
        PieceImplementation pawn1 = new Pawn('p', board3, 1, 1, Sides.WHITE);
        PieceImplementation pawn2 = new Pawn('p', board3, 2, 1, Sides.WHITE);
        PieceImplementation pawn3 = new Pawn('p', board3, 3, 1, Sides.WHITE);
        PieceImplementation pawn4 = new Pawn('p', board3, 4, 1, Sides.WHITE);
        PieceImplementation pawn5 = new Pawn('p', board3, 5, 1, Sides.WHITE);
        PieceImplementation pawn6 = new Pawn('p', board3, 6, 1, Sides.BLACK);
        PieceImplementation pawn7 = new Pawn('p', board3, 7, 1, Sides.BLACK);
        PieceImplementation rook0 = new Rook('r', board3, 0, 0, Sides.BLACK);
        PieceImplementation knight0 = new Knight('n', board3, 1, 0, Sides.BLACK);
        PieceImplementation bishop0 = new Bishop('b', board3, 2, 0, Sides.BLACK);
        PieceImplementation king0 = new King('k', board3, 3, 0, Sides.BLACK);
        PieceImplementation queen0 = new Queen('q', board3, 4, 0, Sides.BLACK);
        PieceImplementation bishop1 = new Bishop('b', board3, 5, 0, Sides.BLACK);
        PieceImplementation knight1 = new Knight('n', board3, 6, 0, Sides.BLACK);
        PieceImplementation rook1 = new Rook('r', board3, 7, 0, Sides.BLACK);

        board3.setLastPieceMoved(pawn0);
        board3.setLastPieceMoved(rook1);

        Assert.assertTrue("Board.set/getLastPieceMoved is not working!", (board3.getLastPieceMoved(Sides.WHITE)).equals(pawn0));
        Assert.assertTrue("Board.set/getLastPieceMoved is not working!", (board3.getLastPieceMoved(Sides.BLACK)).equals(rook1));

        board3.setLastPieceMoved(pawn1);
        board3.setLastPieceMoved(knight1);

        Assert.assertTrue("Board.set/getLastPieceMoved is not working!", (board3.getLastPieceMoved(Sides.WHITE)).equals(pawn1));
        Assert.assertTrue("Board.set/getLastPieceMoved is not working!", (board3.getLastPieceMoved(Sides.BLACK)).equals(knight1));
    }

    public void testSetPieceAtandKing() {
        //adds white pieces
        PieceImplementation pawn0 = new Pawn('p', board3, 0, 1, Sides.WHITE);
        PieceImplementation pawn1 = new Pawn('p', board3, 1, 1, Sides.WHITE);
        PieceImplementation pawn2 = new Pawn('p', board3, 2, 1, Sides.WHITE);
        PieceImplementation pawn3 = new Pawn('p', board3, 3, 1, Sides.WHITE);
        PieceImplementation pawn4 = new Pawn('p', board3, 4, 1, Sides.WHITE);
        PieceImplementation pawn5 = new Pawn('p', board3, 5, 1, Sides.WHITE);
        PieceImplementation pawn6 = new Pawn('p', board3, 6, 1, Sides.BLACK);
        PieceImplementation pawn7 = new Pawn('p', board3, 7, 1, Sides.BLACK);
        PieceImplementation rook0 = new Rook('r', board3, 0, 0, Sides.BLACK);
        PieceImplementation knight0 = new Knight('n', board3, 1, 0, Sides.BLACK);
        PieceImplementation bishop0 = new Bishop('b', board3, 2, 0, Sides.BLACK);
        PieceImplementation king0 = new King('k', board3, 3, 0, Sides.BLACK);
        PieceImplementation queen0 = new Queen('q', board3, 4, 0, Sides.BLACK);
        PieceImplementation bishop1 = new Bishop('b', board3, 5, 0, Sides.BLACK);
        PieceImplementation knight1 = new Knight('n', board3, 6, 0, Sides.BLACK);
        PieceImplementation rook1 = new Rook('r', board3, 7, 0, Sides.BLACK);

        board3.setPieceAt(5, 5, rook1);
        Assert.assertTrue("Board.setPieceAt()", (board3.getPieceAt(5, 5)).equals(rook1));

        Assert.assertTrue("Board.getKing()", (board3.getKing(Sides.BLACK)).equals(king0));
        Assert.assertNull("Board.getKing()", board3.getKing(Sides.WHITE));
    }

    public void testGetDifference() {
        //adds white pieces
        PieceImplementation pawn0 = new Pawn('p', board4, 0, 1, Sides.WHITE);
        PieceImplementation pawn1 = new Pawn('p', board4, 1, 1, Sides.WHITE);
        PieceImplementation pawn2 = new Pawn('p', board4, 2, 1, Sides.WHITE);

        //adds black pawns
        PieceImplementation pawn4 = new Pawn('p', board4, 4, 1, Sides.BLACK);
        PieceImplementation pawn5 = new Pawn('p', board4, 5, 1, Sides.BLACK);
        PieceImplementation pawn6 = new Pawn('p', board4, 6, 1, Sides.BLACK);

        Assert.assertEquals("Board.getDifference() is wrong for equal number of pawns", 0, board4.getDifference(Sides.WHITE));
        Assert.assertEquals("Board.getDifference() is wrong for equal number of pawns", 0, board4.getDifference(Sides.BLACK));

        PieceImplementation pawn3 = new Pawn('p', board4, 3, 1, Sides.WHITE);

        Assert.assertEquals("Board.getDifference() is wrong for one extra WHITE pawn", -1 * Board.PIECEVALUE[PieceType.PAWN.getValue()], board4.getDifference(Sides.WHITE));
        Assert.assertEquals("Board.getDifference() is wrong for one extra WHITE pawn", Board.PIECEVALUE[PieceType.PAWN.getValue()], board4.getDifference(Sides.BLACK));
    }

}
