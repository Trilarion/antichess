/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.engine;

import org.antichess.model.Board;
import org.antichess.model.Helper;
import org.antichess.model.Move;

import java.util.List;
import java.util.Map;

/**
 * BetterEngine represents the player engine // TODO Principal variation search was mentioned, worth looking at
 */ // TODO 4-Player how good is the computer player right now? (just doing random moves)
public final class BetterEngine implements Runnable { // TODO difference to newEngine
// TODO Engine as interface
    /**
     * Constants which determine the search space
     */
    private static final int QUIES_DEPTH = 3;
    private static final int WIN = 123456789;
    private static final int UNKNOWN = 923456789;
    private static final boolean QUIES = true;
    private static final int SWITCH_STARTCOUNT = 10;
    private static final int MAXDEPTH = 5;
    //Necessary fields for this search
    private final Board board;
    private final Map<Long, EvaluatedMove> boardTable;
    private final int initialDepth;
    private final int initialNextTurn;

    private boolean searchInProgress;
    private boolean searchFinished;
    private Move foundMove;
    private Move secondMove;

    /**
     * This starts a new BetterEngine with the corresponding values
     */
    public BetterEngine(Board board, Map<Long, EvaluatedMove> boardTable, int depth, int nextTurn) {
        this.board = board;
        this.boardTable = boardTable;
        initialDepth = depth;
        initialNextTurn = nextTurn;
    }
    //                     ALHPA-BETA WITH HASHTABLE

    /**
     * Standard AlphaBeta algorithm, lower terms
     * It is necessary to make a distinction between the entrance method which runs
     * on the root node and the recursive method which runs on children nodes.
     * This distinction allows us to return from the recursion if depth is given
     * less than 0 or if it is an endgame situation (which should be detected by GameController)
     * Furthermore, entrance method returns a move and modifies the board, meanwhile the
     * recursive method only returns integer values.
     *
     * @return best value found so far
     * @requires board!=null && player=={Piece.WHITE || Piece.BLACK}
     */
    private int alphaBetaHashRec(int depth, int nextTurn, int alphaIN, int betaIN, int doneDepth) {
        //if search is not progressing, please return
        if (!searchInProgress)
            return UNKNOWN;
        //get validMoves
        List<Move> validMoves = board.getValidMoves(nextTurn);
        //check if it is the end of the game
        if (board.isTie(nextTurn, validMoves))
            return 0;
        if (board.isWin(nextTurn, validMoves)) //only next moving player can win the game
            return (WIN - 1);
        // key for this board
        long key = board.zobristCode(nextTurn);
        //evaluate if depth has been reached and search is still in progress
        if (depth <= 0) {
            //put it into the hashTable and return
            int value;
            //= makeQuies(board, nextTurn,alphaIN,betaIN,0);
            if (QUIES) {
                value = makeQuies(board, nextTurn, alphaIN, betaIN, 0);
                if (value == UNKNOWN)
                    value = evaluate(board, nextTurn);
            } else
                value = evaluate(board, nextTurn);
            EvaluatedMove putInEntry = new EvaluatedMove(key, null, EvaluatedMoveType.EXACT, value, 0);
            boardTable.put(key, putInEntry);
            return value;
        }
        //get the hashEntry which have the same key as the board
        EvaluatedMove retrievedEntry = boardTable.get(key);
        //check if we can return this value
        if (retrievedEntry != null) {
            //check if the depth makes us use this node effectively
            if (retrievedEntry.getKey() == key && depth <= retrievedEntry.getDepth()) {
                if (retrievedEntry.getType() == EvaluatedMoveType.EXACT) {
                    // we are making a cut from the exact table
                    return retrievedEntry.getValue();
                }
                if (retrievedEntry.getType() == EvaluatedMoveType.ALPHA && retrievedEntry.getValue() <= alphaIN) {
                    // we are making a cut from the alpha table
                    return alphaIN;
                }
                if (retrievedEntry.getType() == EvaluatedMoveType.BETA && betaIN <= retrievedEntry.getValue()) {
                    // we are making a cut from the beta table
                    return betaIN;
                }
            } else //else, if this move is in our list play it first
                if (retrievedEntry.getBestMove() != null) {
                    int size = validMoves.size();
                    for (int i = 0; i < size; i++)
                        if (retrievedEntry.getBestMove().equals(validMoves.get(i))) {
                            //move the first to be the first to be handled
                            Move importantMove = validMoves.remove(i);
                            validMoves.add(0, importantMove);
                            break;
                        }
                }
        }
        //start looking at the move list
        if (board.getTotalAliveCount(nextTurn) > SWITCH_STARTCOUNT)   ///consider switch moves only and only if there are less
            validMoves = board.getStandardValidMoves(nextTurn); //than SWITCH_STARTCOUNT left on the board
        if (validMoves.isEmpty())
            validMoves = board.getSwitchMoves(nextTurn);
        int best = (-WIN); //best move found so far, has the lowest number possible
        int value;                   //value of the child node, could be equal to anything in the beginning
        int alpha = alphaIN;             //alpha value for this level
        int beta = betaIN;               //beta value for this level
        Move bestMove = null;            //has to be null, if it still is null after the 'for' loop
        //we can detect that our algorithm is not working correctly
        EvaluatedMoveType hashType = EvaluatedMoveType.ALPHA; //this node is treated as an alpha node to begin
        //with
        boolean pvFound = false;        //primary variation
        // having updated move list we can begin alpha-beta
        for (Move validMove : validMoves)
            if (best < beta) {
                if (best > alpha) {
                    hashType = EvaluatedMoveType.EXACT; //this is an exact node
                    alpha = best;  //our alpha is the newly found value
                    pvFound = true; //pv node found
                    if (doneDepth == 1)
                        secondMove = bestMove;
                }
                board.makeGeneratedMove(validMove);
                //PRIMARY VARIATION OPTIMIZATION
                if (pvFound) {
                    value = (-1) * alphaBetaHashRec(depth - 1,
                            Helper.getOpponentColor(nextTurn), -alpha - 1, -alpha, 2);
                    if ((value > alpha) && (value < beta)) // Check for failure.
                        value = (-1) * alphaBetaHashRec(depth - 1,
                                Helper.getOpponentColor(nextTurn), -beta, -alpha, 2);
                } else
                    value = (-1) * alphaBetaHashRec(depth - 1,
                            Helper.getOpponentColor(nextTurn), -beta, -alpha, 2);
                board.undoMove(validMove);
                if (value == UNKNOWN) return UNKNOWN;
                if (value > best) {
                    best = value;
                    bestMove = validMove;           //if this is a better move, take this one

                }

            } else //beta has been found
            {
                hashType = EvaluatedMoveType.BETA;
                best = beta;
                bestMove = validMove;
                break;
            }
        //record to the hashTable before we return
        EvaluatedMove putInEntry = new EvaluatedMove(key, bestMove, hashType, best, depth);
        boardTable.put(key, putInEntry);
        if (searchInProgress)
            return best;
        return UNKNOWN;
    }

    /**
     * Standard Alpha-Beta algorithm, entrance method
     * <br><b> board</b> Board to make a search on
     * <br><b> depth</b> Depth of search
     * <br><b> nextTurn</b>next player to move
     * <br><b> alpha</b>lower term of alpha-beta
     * <br><b> beta</b> higher term of alpha-beta
     *
     * @throws RuntimeException if {@code requires} is not satisfied
     * @requires {@code !Piece.isGameOver() && (depth>0)}
     * @return the best move
     */
    public Move alphaBetaHash(int depth, int nextTurn, int alphaIN, int betaIN) {
        if (!searchInProgress)
            return null;
        //get validMoves
        List<Move> validMoves = board.getValidMoves(nextTurn);
        //check if it is the end of the game
        if (board.isGameOver(nextTurn, validMoves))
            throw new RuntimeException("BetterEngine.alphaBeta():Game is Over!");
        //if we have reached the end children, we evaluate the board
        //and return a value for it
        if (depth <= 0)
            throw new RuntimeException("BetterEngine.alphaBeta():Depth is non-positive ");
        //get the hashEntry which have the same key as the board
        long key = board.zobristCode(nextTurn);
        EvaluatedMove retrievedEntry = boardTable.get(key);
        //check if we can return this value
        if (retrievedEntry != null) {   //check if the depth makes us use this node effectively
            if (retrievedEntry.getKey() == key && depth <= retrievedEntry.getDepth() && retrievedEntry.getBestMove() != null) {
                // we are making a cut from the table
                //since this board exists in the hashTable, even if it is an alpha, beta or exact node
                //just check if we can make the suggested move and make it else
                //proceed normally
                boolean canMake = false;
                if (validMoves.contains(retrievedEntry.getBestMove()))
                    canMake = true;
                //if this move is in our validMove list then it means that we canMake this move
                if (canMake) {
                    return retrievedEntry.getBestMove();
                }

            } else //else, if this move is in our list play it first
                if (retrievedEntry.getBestMove() != null) {
                    int size = validMoves.size();
                    for (int i = 0; i < size; i++)
                        if (retrievedEntry.getBestMove().equals(validMoves.get(i))) {
                            //move the first to be the first to be handled
                            Move importantMove = validMoves.remove(i);
                            validMoves.add(0, importantMove);
                            break;
                        }
                }
        }
        //start looking at the move list
        if (board.getTotalAliveCount(nextTurn) > SWITCH_STARTCOUNT)   ///consider switch moves only and only if there are less
            validMoves = board.getStandardValidMoves(nextTurn); //than SWITCH_STARTCOUNT left on the board
        if (validMoves.isEmpty())
            validMoves = board.getSwitchMoves(nextTurn);
        int best = (-WIN); //best move found so far, has the lowest number possible
        int value;                   //value of the child node, could be equal to anything in the beginning
        int alpha = alphaIN;             //alpha value for this level
        int beta = betaIN;               //beta value for this level
        Move bestMove = null;            //has to be null, if it still is null after the 'for' loop
        //we can detect that our algorithm is not working correctly
        EvaluatedMoveType hashType = EvaluatedMoveType.ALPHA; //this node is treated as an alpha node to begin
        //with
        boolean pvFound = false;        //primary variation
        for (Move validMove : validMoves)
            if (best < beta) {
                if (best > alpha) {
                    hashType = EvaluatedMoveType.EXACT; //this is an exact node
                    alpha = best;  //our alpha is the newly found value
                    pvFound = true;

                }
                board.makeGeneratedMove(validMove); //make the next move and modify the board
                //PRIMARY VARIATION OPTIMIZATION
                if (pvFound) {
                    value = (-1) * alphaBetaHashRec(depth - 1,
                            Helper.getOpponentColor(nextTurn), -alpha - 1, -alpha, 1);
                    if ((value > alpha) && (value < beta)) // Check for failure.
                        value = (-1) * alphaBetaHashRec(depth - 1,
                                Helper.getOpponentColor(nextTurn), -beta, -alpha, 1);
                } else
                    value = (-1) * alphaBetaHashRec(depth - 1,
                            Helper.getOpponentColor(nextTurn), -beta, -alpha, 1);
                board.undoMove(validMove);  //undo the move, and revert back to initial board
                if (value == UNKNOWN) return null;
                if (value > best) {
                    best = value;
                    bestMove = validMove;
                }          //if this is a better move, take this one

            } else //beta has been found
            {
                bestMove = validMove;
                best = beta;
                hashType = EvaluatedMoveType.BETA;
                break;
            }
        //put this to the hashTable first
        EvaluatedMove putInEntry = new EvaluatedMove(key, bestMove, hashType, best, depth);
        boardTable.put(key, putInEntry);
        if (searchInProgress)
            return bestMove;
        return null;

    }

    public void run() {
        searchInProgress = true;
        searchFinished = false;
        List<Move> validMoves = board.getValidMoves(initialNextTurn);
        //don't spend time thinking about this guy // TODO number of valid moves should not be zero, assert
        if (validMoves.size() == 1) {
            foundMove = validMoves.get(0);
            searchFinished = true;
            searchInProgress = false;
            return;
        }
        //**Look into the hashTable first
        //get the hashEntry which have the same key as the board
        long key = board.zobristCode(initialNextTurn);
        EvaluatedMove retrievedEntry = boardTable.get(key);
        if (retrievedEntry != null && searchInProgress) {   //check if the depth makes us use this node effectively
            if (retrievedEntry.getKey() == key && initialDepth <= retrievedEntry.getDepth() && retrievedEntry.getBestMove() != null) {
                //since this board exists in the hashTable, even if it is an alpha, beta or exact node
                //just check if we can make the suggested move and make it else
                //proceed normally
                if (validMoves.contains(retrievedEntry.getBestMove())) {
                    secondMove = null;
                    foundMove = retrievedEntry.getBestMove();
                    searchFinished = true;
                    return;
                }

            }
        }
        foundMove = null;
        Move bestFromDepth;
        for (int depth = 1; depth <= initialDepth && searchInProgress; depth++) {
            System.out.println("BetterEngine.java: new depth:" + depth);
            bestFromDepth = alphaBetaHash(depth, initialNextTurn, -WIN, WIN);
            if (bestFromDepth != null)
                foundMove = bestFromDepth;
        }
        //if bestMove is null, there is an error in our algorithm
        if (foundMove == null) {
            foundMove = validMoves.get(0);
            System.out.println("bestMove found so far is NULL: BIG ERROR IN PROGRAM");
            // System.exit(0);
        }
        searchFinished = true;

    }
    //**   QUIESCENT SEARCH    **

    /**
     * Returns the quiescent search value
     */
    private int makeQuies(Board board, int nextTurn, int alphaIN, int betaIN, int depth) {
        if (!searchInProgress)
            return UNKNOWN;
        //check for checkmates first
        List<Move> validMoves = board.getValidMoves(nextTurn);   //generate all the moves
        //check if it is the end of the game
        if (board.isTie(nextTurn, validMoves))
            return 0;
        if (board.isWin(nextTurn, validMoves)) //only next moving player can win the game
            return (WIN - 1);
        //now that it is not a checkmate we can do quiescent search
        int alpha = alphaIN;
        int beta = betaIN;
        int value = evaluate(board, nextTurn);
        if (value >= beta)
            return beta;
        if (value > alpha)
            alpha = value;
        if (depth >= QUIES_DEPTH)
            return (value);
        for (Move validMove : validMoves)
            if (validMove.isCapture() && searchInProgress) {
                board.makeGeneratedMove(validMove); //make the next move and modify the board
                value = -makeQuies(board, Helper.getOpponentColor(nextTurn), -beta, -alpha, depth + 1);
                board.undoMove(validMove);  //undo the move, and revert back to initial board
                if (value >= beta)
                    return beta;
                if (value > alpha)
                    alpha = value;
            }
        return alpha;
    }

    /**
     * Our evaluate function
     */
    private int evaluate(Board board, int nextTurn) {
        return board.getDifference(nextTurn);
    }

    /**
     * This is to communicate the engine whether it should quit searching the searching process
     * WILL BE SET BY OUTSIDERS
     */
    public boolean isSearchInProgress() {
        return searchInProgress;
    }

    public void setSearchInProgress(boolean searchInProgress) {
        this.searchInProgress = searchInProgress;
    }

    /**
     * If searchFinished is true, foundMove is guaranteed to have a valid move
     * WILL BE SET BY BETTER ENGINE
     */
    public boolean isSearchFinished() {
        return searchFinished;
    }

    public void setSearchFinished(boolean searchFinished) {
        this.searchFinished = searchFinished;
    }

    /**
     * If searchFinished is true, foundMove is a good move, check if it is valid
     * WILL BE SET BY BETTER ENGINE
     */
    public Move getFoundMove() {
        return foundMove;
    }

    public void setFoundMove(Move foundMove) {
        this.foundMove = foundMove;
    }

    /**
     * If searchFinished is true, secondMove is a good move for opponent, check if it is valid
     * WILL BE SET BY BETTER ENGINE
     */
    public Move getSecondMove() {
        return secondMove;
    }

    public void setSecondMove(Move secondMove) {
        this.secondMove = secondMove;
    }
}
