/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.engine;

import org.antichess.model.Move;

/**
 *
 */
public class EvaluatedMove { // TODO is not really an evaluated move, but rather a full evaluated position

    private final Move bestMove;
    private final int value;
    private final EvaluatedMoveType type;
    private final int depth;
    private final long key;

    public EvaluatedMove(long key, Move bestMove, EvaluatedMoveType type, int value, int depth) {
        if (bestMove != null)
            this.bestMove = new Move(bestMove.toString()); // TODO this is to make it a copy, maybe Move should be immutable
        else this.bestMove = null;

        this.key = key;
        this.value = value;
        this.type = type;
        this.depth = depth;
    }

    //@returns key
    public long getKey() {
        return key;
    }

    //@returns bestMove
    public Move getBestMove() {
        return bestMove;
    }

    //@returns the type of the hash entry
    public EvaluatedMoveType getType() {
        return type;
    }

    //@returns value
    public int getValue() {
        return value;
    }

    //@returns depth of calculation that resulted in bestMOve
    public int getDepth() {
        return depth;
    }

    //@returns a string rep of this hashEntry
    public String toString() {
        return "i:" + (int) key + "k:" + key + ":M:" + bestMove + ":v:" + value + ":d:" + depth + ":t:" + type;
    }

}
