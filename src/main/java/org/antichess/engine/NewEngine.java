/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.engine;

import org.antichess.model.Board;
import org.antichess.model.Helper;
import org.antichess.model.Move;

import java.util.List;
import java.util.Map;

/**
 * NewEngine represents the player engine
 *
 * <p>
 * NewEngine is a class with static methods
 */
public final class NewEngine {

    private NewEngine() {
    }

    /**
     * Standard NegaMax algorithm, recursive method (for children nodes)
     * It is necessary to make a distinction between the entrance method which runs
     * on the root node and the recursive method which runs on children nodes.
     * This distinction allows us to return from the recursion if depth is given
     * less than 0 or if it is an endgame situation (which should be detected by GameController)
     * Furthermore, entrance method returns a move and modifies the board, meanwhile the
     * recursive method only returns integer values.
     *
     * @return best value found so far
     * @requires board!=null && player=={Piece.WHITE || Piece.BLACK}
     */
    private static int negaMaxRec(Board board, int depth, int nextTurn) {

        //get validMoves
        List<Move> validMoves = board.getValidMoves(nextTurn);

        //check if it is the end of the game
        if (board.isTie(nextTurn, validMoves))
            return 0;
        if (board.isWin(nextTurn, validMoves)) //only next moving player can win the game
            return (Integer.MAX_VALUE - 1);

        //if we have reached the end children, we evaluate the board
        //and return a value for it
        if (depth <= 0)
            return board.getDifference(nextTurn);

        //start looking at the move list
        int best = (-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
        int value;                   //value of the child node, could be equal to anything in the beginning

        for (Move validMove : validMoves) {
            board.makeGeneratedMove(validMove); //make the next move and modify the board

            value = (-1) * negaMaxRec(board, depth - 1,
                    Helper.getOpponentColor(nextTurn));

            if (value > best) best = value;                             //if this is a better move, take this one
            board.undoMove(validMove);          //undo the move, and revert back to initial board
        }

        return best; //return the best value

    }

    /**
     * Standard NegaMax algorithm, entrance method (for the root node)
     * <br><b> board</b> Board to make a search on
     * <br><b> depth</b> Depth of search
     * <br><b> nextTurn</b> next player to move
     *
     * @throws RuntimeException if {@code requires} is not satisfied
     * @requires {@code !Piece.isGameOver() && (depth>0)}
     * @modifies board using the best move
     * @return the best move
     */
    public static Move negaMax(Board board, int depth, int nextTurn) {

        //get validMoves
        List<Move> validMoves = board.getValidMoves(nextTurn);

        //check if it is the end of the game, if it is, it is an error
        if (board.isGameOver(nextTurn, validMoves))
            throw new RuntimeException("NewEngine.negaMax():Game is Over!");

        //if we have reached the end children in the entrance, it is an error
        if (depth <= 0)
            throw new RuntimeException("NewEngine.negaMax():Depth is non-positive ");

        //start looking at the move list
        int best = (-Integer.MAX_VALUE);  //best move found so far, has the lowest number possible
        int value;                    //value of the child node, could be equal to anything in the beginning
        int size = validMoves.size();     //size of the validMoves
        Move bestMove = null;

        //if there is only one move to make, do not think
        //you can think later, just make the move
        if (size == 1)
            bestMove = validMoves.get(0);
        else {

            for (Move validMove : validMoves) {
                board.makeGeneratedMove(validMove); //make the next move and modify the board

                value = (-1) * negaMaxRec(board, depth - 1,
                        Helper.getOpponentColor(nextTurn));

                if (value > best) {
                    best = value;
                    bestMove = validMove;
                }         //if this is a better move, take this one

                board.undoMove(validMove); //undo the move, and revert back to initial board
            }

        }

        //if bestMove is null, there is an error in our algorithm
        if (bestMove == null) {
            System.out.println("bestMove found so far is NULL: BIG ERROR IN PROGRAM");
            System.exit(0);
        }

        //make the best move found so far
        board.makeGeneratedMove(bestMove);
        return bestMove;
    }

    /**
     * Returns a random move from the valid moves of the player
     *
     * @requires {@code board instanceof Board && player={Piece.WHITE || Piece.BLACK}}
     * @modifies board using the best move
     * @return the best move
     */
    public static Move randomMove(Board board, int player) {

        //get validMoves
        List<Move> validMoves = board.getValidMoves(player);

        //choose a random move
        int moveIndex = (int) (validMoves.size() * Math.random());

        //take that move and apply it to the board
        //    Piece.makeGeneratedMove(board,m);
        return validMoves.get(moveIndex);
    }

    /**
     * Standard AlphaBeta algorithm, lower terms
     * It is necessary to make a distinction between the entrance method which runs
     * on the root node and the recursive method which runs on children nodes.
     * This distinction allows us to return from the recursion if depth is given
     * less than 0 or if it is an endgame situation (which should be detected by GameController)
     * Furthermore, entrance method returns a move and modifies the board, meanwhile the
     * recursive method only returns integer values.
     *
     * @return best value found so far
     * @requires board!=null && player=={Piece.WHITE || Piece.BLACK}
     */
    private static int alphaBetaRec(Board board, int depth, int nextTurn, int alphaIN, int betaIN) {

        //get validMoves
        List<Move> validMoves = board.getValidMoves(nextTurn);

        //check if it is the end of the game
        if (board.isTie(nextTurn, validMoves))
            return 0;
        if (board.isWin(nextTurn, validMoves)) //only next moving player can win the game
            return (Integer.MAX_VALUE - 1);

        //evaluate if depth has been reached
        if (depth <= 0) {
            return board.getDifference(nextTurn);
        }

        //start looking at the move list
        int best = (-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
        int value;                   //value of the child node, could be equal to anything in the beginning
        int alpha = alphaIN;             //alpha value for this level
        int beta = betaIN;               //beta value for this level

        for (Move validMove : validMoves)
            if (best < beta) {
                board.makeGeneratedMove(validMove);

                if (best > alpha) alpha = best;

                value = (-1) * alphaBetaRec(board, depth - 1,
                        Helper.getOpponentColor(nextTurn), -beta, -alpha);

                if (value > best) best = value;
                board.undoMove(validMove);
            }

        return best;

    }

    /**
     * Standard Alpha-Beta algorithm, entrance method
     * <br><b> board</b> Board to make a search on
     * <br><b> depth</b> Depth of search
     * <br><b> nextTurn</b>next player to move
     * <br><b> alpha</b>lower term of alpha-beta
     * <br><b> beta</b> higher term of alpha-beta
     *
     * @throws RuntimeException if {@code requires} is not satisfied
     * @requires {@code !Piece.isGameOver() && (depth>0)}
     * @modifies board using the best move
     * @return the best move
     */
    public static Move alphaBeta(Board board, int depth, int nextTurn, int alphaIN, int betaIN) {

        System.out.println("Started A-B  with depth:" + depth);
        //get validMoves
        List<Move> validMoves = board.getValidMoves(nextTurn);

        //check if it is the end of the game
        if (board.isGameOver(nextTurn, validMoves))
            throw new RuntimeException("NewEngine.alphaBeta():Game is Over!");

        //if we have reached the end children, we evaluate the board
        //and return a value for it
        if (depth <= 0)
            throw new RuntimeException("NewEngine.alphaBeta():Depth is non-positive ");

        //start looking at the move list
        int best = (-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
        int value;                   //value of the child node, could be equal to anything in the beginning
        int size = validMoves.size();    //size of the validMoves
        int alpha = alphaIN;             //alpha value for this level
        int beta = betaIN;               //beta value for this level
        Move bestMove = null;            //has to be null, if it still is null after the 'for' loop
        //we can detect that our algorithm is not working correctly

        //if there is only one move to make, do not think
        //you can think later, just make the move, only valid
        //at the first level
        if (size == 1)
            bestMove = validMoves.get(0);
        else {

            for (Move validMove : validMoves)
                if (best < beta) {
                    board.makeGeneratedMove(validMove); //make the next move and modify the board

                    if (best > alpha) alpha = best; //adjust alpha accordingly

                    value = (-1) * alphaBetaRec(board, depth - 1,
                            Helper.getOpponentColor(nextTurn), -beta, -alpha);

                    if (value > best) {
                        best = value;
                        bestMove = validMove;
                    }          //if this is a better move, take this one
                    board.undoMove(validMove);  //undo the move, and revert back to initial board
                }

        }
        //if bestMove is null, there is an error in our algorithm
        if (bestMove == null) {
            System.out.println("bestMove found so far is NULL: BIG ERROR IN PROGRAM");
            System.exit(0);
        }

        //apply the bestMove found so far to the board
        board.makeGeneratedMove(bestMove);
        return bestMove;

    }

    //                     ALHPA-BETA WITH HASHTABLE

    /**
     * Standard AlphaBeta algorithm, lower terms
     * It is necessary to make a distinction between the entrance method which runs
     * on the root node and the recursive method which runs on children nodes.
     * This distinction allows us to return from the recursion if depth is given
     * less than 0 or if it is an endgame situation (which should be detected by GameController)
     * Furthermore, entrance method returns a move and modifies the board, meanwhile the
     * recursive method only returns integer values.
     *
     * @return best value found so far
     * @requires board!=null && player=={Piece.WHITE || Piece.BLACK}
     */
    private static int alphaBetaHashRec(Board board, Map<Long, EvaluatedMove> boardTable, int depth, int nextTurn, int alphaIN, int betaIN) {

        //get validMoves
        List<Move> validMoves = board.getValidMoves(nextTurn);

        //check if it is the end of the game
        if (board.isTie(nextTurn, validMoves))
            return 0;
        if (board.isWin(nextTurn, validMoves)) //only next moving player can win the game
            return (Integer.MAX_VALUE - 1);

        //evaluate if depth has been reached
        if (depth <= 0) {
            //put it into the hashTable and return
            int value = board.getDifference(nextTurn);
            long key = board.zobristCode(nextTurn);
            EvaluatedMove putInEntry = new EvaluatedMove(key, null, EvaluatedMoveType.EXACT, value, 0);
            boardTable.put(key, putInEntry);
            return value;
        }

        //get the hashEntry which have the same key as the board
        long key = board.zobristCode(nextTurn);
        EvaluatedMove retrievedEntry = boardTable.get(key);

        //check if we can return this value
        if (retrievedEntry != null) {   //check if the depth makes us use this node effectively
            if (retrievedEntry.getKey() == key && depth <= retrievedEntry.getDepth()) {
                if (retrievedEntry.getType() == EvaluatedMoveType.EXACT) {
                    // we are making a cut from the table exact
                    return retrievedEntry.getValue();
                }
                if (retrievedEntry.getType() == EvaluatedMoveType.ALPHA && retrievedEntry.getValue() <= alphaIN) {
                    // we are making a cut from the table alpha
                    return alphaIN;
                }
                if (retrievedEntry.getType() == EvaluatedMoveType.BETA && betaIN <= retrievedEntry.getValue()) {
                    // we are making a cut from the table beta
                    return betaIN;
                }
            } else //else, if this move is in our list play it first
                if (retrievedEntry.getBestMove() != null) {
                    int size = validMoves.size();
                    for (int i = 0; i < size; i++)
                        if (retrievedEntry.getBestMove().equals(validMoves.get(i))) {
                            //move the first to be the first to be handled
                            Move importantMove = validMoves.remove(i);
                            validMoves.add(0, importantMove);
                            break;
                        }
                }
        }

        //start looking at the move list
        int best = (-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
        int value;                   //value of the child node, could be equal to anything in the beginning
        int alpha = alphaIN;             //alpha value for this level
        int beta = betaIN;               //beta value for this level
        Move bestMove = null;            //has to be null, if it still is null after the 'for' loop
        //we can detect that our algorithm is not working correctly

        EvaluatedMoveType hashType = EvaluatedMoveType.ALPHA; //this node is treated as an alpha node to begin
        //with

        // having updated move list we can begin alpha-beta
        for (Move validMove : validMoves)
            if (best < beta) {
                board.makeGeneratedMove(validMove);

                if (best > alpha) {
                    hashType = EvaluatedMoveType.EXACT; //this is an exact node
                    alpha = best;  //our alpha is the newly found value
                }

                value = (-1) * alphaBetaHashRec(board, boardTable, depth - 1,
                        Helper.getOpponentColor(nextTurn), -beta, -alpha);

                if (value > best) {
                    best = value;
                    bestMove = validMove;           //if this is a better move, take this one
                }

                board.undoMove(validMove);

            } else //beta has been found
            {
                hashType = EvaluatedMoveType.BETA;
                best = beta;
                break;
            }

        //record to the hashTable before we return
        EvaluatedMove putInEntry = new EvaluatedMove(key, bestMove, hashType, best, depth);
        boardTable.put(key, putInEntry);
        return best;

    }

    /**
     * Standard Alpha-Beta algorithm, entrance method
     * <br><b> board</b> Board to make a search on
     * <br><b> depth</b> Depth of search
     * <br><b> nextTurn</b>next player to move
     * <br><b> alpha</b>lower term of alpha-beta
     * <br><b> beta</b> higher term of alpha-beta
     *
     * @throws RuntimeException if {@code requires} is not satisfied
     * @requires {@code !Piece.isGameOver() && (depth>0)}
     * @modifies board using the best move
     * @return the best move
     */
    public static Move alphaBetaHash(Board board, Map<Long, EvaluatedMove> boardTable, int depth, int nextTurn, int alphaIN, int betaIN) {

        System.out.println("Started Hash with depth:" + depth);

        //get validMoves
        List<Move> validMoves = board.getValidMoves(nextTurn);

        //check if it is the end of the game
        if (board.isGameOver(nextTurn, validMoves))
            throw new RuntimeException("NewEngine.alphaBeta():Game is Over!");

        //if we have reached the end children, we evaluate the board
        //and return a value for it
        if (depth <= 0)
            throw new RuntimeException("NewEngine.alphaBeta():Depth is non-positive ");

        //get the hashEntry which have the same key as the board
        long key = board.zobristCode(nextTurn);
        EvaluatedMove retrievedEntry = boardTable.get(key);

        //check if we can return this value

        if (retrievedEntry != null) {   //check if the depth makes us use this node effectively
            if (retrievedEntry.getKey() == key && depth <= retrievedEntry.getDepth() && retrievedEntry.getBestMove() != null) {
                // we are making a cut from the table

                //since this board exists in the hashTable, even if it is an alpha, beta or exact node
                //just check if we can make the suggested move and make it else
                //proceed normally
                boolean canMake = false;
                for (Move validMove : validMoves)
                    if (retrievedEntry.getBestMove().equals(validMove)) {
                        canMake = true;
                        break;
                    }

                //if this move is in our validMove list then it means that we canMake this move
                if (canMake) {
                    Move bestMove = retrievedEntry.getBestMove();
                    //apply the bestMove found so far to the board
                    board.makeGeneratedMove(bestMove);
                    return bestMove;
                }

            } else //else, if this move is in our list play it first
                if (retrievedEntry.getBestMove() != null) {
                    int size = validMoves.size();
                    for (int i = 0; i < size; i++)
                        if (retrievedEntry.getBestMove().equals(validMoves.get(i))) {
                            //move the first to be the first to be handled
                            Move importantMove = validMoves.remove(i);
                            validMoves.add(0, importantMove);
                            break;
                        }
                }
        }

        //start looking at the move list
        int best = (-Integer.MAX_VALUE); //best move found so far, has the lowest number possible
        int value;                   //value of the child node, could be equal to anything in the beginning
        int size = validMoves.size();    //size of the validMoves
        int alpha = alphaIN;             //alpha value for this level
        int beta = betaIN;               //beta value for this level
        Move bestMove = null;            //has to be null, if it still is null after the 'for' loop
        //we can detect that our algorithm is not working correctly

        EvaluatedMoveType hashType = EvaluatedMoveType.ALPHA; //this node is treated as an alpha node to begin
        //with

        //if there is only one move to make, do not think
        //you can think later, just make the move
        if (size == 1)
            bestMove = validMoves.get(0);
        else {

            for (Move validMove : validMoves)
                if (best < beta) {
                    board.makeGeneratedMove(validMove); //make the next move and modify the board

                    if (best > alpha) {
                        hashType = EvaluatedMoveType.EXACT; //this is an exact node
                        alpha = best;  //our alpha is the newly found value
                    }

                    value = (-1) * alphaBetaHashRec(board, boardTable, depth - 1,
                            Helper.getOpponentColor(nextTurn), -beta, -alpha);

                    if (value > best) {
                        best = value;
                        bestMove = validMove;
                    }          //if this is a better move, take this one
                    board.undoMove(validMove);  //undo the move, and revert back to initial board
                } else //beta has been found
                {
                    hashType = EvaluatedMoveType.BETA;
                    break;
                }

        }

        //if bestMove is null, there is an error in our algorithm
        if (bestMove == null) {
            System.out.println("bestMove found so far is NULL: BIG ERROR IN PROGRAM");
            System.exit(0);
        }

        //put this to the hashTable first
        EvaluatedMove putInEntry = new EvaluatedMove(key, bestMove, hashType, best, depth);
        boardTable.put(key, putInEntry);

        //apply the bestMove found so far to the board
        board.makeGeneratedMove(bestMove);
        return bestMove;
    }
}
