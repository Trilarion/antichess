/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.engine;

import org.antichess.model.Board;
import org.antichess.model.Helper;
import org.antichess.model.Sides;
import org.antichess.model.pieces.PieceImplementation;
import org.antichess.model.pieces.PieceType;

/**
 * This class  assigns a numerical value to a board position for a particular player.
 * A positive score for a given player implies that the board is preferable for him to reach.
 * Similarly, a negative  score implies that the board is undesired.
 * A score of 0 suggests that the board does not give an advantage or disadvantage for that specific player.
 * Evaluator is immutable.
 */
public final class CEvaluator { // TODO difference to AEvaluator

    /*
     * @requires board.getColors().contain(colorToMove) && 0<=evalQuality<=2 && board is a valid board position according to rules of org.antichess
     * @modifies nothing
     * @return an integer value that assesses the board for the particular player.
     * More positive values are better for the player.
     */
    private static final int WHITE = 0;
    private static final int BLACK = 1;
    private static final int PAWN = 0;
    private static final int ROOK = 1;
    private static final int KNIGHT = 2;
    private static final int BISHOP = 3;
    private static final int QUEEN = 4;
    private static final int KING = 5;
    private static final int SPECIAL_MOVE_BONUS = 50;
    private static final int ISOLATED_PAWN_PENALTY = 50;
    private static final int DOUBLED_PAWN_PENALTY = 25;
    private static final int ROOK_OPEN_FILE_PENALTY = 15;
    private static final int ROOK_SEMI_OPEN_FILE_PENALTY = 25;
    private static final int ROOK_ON_SEVENTH_PENALTY = 15;
    private static final int PASSED_PAWN_PENALTY = 25;
    private static final int BLOCKED_ISOLATED_PAWN_BONUS = 50;
    private static final int[] piece_value = {100, 500, 300, 300, 900, 0};
    private static final int[] piece_value1 = {2, 5, 3, 3, 9, 0};
/* The "pcsq" arrays are piece/square tables. They're values
   added to the material value of the piece based on the
   location of the piece. */

    private static final int[] pawn_pcsq = {
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 0,
            2, 2, 2, 2, 2, 2, 2, 2,
            4, 4, 4, 4, 4, 4, 4, 4,
            6, 6, 6, 6, 6, 6, 6, 6,
            8, 8, 8, 8, 8, 8, 8, 8,
            10, 10, 10, 10, 10, 10, 10, 10,
            0, 0, 0, 0, 0, 0, 0, 0
    };

    private static final int[] knight_pcsq = {
            10, 10, 10, 10, 10, 10, 10, 10,
            0, 0, 0, 0, 0, 0, 0, 0,
            -5, -5, -5, -5, -5, -5, -5, -5,
            -10, -10, -10, -10, -10, -10, -10, -10,
            -10, -10, -10, -10, -10, -10, -10, -10,
            -5, -5, -5, -5, -5, -5, -5, -5,
            0, 0, 0, 0, 0, 0, 0, 0,
            10, 30, 10, 10, 10, 10, 30, 10
    };

    private static final int[] bishop_pcsq = {
            10, 10, 10, 10, 10, 10, 10, 10,
            0, 0, 0, 0, 0, 0, 0, 0,
            -5, -5, -5, -5, -5, -5, -5, -5,
            -10, -10, -10, -10, -10, -10, -10, -10,
            -10, -10, -10, -10, -10, -10, -10, -10,
            -5, -5, -5, -5, -5, -5, -5, -5,
            0, 0, 0, 0, 0, 0, 0, 0,
            10, 10, 20, 10, 10, 20, 10, 10
    };

    private static final int[] queen_pcsq = {
            -10, -10, -10, -10, -10, -10, -10, -10,
            0, 0, 0, 0, 0, 0, 0, 0,
            -5, -5, -5, -5, -5, -5, -5, -5,
            -15, -15, -15, -15, -15, -15, -15, -15,
            -15, -15, -15, -15, -15, -15, -15, -15,
            -5, -5, -5, -5, -5, -5, -5, -5,
            0, 0, 0, 0, 0, 0, 0, 0,
            0, 0, 0, 0, 0, 0, 0, 10
    };

    // king heuristic is redundant for org.antichess
    /**
     * private static final int[] king_pcsq= {
     * -40, -40, -40, -40, -40, -40, -40, -40,
     * -40, -40, -40, -40, -40, -40, -40, -40,
     * -40, -40, -40, -40, -40, -40, -40, -40,
     * -40, -40, -40, -40, -40, -40, -40, -40,
     * -40, -40, -40, -40, -40, -40, -40, -40,
     * -40, -40, -40, -40, -40, -40, -40, -40,
     * -20, -20, -20, -20, -20, -20, -20, -20,
     * 0,  20,  40, -20,   0, -20,  40,  20
     * };
     **/

/* The flip array is used to calculate the piece/square
   values for BLACK pieces. The piece/square value of a
   WHITE pawn is pawn_pcsq[sq] and the value of a DARK
   pawn is pawn_pcsq[flip[sq]] */
    private static final int[] flip = {
            56, 57, 58, 59, 60, 61, 62, 63,
            48, 49, 50, 51, 52, 53, 54, 55,
            40, 41, 42, 43, 44, 45, 46, 47,
            32, 33, 34, 35, 36, 37, 38, 39,
            24, 25, 26, 27, 28, 29, 30, 31,
            16, 17, 18, 19, 20, 21, 22, 23,
            8, 9, 10, 11, 12, 13, 14, 15,
            0, 1, 2, 3, 4, 5, 6, 7
    };

    /* pawn_leastRow[x][y] is the row of the least advanced pawn of color x on column
       y - 1. There are "buffer files" on the left and right to avoid special-case
       logic later. If there's no pawn on a rank, we pretend the pawn is
       impossibly far advanced (0 for WHITE and 7 for BLACK). This makes it easy to
       test for pawns on a rank and it simplifies some pawn evaluation code.
       It is important to note that last rank for white is 0, whereas for black it is 7.
       This naming is counter-intuitive at first, but all arrays above are represented this way.
       This matrix will be setup when evaluator loads up.
    */
    private static final int[][] pawn_leastRow = new int[2][10];

    // this array stores isolated pawn information
    private static final int[][] pawn_isolated = new int[2][10];

    private static final int[] piece_mat = new int[2];  /* the value of a side's pieces */
    private static final int[] pawn_mat = new int[2];  /* the value of a side's pawns */

    private CEvaluator() {
    }

    public static int evaluateBoard(Board board, int colorToMove, int evalQuality) {
        if (evalQuality == 0) {
            int fixedColumn;  /* column added 1 for convention */
            int[] score = new int[2];  /* each side's score. First entry is white as usual. The higher score for a side, the worse for him according to org.antichess.*/

            /* this is the first pass: set up pawn_leastRow, piece_mat, and pawn_mat. */
            for (int i = 0; i < 10; ++i) {
                pawn_leastRow[WHITE][i] = 0;
                pawn_leastRow[BLACK][i] = 7;
                pawn_isolated[WHITE][i] = 0;
                pawn_isolated[BLACK][i] = 0;
            }
            piece_mat[WHITE] = 0;
            piece_mat[BLACK] = 0;
            pawn_mat[WHITE] = 0;
            pawn_mat[BLACK] = 0;
            // now start assigning real values to these matrixes, initialize them first.
            PieceImplementation[][] whitePieces = board.getPiecesOnBoard(Sides.WHITE);
            PieceImplementation[][] blackPieces = board.getPiecesOnBoard(Sides.BLACK);

            // numbers of each type of each on board
            int numWhitePawns = board.getAliveCount(Sides.WHITE, PieceType.PAWN.getValue());
            int numWhiteRooks = board.getAliveCount(Sides.WHITE, PieceType.ROOK.getValue());
            int numWhiteBishops = board.getAliveCount(Sides.WHITE, PieceType.BISHOP.getValue());
            int numWhiteKnights = board.getAliveCount(Sides.WHITE, PieceType.KNIGHT.getValue());
            int numWhiteQueens = board.getAliveCount(Sides.WHITE, PieceType.QUEEN.getValue());
            int numWhiteKings = board.getAliveCount(Sides.WHITE, PieceType.KING.getValue());
            int numBlackPawns = board.getAliveCount(Sides.BLACK, PieceType.PAWN.getValue());
            int numBlackRooks = board.getAliveCount(Sides.BLACK, PieceType.ROOK.getValue());
            int numBlackBishops = board.getAliveCount(Sides.BLACK, PieceType.BISHOP.getValue());
            int numBlackKnights = board.getAliveCount(Sides.BLACK, PieceType.KNIGHT.getValue());
            int numBlackQueens = board.getAliveCount(Sides.BLACK, PieceType.QUEEN.getValue());
            int numBlackKings = board.getAliveCount(Sides.BLACK, PieceType.KING.getValue());
            int numTotalPieces = numWhitePawns + numWhiteRooks + numWhiteBishops + numWhiteKnights + numWhiteQueens + numWhiteKings +
                    numBlackPawns + numBlackRooks + numBlackBishops + numBlackKnights + numBlackQueens + numBlackKings;

            // piece arrays of each piece on board
            PieceImplementation[] whitePawns = whitePieces[PieceType.PAWN.getValue()];
            PieceImplementation[] whiteRooks = whitePieces[PieceType.ROOK.getValue()];
            PieceImplementation[] whiteBishops = whitePieces[PieceType.BISHOP.getValue()];
            PieceImplementation[] whiteKnights = whitePieces[PieceType.KNIGHT.getValue()];
            PieceImplementation[] whiteQueens = whitePieces[PieceType.QUEEN.getValue()];
            PieceImplementation[] whiteKings = whitePieces[PieceType.KING.getValue()];
            PieceImplementation[] blackPawns = blackPieces[PieceType.PAWN.getValue()];
            PieceImplementation[] blackRooks = blackPieces[PieceType.ROOK.getValue()];
            PieceImplementation[] blackBishops = blackPieces[PieceType.BISHOP.getValue()];
            PieceImplementation[] blackKnights = blackPieces[PieceType.KNIGHT.getValue()];
            PieceImplementation[] blackQueens = blackPieces[PieceType.QUEEN.getValue()];
            PieceImplementation[] blackKings = blackPieces[PieceType.KING.getValue()];

            // maximum length of these piece arrays on board
            int numMaxWhitePawns = board.getPieceCount(Sides.WHITE, PieceType.PAWN.getValue());
            int numMaxWhiteRooks = board.getPieceCount(Sides.WHITE, PieceType.ROOK.getValue());
            int numMaxWhiteBishops = board.getPieceCount(Sides.WHITE, PieceType.BISHOP.getValue());
            int numMaxWhiteKnights = board.getPieceCount(Sides.WHITE, PieceType.KNIGHT.getValue());
            int numMaxWhiteQueens = board.getPieceCount(Sides.WHITE, PieceType.QUEEN.getValue());
            int numMaxWhiteKings = board.getPieceCount(Sides.WHITE, PieceType.KING.getValue());
            int numMaxBlackPawns = board.getPieceCount(Sides.BLACK, PieceType.PAWN.getValue());
            int numMaxBlackRooks = board.getPieceCount(Sides.BLACK, PieceType.ROOK.getValue());
            int numMaxBlackBishops = board.getPieceCount(Sides.BLACK, PieceType.BISHOP.getValue());
            int numMaxBlackKnights = board.getPieceCount(Sides.BLACK, PieceType.KNIGHT.getValue());
            int numMaxBlackQueens = board.getPieceCount(Sides.BLACK, PieceType.QUEEN.getValue());
            int numMaxBlackKings = board.getPieceCount(Sides.BLACK, PieceType.KING.getValue());

            for (int i = 0; i < numMaxWhitePawns && whitePawns[i] != null; i++) {
                PieceImplementation currWhitePawn = whitePawns[i];
                pawn_mat[WHITE] += piece_value[PAWN];
                fixedColumn = currWhitePawn.getCol() + 1;  // add 1 because of the extra column in the array
                if (pawn_leastRow[WHITE][fixedColumn] < (7 - currWhitePawn.getRow())) {
                    pawn_leastRow[WHITE][fixedColumn] = 7 - currWhitePawn.getRow();
                }
            }
            for (int i = 0; i < numMaxBlackPawns && blackPawns[i] != null; i++) {
                PieceImplementation currBlackPawn = blackPawns[i];
                pawn_mat[BLACK] += piece_value[PAWN];
                fixedColumn = currBlackPawn.getCol() + 1;  // add 1 because of the extra column in the array
                if (pawn_leastRow[BLACK][fixedColumn] > (7 - currBlackPawn.getRow())) {
                    pawn_leastRow[BLACK][fixedColumn] = 7 - currBlackPawn.getRow();
                }
            }

            piece_mat[WHITE] += piece_value[ROOK] * board.getAliveCount(Sides.WHITE, PieceType.ROOK.getValue());
            piece_mat[WHITE] += piece_value[BISHOP] * board.getAliveCount(Sides.WHITE, PieceType.BISHOP.getValue());
            piece_mat[WHITE] += piece_value[KNIGHT] * board.getAliveCount(Sides.WHITE, PieceType.KNIGHT.getValue());
            piece_mat[WHITE] += piece_value[QUEEN] * board.getAliveCount(Sides.WHITE, PieceType.QUEEN.getValue());
            piece_mat[BLACK] += piece_value[ROOK] * board.getAliveCount(Sides.BLACK, PieceType.ROOK.getValue());
            piece_mat[BLACK] += piece_value[BISHOP] * board.getAliveCount(Sides.BLACK, PieceType.BISHOP.getValue());
            piece_mat[BLACK] += piece_value[KNIGHT] * board.getAliveCount(Sides.BLACK, PieceType.KNIGHT.getValue());
            piece_mat[BLACK] += piece_value[QUEEN] * board.getAliveCount(Sides.BLACK, PieceType.QUEEN.getValue());

            // The following will be the second run that will take square heuristics into account.
            // Depending on the position of the piece, a new value will be added or subtracted from total score.
            score[WHITE] = piece_mat[WHITE] + pawn_mat[WHITE];
            score[BLACK] = piece_mat[BLACK] + pawn_mat[BLACK];

            for (int i = 0; i < numMaxWhitePawns && whitePawns[i] != null; i++) {// this code can be added to the first pawn for loop above to improve performance. Add it later on.
                PieceImplementation currWhitePawn = whitePawns[i];
                score[WHITE] += eval_white_pawn(currWhitePawn);
            }
            // System.out.println ("White score after whitepawn heuristic: " + score[CEvaluator.WHITE]);

            for (int i = 0; i < numMaxBlackPawns && blackPawns[i] != null; i++) {// this code can be added to the first pawn for loop above to improve performance. Add it later on.
                PieceImplementation currBlackPawn = blackPawns[i];
                score[BLACK] += eval_black_pawn(currBlackPawn);
            }
            //System.out.println ("Black score after blackpawn heuristic: " + score[CEvaluator.BLACK]);

            for (int i = 0; i < numMaxWhiteRooks && whiteRooks[i] != null; i++) {// add open file, semi-open file, and 7th rank penalties
                PieceImplementation currWhiteRook = whiteRooks[i];
                int currColumn = currWhiteRook.getCol();
                int currRow = currWhiteRook.getRow();
                if (pawn_leastRow[WHITE][currColumn + 1] == 0) {
                    if (pawn_leastRow[BLACK][currColumn + 1] == 7) {
                        score[WHITE] += ROOK_OPEN_FILE_PENALTY;
                    } else {
                        score[WHITE] += ROOK_SEMI_OPEN_FILE_PENALTY;
                    }
                }
                if (currRow == 6) {
                    score[WHITE] += ROOK_ON_SEVENTH_PENALTY;
                }
            }

            for (int i = 0; i < numMaxWhiteBishops && whiteBishops[i] != null; i++) {
                PieceImplementation currWhiteBishop = whiteBishops[i];
                int currColumn = currWhiteBishop.getCol() + 1;
                int fixedRow = 7 - currWhiteBishop.getRow();
                int sq = currWhiteBishop.returnSquareValue();
                score[WHITE] -= bishop_pcsq[sq];
                if ((pawn_isolated[BLACK][currColumn] == 1)
                        && pawn_leastRow[BLACK][currColumn] < fixedRow) {
                    score[WHITE] -= BLOCKED_ISOLATED_PAWN_BONUS;
                }
            }

            for (int i = 0; i < numMaxWhiteKnights && whiteKnights[i] != null; i++) {
                PieceImplementation currWhiteKnight = whiteKnights[i];
                int currColumn = currWhiteKnight.getCol() + 1;
                int fixedRow = 7 - currWhiteKnight.getRow();
                int sq = currWhiteKnight.returnSquareValue();
                score[WHITE] -= knight_pcsq[sq];
                if ((pawn_isolated[BLACK][currColumn] == 1)
                        && pawn_leastRow[BLACK][currColumn] > fixedRow) {
                    score[WHITE] -= BLOCKED_ISOLATED_PAWN_BONUS;
                }

            }
            for (int i = 0; i < numMaxWhiteQueens && whiteQueens[i] != null; i++) {
                PieceImplementation currWhiteQueen = whiteQueens[i];
                int sq = currWhiteQueen.returnSquareValue();
                score[WHITE] -= queen_pcsq[sq];
            }

            for (int i = 0; i < numMaxBlackRooks && blackRooks[i] != null; i++) {// add open file, semi-open file, and 7th rank penalties
                PieceImplementation currBlackRook = blackRooks[i];
                int currColumn = currBlackRook.getCol();
                int currRow = currBlackRook.getRow();
                if (pawn_leastRow[BLACK][currColumn + 1] == 7) {
                    if (pawn_leastRow[WHITE][currColumn + 1] == 0) {
                        score[BLACK] += ROOK_OPEN_FILE_PENALTY;
                    } else {
                        score[BLACK] += ROOK_SEMI_OPEN_FILE_PENALTY;
                    }
                }
                if (currRow == 1) {
                    score[BLACK] += ROOK_ON_SEVENTH_PENALTY;
                }
            }

            for (int i = 0; i < numMaxBlackBishops && blackBishops[i] != null; i++) {

                PieceImplementation currBlackBishop = blackBishops[i];
                int currColumn = currBlackBishop.getCol() + 1;
                int fixedRow = 7 - currBlackBishop.getRow();
                int sq = currBlackBishop.returnSquareValue();
                score[BLACK] -= bishop_pcsq[flip[sq]];
                if ((pawn_isolated[WHITE][currColumn] == 1)
                        && pawn_leastRow[WHITE][currColumn] > fixedRow) {
                    score[BLACK] -= BLOCKED_ISOLATED_PAWN_BONUS;
                }

            }

            for (int i = 0; i < numMaxBlackKnights && blackKnights[i] != null; i++) {
                PieceImplementation currBlackKnight = blackKnights[i];
                int currColumn = currBlackKnight.getCol() + 1;
                int fixedRow = 7 - currBlackKnight.getRow();
                int sq = currBlackKnight.returnSquareValue();
                score[BLACK] -= knight_pcsq[flip[sq]];
                if ((pawn_isolated[WHITE][currColumn] == 1)
                        && pawn_leastRow[WHITE][currColumn] > fixedRow) {
                    score[BLACK] -= BLOCKED_ISOLATED_PAWN_BONUS;
                }
            }

            for (int i = 0; i < numMaxBlackQueens && blackQueens[i] != null; i++) {
                PieceImplementation currBlackQueen = blackQueens[i];
                int sq = currBlackQueen.returnSquareValue();
                score[BLACK] -= queen_pcsq[flip[sq]];
            }

            // add special move heuristics

            if (numTotalPieces < 16) {
                if (!board.getSpecial(Sides.WHITE)) {
                    score[WHITE] -= SPECIAL_MOVE_BONUS;
                }
                if (!board.getSpecial(Sides.BLACK)) {
                    score[BLACK] -= SPECIAL_MOVE_BONUS;
                }
            }

            // score array is finalized, now return the relative answer depending on turn side

            if (colorToMove == Sides.WHITE) {
                return score[BLACK] - score[WHITE];
            } else {
                return score[WHITE] - score[BLACK];
            }
        }

        if (evalQuality == 1) {
            // a basic weighted sum of piece values will be returned at the most basic evaluator
            int oppColor = Helper.getOpponentColor(colorToMove);
            int numSelfPawns = board.getAliveCount(colorToMove, PieceType.PAWN.getValue());
            int numSelfRooks = board.getAliveCount(colorToMove, PieceType.ROOK.getValue());
            int numSelfBishops = board.getAliveCount(colorToMove, PieceType.BISHOP.getValue());
            int numSelfKnights = board.getAliveCount(colorToMove, PieceType.KNIGHT.getValue());
            int numSelfQueens = board.getAliveCount(colorToMove, PieceType.QUEEN.getValue());
            int numSelfKings = board.getAliveCount(colorToMove, PieceType.KING.getValue());
            int numOppPawns = board.getAliveCount(oppColor, PieceType.PAWN.getValue());
            int numOppRooks = board.getAliveCount(oppColor, PieceType.ROOK.getValue());
            int numOppBishops = board.getAliveCount(oppColor, PieceType.BISHOP.getValue());
            int numOppKnights = board.getAliveCount(oppColor, PieceType.KNIGHT.getValue());
            int numOppQueens = board.getAliveCount(oppColor, PieceType.QUEEN.getValue());
            int numOppKings = board.getAliveCount(oppColor, PieceType.KING.getValue());
            int numTotalPieces = numSelfPawns + numSelfRooks + numSelfBishops + numSelfKnights + numSelfQueens + numSelfKings + numOppPawns + numOppRooks + numOppBishops + numOppKnights + numOppQueens + numOppKings;

            int result =
                    piece_value1[PAWN] * (numOppPawns - numSelfPawns) +
                            piece_value1[ROOK] * (numOppRooks - numSelfRooks) +
                            piece_value1[KNIGHT] * (numOppKnights - numSelfKnights) +
                            piece_value1[BISHOP] * (numOppBishops - numSelfBishops) +
                            piece_value1[QUEEN] * (numOppQueens - numSelfQueens);

            if (numTotalPieces < 16) {
                if (!board.getSpecial(colorToMove)) {
                    result++;
                }
                if (!board.getSpecial(oppColor)) {
                    result--;
                }
            }
            // most basic quality of search should return this sum
            return result;
        }

        return board.getDifference(colorToMove);
    }

    //@requires whitePawn.getColor()==Piece.WHITE &&whitePawn.getType()==Piece.PAWN
    //@returns the evaluation for the pawn using extra pawn heuristics
    private static int eval_white_pawn(PieceImplementation whitePawn) {

        int returnValue;  // the value to return
        int column;  // the pawn's file(column)
        int row; // the pawn's row
        int sq; // pawn's square on matrix representation as explained above
        returnValue = 0;
        column = whitePawn.getCol() + 1;// fixing column value since we are using 10by2 array.
        row = 7 - whitePawn.getRow();
        sq = whitePawn.returnSquareValue();
        //System.out.println ("eval_white_pawn: sq: " + sq);

        // initial value assignment from array lookup
        returnValue -= pawn_pcsq[sq];

        //System.out.println ("eval_white_pawn: returnValue: " + returnValue);
    /* if there's a pawn behind this one, it's doubled
     watch out for reserve representation logic again. */
        if (pawn_leastRow[WHITE][column] > row)
            returnValue += DOUBLED_PAWN_PENALTY;
        //System.out.println ("eval_white_pawn: returnValue after doubled_pawn_penalty: " + returnValue);
    
    
    /* if there aren't any friendly pawns on either side of
       this one, it's isolated. This function is the reason why we have made a 10by2 array.
    */
        if ((pawn_leastRow[WHITE][column - 1] == 0) &&
                (pawn_leastRow[WHITE][column + 1] == 0)) {
            returnValue += ISOLATED_PAWN_PENALTY;
            pawn_isolated[WHITE][column] = 1;
        }
        //System.out.println ("eval_white_pawn: returnValue after isolated_pawn_penalty: " + returnValue);

        /* add a penalty if the pawn is passed and is going for queen*/
        if ((pawn_leastRow[BLACK][column - 1] >= row) &&
                (pawn_leastRow[BLACK][column] >= row) &&
                (pawn_leastRow[BLACK][column + 1] >= row))
            returnValue += (7 - row) * PASSED_PAWN_PENALTY;
        //System.out.println ("eval_white_pawn: returnValue after passed_pawn_penalty: " + returnValue);

        return returnValue;
    }

    //@requires blackPawn.getColor()==Piece.BLACK &&blackPawn.getType()==Piece.PAWN
    //@returns the evaluation for the pawn using extra pawn heuristics
    private static int eval_black_pawn(PieceImplementation blackPawn) {
        int returnValue;  // the value to return
        int column;  // the pawn's file(column)
        int row; // the pawn's row
        int sq; // pawn's square on matrix representation as explained above
        returnValue = 0;
        column = blackPawn.getCol() + 1;// fixing to adjust to 10by2 array
        row = 7 - blackPawn.getRow();
        sq = blackPawn.returnSquareValue();
        //System.out.println ("eval_black_pawn: sq: " + sq);
        // initial value assignment from array lookup
        returnValue -= pawn_pcsq[flip[sq]];
        //System.out.println ("eval_black_pawn: returnvalue: " + returnValue);
    
    /* if there's a pawn behind this one, it's doubled
       watch out for reserve representation logic again. */
        if (pawn_leastRow[BLACK][column] < row)
            returnValue += DOUBLED_PAWN_PENALTY;
        //System.out.println ("eval_black_pawn: returnvalue after doubled_pawn_penalty: " + returnValue);
    
    /* if there aren't any friendly pawns on either side of
       this one, it's isolated. This function is the reason why we have made a 10by2 array.
    */
        if ((pawn_leastRow[BLACK][column - 1] == 7) &&
                (pawn_leastRow[BLACK][column + 1] == 7)) {
            returnValue += ISOLATED_PAWN_PENALTY;
            pawn_isolated[BLACK][column] = 1;
        }
        //System.out.println ("eval_black_pawn: returnvalue after isolated_pawn_penalty: " + returnValue);

        /* add a penalty if the pawn is passed and is going for queen*/
        if ((pawn_leastRow[WHITE][column - 1] <= row) &&
                (pawn_leastRow[WHITE][column] <= row) &&
                (pawn_leastRow[WHITE][column + 1] <= row))
            // the closer to final line the worse
            returnValue += row * PASSED_PAWN_PENALTY;
        //System.out.println ("eval_black_pawn: returnvalue after passed_pawn_penalty: " + returnValue);

        return returnValue;
    }

}
