/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.ui;

import org.antichess.controller.Controller;
import org.antichess.controller.Message;
import org.antichess.controller.MessageType;
import org.antichess.model.*;
import org.antichess.utils.Observer;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

/**
 * The main frame of the application.
 */
public class MainFrame extends JFrame implements Observer<Message> { // TODO undo, resign, offer draw capabilities?

    private final JDialog chessOrachessDialog;
    private final ChessBoardPane chessBoardPanel;
    private final JRadioButton chessButton;
    private final JRadioButton antiChessButton;
    private final javax.swing.Timer chessTimer;
    private final JMenu newMenu;
    private final JMenuItem openMenuItem;
    private final JMenuItem saveMenuItem;
    private final JDialog helpDialog;
    private final TwoPlayerDialog gameDialog;
    private final JDialog settingsDialog;
    private final FourPlayerDialog fourPlayerDialog;
    private final PlayerInformationPanel playerInformationPanel;
    private Controller controller; // TODO make controller a singleton
    private Thread onGoingGame;
    private int moveCount;
    private boolean showTime;
    private boolean showMoves;
    private boolean showPieces;

    /**
     * Creates new form GraphicUI1
     */
    public MainFrame() {

        // Initialized the default values of various variables
        TextFields txt = new TextFields("english");

        getContentPane().setLayout(new GridBagLayout());
        setTitle(TextFields.ProgramName + TextFields.Version);
        setIconImage((new ImageIcon(MainFrame.class.getResource("/data/artwork/icon.gif"))).getImage());
        setResizable(false);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                exitApplication(evt);
            }
        });

        controller = null;
        onGoingGame = null;

        showTime = false;
        showMoves = false;

        // chess timer
        ActionListener updateTime = new ActionListener() {
            public void actionPerformed(ActionEvent evt) {
                if (controller != null && onGoingGame != null) {
                    playerInformationPanel.getWhiteplayertimeTextField().setText(controller.getTimeLeftString(Sides.WHITE));
                    playerInformationPanel.getBlackplayertimeTextField().setText(controller.getTimeLeftString(Sides.BLACK));
                    playerInformationPanel.getRedplayertimeTextField().setText(controller.getTimeLeftString(Sides.RED));
                    playerInformationPanel.getBlueplayertimeTextField().setText(controller.getTimeLeftString(Sides.BLUE));
                }
            }
        };
        // chess timer
        chessTimer = new Timer(1000, updateTime);

        // chess board pane
        chessBoardPanel = new ChessBoardPane();
        chessBoardPanel.setBackground(new Color(184, 184, 244));
        chessBoardPanel.setPreferredSize(new Dimension(625, 625));
        chessBoardPanel.setLayout(null);

        // add chessboard pane to pane
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        getContentPane().add(chessBoardPanel, gridBagConstraints);

        // side panel (player information)
        playerInformationPanel = new PlayerInformationPanel();


        // add side panel to pane
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridheight = 2;
        gridBagConstraints.fill = GridBagConstraints.VERTICAL;
        getContentPane().add(playerInformationPanel, gridBagConstraints);

        // panel
        JPanel jPanel3 = new JPanel();
        jPanel3.setBackground(new Color(204, 214, 204));
        jPanel3.setPreferredSize(new Dimension(425, 37));

        JButton jButton1 = new JButton("Offer Draw");
        jButton1.setEnabled(false);
        jButton1.setVisible(false);
        jPanel3.add(jButton1);

        JButton exitGameButton = new JButton();
        exitGameButton.setText("Exit This Game");
        exitGameButton.addActionListener(evt -> exitGameButtonActionPerformed("User Exit"));
        jPanel3.add(exitGameButton);

        JButton jButton3 = new JButton("Undo Move");
        jButton3.setEnabled(false);
        jButton3.setVisible(false);
        jPanel3.add(jButton3);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        getContentPane().add(jPanel3, gridBagConstraints);

        // chessOrachessDialog // TODO not used apparently
        chessOrachessDialog = new JDialog(this, true);
        chessOrachessDialog.setSize(new Dimension(250, 160));
        chessOrachessDialog.getContentPane().setLayout(new FlowLayout());
        chessOrachessDialog.setTitle("Choose what to play");

        // panel
        JPanel jPanelAC = new JPanel();
        jPanelAC.setLayout(new BoxLayout(jPanelAC, BoxLayout.Y_AXIS));

        JPanel jPanelAC3 = new JPanel();
        jPanelAC3.setLayout(new GridLayout(3, 0, 10, 0));
        chessButton = new JRadioButton("play chess");
        chessButton.setSelected(false);
        chessButton.addActionListener(this::chessButtonActionPerformed);
        antiChessButton = new JRadioButton("play antichess");
        antiChessButton.setSelected(true);
        antiChessButton.addActionListener(this::antiChessButtonActionPerformed);
        jPanelAC3.add(chessButton);
        jPanelAC3.add(antiChessButton);
        jPanelAC.add(jPanelAC3);

        JPanel jPanelAC2 = new JPanel();
        JButton chessOrachessOkButton = new JButton(TextFields.OK);
        chessOrachessOkButton.addActionListener(evt -> chessOrachessDialog.setVisible(false));
        JButton chessOrachessCancelButton = new JButton(TextFields.Cancel);
        chessOrachessCancelButton.addActionListener(this::chessOrachessCancelButtonActionPerformed);
        jPanelAC2.add(chessOrachessOkButton);
        jPanelAC2.add(chessOrachessCancelButton);
        jPanelAC.add(jPanelAC2);

        // add panel to dialog
        chessOrachessDialog.getContentPane().add(jPanelAC);

        // four player game dialog
        fourPlayerDialog = new FourPlayerDialog(this, true);
        fourPlayerDialog.setSize(new Dimension(320, 360));

        // help dialog
        helpDialog = new HelpDialog(this);

        // settings dialog // extract own class
        settingsDialog = new SettingsDialog(this, true);

        // game settings dialog
        gameDialog = new TwoPlayerDialog(this);
        gameDialog.setOkActionListener(this::startTwoPlayerGameButtonActionPerformed);

        // top level menu bar
        JMenuBar menuBar = new JMenuBar();

        JMenuItem chessOrachessItem = new JMenuItem("Chess or AChess");
        chessOrachessItem.addActionListener(evt -> chessOrachessDialog.setVisible(true));

        // arranges the file menu
        JMenu fileMenu = new JMenu(TextFields.Game);

        // new game menu
        newMenu = new JMenu(TextFields.NewGame);

        JMenuItem newGameTwoPlayerMenuItem = new JMenuItem(TextFields.TwoPlayers);
        newGameTwoPlayerMenuItem.addActionListener(this::newGameTwoPlayerMenuItemActionPerformed);
        JMenuItem newGameFourPlayerMenuItem = new JMenuItem(TextFields.FourPlayers);
        newGameFourPlayerMenuItem.addActionListener(evt -> fourPlayerDialog.setVisible(true));

        // add sub menus to new game menu and new game menu to file menu
        newMenu.add(newGameTwoPlayerMenuItem);
        newMenu.add(newGameFourPlayerMenuItem);
        fileMenu.add(newMenu);

        final JFileChooser jFileChooser = new JFileChooser();

        // open game menu
        openMenuItem = new JMenuItem(TextFields.OpenGame);
        openMenuItem.addActionListener(evt -> openMenuItemActionPerformed(evt, jFileChooser));
        fileMenu.add(openMenuItem);

        // save game menu
        saveMenuItem = new JMenuItem(TextFields.SaveGame);
        saveMenuItem.addActionListener(evt -> saveMenuItemActionPerformed(evt, jFileChooser));
        fileMenu.add(saveMenuItem);

        // separator
        fileMenu.add(new JSeparator());

        // settings menu
        JMenuItem settingsMenuItem = new JMenuItem(TextFields.Settings);
        settingsMenuItem.addActionListener(evt -> settingsDialog.setVisible(true));
        fileMenu.add(settingsMenuItem);

        // separator
        fileMenu.add(new JSeparator());

        // exit menu
        JMenuItem exitMenuItem = new JMenuItem(TextFields.Quit);
        exitMenuItem.addActionListener(evt -> dispatchEvent(new WindowEvent(this, WindowEvent.WINDOW_CLOSING)));
        fileMenu.add(exitMenuItem);

        // add file menu to menu bar
        menuBar.add(fileMenu);

        // arrange help menu
        JMenu helpMenu = new JMenu(TextFields.Help);

        // content menu
        JMenuItem contentsMenuItem = new JMenuItem(TextFields.Contents);
        contentsMenuItem.addActionListener(evt -> helpDialog.setVisible(true));
        helpMenu.add(contentsMenuItem);

        // about menu
        JMenuItem aboutMenuItem = new JMenuItem(TextFields.About);
        aboutMenuItem.addActionListener(evt -> JOptionPane.showMessageDialog(null, "", TextFields.About, JOptionPane.PLAIN_MESSAGE, new ImageIcon(MainFrame.class.getResource("/data/artwork/about.gif"))));
        helpMenu.add(aboutMenuItem);

        // add help menu to menu bar
        menuBar.add(helpMenu);

        // set menu bar
        setJMenuBar(menuBar);

        // pack and ready
        pack();
    }

    /**
     * Finishes a game in an orderly fashion
     */
    private void exitGameButtonActionPerformed(String reason) {
        //stop the game first
        //gc.stopGame();
        if (onGoingGame == null)
            return;

        chessBoardPanel.updateBoard();//board has been changed

        controller.setGameInProgress(false);
        //onGoingGame.destroy(); //SHOULD BE REPLACED
        onGoingGame = null;

        //stop the timer
        chessTimer.stop();

        //stop observing what might be happening in the game
        if (controller != null) controller.removeObserver(this);

        //print an informative menu on the screen
        JOptionPane.showMessageDialog(chessBoardPanel, "Game Over: " + reason);

        //disassociate the board with the game controller
        chessBoardPanel.setGameController(null);

        //disable newMenuItem and openMenuItem
        newMenu.setEnabled(true);
        openMenuItem.setEnabled(true);
        saveMenuItem.setEnabled(true);

        //trash the game controller
        controller = null;

        //clean the move table
        playerInformationPanel.getjTextArea1().setText("         Moves");

        //clean jpanel4 and jpanel5, bc this could have been a 4player game

        playerInformationPanel.getWhiteplayercolorLabel().setBackground(new java.awt.Color(194, 204, 204));
        playerInformationPanel.getBlackplayercolorLabel().setBackground(new java.awt.Color(204, 174, 174));
        playerInformationPanel.getRedplayernameLabel().setBackground(new java.awt.Color(255, 0, 51));
        playerInformationPanel.getBlueplayercolorLabel().setBackground(new java.awt.Color(225, 133, 225));

        playerInformationPanel.getRedplayercolorLabel().setVisible(false);
        playerInformationPanel.getRedplayernameLabel().setVisible(false);
        playerInformationPanel.getRedplayertimeTextField().setVisible(false);
        playerInformationPanel.getBlueplayercolorLabel().setVisible(false);
        playerInformationPanel.getBlueplayernameLabel().setVisible(false);
        playerInformationPanel.getBlueplayertimeTextField().setVisible(false);
    }

    private void chessOrachessCancelButtonActionPerformed(ActionEvent evt) {
        chessOrachessDialog.setVisible(false);
        chessButton.setSelected(true);
        antiChessButton.setSelected(false);
    }

    private void chessButtonActionPerformed(ActionEvent evt) {
        chessButton.setSelected(true);
        antiChessButton.setSelected(false);
    }

    private void antiChessButtonActionPerformed(ActionEvent evt) {
        chessButton.setSelected(false);
        antiChessButton.setSelected(true);
    }

    private void openMenuItemActionPerformed(ActionEvent evt, JFileChooser jFileChooser) {
        if (controller != null)
            return; //if game is still going on, return without doing anything

        int returnVal = jFileChooser.showOpenDialog(rootPane);
        if (returnVal == JFileChooser.APPROVE_OPTION) {

            File f = jFileChooser.getSelectedFile();
            String gameName = jFileChooser.getName(f);
            String path = f.getAbsolutePath();

            //start a new GameController and initialize gameDialog
            // opening file = path
            try {
                controller = new Controller(path);
            } catch (Exception e) { //print an informative menu on the screen that it is a corrupt file
                JOptionPane.showMessageDialog(chessBoardPanel, "Corrupt file:" + path);
                return; //return from the open menu
            }

            gameDialog.initGameDialog(gameName, controller);
            gameDialog.setVisible(true);
        }
    }

    private void saveMenuItemActionPerformed(ActionEvent evt, JFileChooser jFileChooser) {

        //if there is no game, we cant save
        if (controller == null)
            return;
        //get gamename from gc
        String s = controller.getName();
        //set selected file
        jFileChooser.setSelectedFile(new File(s));
        //get the pressed button
        int result = jFileChooser.showSaveDialog(rootPane);
        //if save is selected, proceed
        if (result == JFileChooser.APPROVE_OPTION)
            Controller.saveGameToFile(controller, jFileChooser.getSelectedFile().getAbsolutePath());

    }

    /**
     * Start the game
     *
     * @throws RuntimeException if gc==null || onGoingGame!=null
     * @requires gc!=null && onGoingGame==null
     */
    private void startTwoPlayerGameButtonActionPerformed(ActionEvent evt) {
        if (controller == null)
            throw new RuntimeException("GameController is null just before the game starts!");
        if (onGoingGame != null)
            throw new RuntimeException("GameController has a game running before a new game starts");

        //we have to know which players are associated with what colors
        int Player1Color;
        int Player2Color;

        if (gameDialog.getPlayer1whitesideButton().isSelected()) {
            Player1Color = Sides.WHITE;
            Player2Color = Sides.BLACK;
        } else {
            Player2Color = Sides.WHITE;
            Player1Color = Sides.BLACK;
        }

        if (Player1Color == Sides.WHITE) {
            playerInformationPanel.getWhiteplayernameLabel().setText(gameDialog.getPlayer1nameTextField().getText());
            playerInformationPanel.getBlackplayernameLabel().setText(gameDialog.getPlayer2nameTextField().getText());
        } else {
            playerInformationPanel.getWhiteplayernameLabel().setText(gameDialog.getPlayer2nameTextField().getText());
            playerInformationPanel.getBlackplayernameLabel().setText(gameDialog.getPlayer1nameTextField().getText());
        }

        //set name, show moves and show pieces
        controller.setName(gameDialog.getGameTextField().getText());
        controller.setShowLegalMoves(showMoves);
        controller.setShowLegalPieces(showPieces);

        //set type
        if (gameDialog.getCylinderCheckBox().isSelected())
            controller.setBoardType(BoardType.CYLINDER);
        else
            controller.setBoardType(BoardType.REGULAR);

        //set time
        controller.setTimeLeftString(Player1Color, gameDialog.getPlayer1timeTextField().getText());
        controller.setTimeLeftString(Player2Color, gameDialog.getPlayer2timeTextField().getText());

        //set players
        if (gameDialog.getPlayersRadioButton1().isSelected()) {
            //player 1 and 2 are both human
            controller.setPlayer(Player1Color, PlayerType.HUMAN, 0);
            controller.setPlayer(Player2Color, PlayerType.HUMAN, 0);
        } else if (gameDialog.getPlayersRadioButton2().isSelected()) {
            controller.setPlayer(Player1Color, PlayerType.HUMAN, 0);
            controller.setPlayer(Player2Color, PlayerType.MACHINE, gameDialog.getPlayer2styleComboBox().getSelectedIndex());
        } else {
            //player 1 is achess
            //player 2 is achess
            controller.setPlayer(Player1Color, PlayerType.MACHINE, gameDialog.getPlayer1styleComboBox().getSelectedIndex());
            controller.setPlayer(Player2Color, PlayerType.MACHINE, gameDialog.getPlayer2styleComboBox().getSelectedIndex());
        }

        // start the game

        //disable newtwoMenuItem and openMenuItem
        newMenu.setEnabled(false);
        openMenuItem.setEnabled(false);
        saveMenuItem.setEnabled(true);

        //associate the board with the game controller
        chessBoardPanel.setGameController(controller);

        //start observing what might be happening in the game
        controller.addObserver(this);

        //hide this dialog and start the game
        gameDialog.setVisible(false);

        //start timers for each side
        chessTimer.start();
        //gc.startGame();
        if (onGoingGame == null)
            (onGoingGame = new Thread(controller)).start();
        else
            throw new RuntimeException("GameController has a game running before a new game starts");
        moveCount = 0;

        //change the color of label of the first player
        int nextTurn = controller.getNextTurn();

        if (nextTurn == 0)
            playerInformationPanel.getWhiteplayercolorLabel().setBackground(new java.awt.Color(255, 255, 0));

        if (nextTurn == 1)
            playerInformationPanel.getBlackplayercolorLabel().setBackground(new java.awt.Color(255, 255, 0));

    }

    /**
     * The game controller object has an update in the flow of the game.
     */
    public void update(@NotNull Message notification) {
        //check if a game is going on at all
        if (controller == null) return;
        //act now depending on what has happened to the game.
        if (notification.getType() == MessageType.MOVE) {
            // Given a move made, this method puts this in the move list and repaints the board

            Move move = notification.getMove();
            //for highlighting the last move
            playerInformationPanel.getjTextArea1().requestFocus();
            playerInformationPanel.getjTextArea1().select(0, 0);

            if (moveCount % 2 == 0)
                playerInformationPanel.getjTextArea1().append("\n" + (showTime ? controller.getTimeLeftString(Sides.WHITE) + "   " : "  ") + (moveCount / controller.getPlayers().getNumber() + 1) + "." + move + " ");
            else
                playerInformationPanel.getjTextArea1().append("\n" + (showTime ? controller.getTimeLeftString(Sides.BLACK) + "   " : "  ") + (moveCount / controller.getPlayers().getNumber() + 1) + "......" + move + "  ");

            //change the color of the NexTplayer
            int nextTurn = controller.getNextTurn();

            playerInformationPanel.getWhiteplayercolorLabel().setBackground(new Color(194, 204, 204));
            playerInformationPanel.getBlackplayercolorLabel().setBackground(new Color(204, 174, 174));
            playerInformationPanel.getRedplayercolorLabel().setBackground(new Color(255, 0, 51));
            playerInformationPanel.getBlueplayercolorLabel().setBackground(new Color(225, 133, 225));

            if (nextTurn == 0)
                playerInformationPanel.getWhiteplayercolorLabel().setBackground(new Color(255, 255, 0));

            if (nextTurn == 1)
                playerInformationPanel.getBlackplayercolorLabel().setBackground(new Color(255, 255, 0));

            if (nextTurn == 2)
                playerInformationPanel.getRedplayercolorLabel().setBackground(new Color(255, 255, 0));

            if (nextTurn == 3)
                playerInformationPanel.getBlueplayercolorLabel().setBackground(new Color(255, 255, 0));

            moveCount++;

            //for highlighting the last move
            int lineCount = playerInformationPanel.getjTextArea1().getLineCount();
            try {
                playerInformationPanel.getjTextArea1().select(playerInformationPanel.getjTextArea1().getLineStartOffset(lineCount - 1), playerInformationPanel.getjTextArea1().getLineEndOffset(lineCount - 1));
            } catch (Exception e) {
            }

            chessBoardPanel.updateBoard();//board has been changed
        }
        if (notification.getType() == MessageType.MESSAGE) {
            exitGameButtonActionPerformed(notification.getMessage());
        }

    }

    private void newGameTwoPlayerMenuItemActionPerformed(ActionEvent evt) {
        if (controller != null) return; //if a game is going on return
        controller = new Controller(Players.TWO);
        gameDialog.initGameDialog(null, controller);       //initialize with the standard values
        gameDialog.setVisible(true);    //let them set the values
    } // TODO gameDialog could also be outsourced to a separate JDialog

    /**
     * Starts a four player game
     */
    public void startfourPlayerGame(String[] names, String[] times, int[] types) {

        //** START FOUR PLAYER GAME**

        if (onGoingGame != null)
            throw new RuntimeException("GameController has a game running before a new game starts");

        //start a new game controller
        controller = new Controller(Players.FOUR);

        //set name
        controller.setName("Four Player Game");
        controller.setShowLegalMoves(showMoves);
        controller.setShowLegalPieces(showPieces);

        //set time
        controller.setTimeLeftString(Sides.WHITE, times[0]);
        controller.setTimeLeftString(Sides.BLACK, times[2]);
        controller.setTimeLeftString(Sides.RED, times[1]);
        controller.setTimeLeftString(Sides.BLUE, times[3]);

        //set players
        if (types[0] == 0)  //player 1 is human
            controller.setPlayer(Sides.WHITE, PlayerType.HUMAN, 0);
        else
            controller.setPlayer(Sides.WHITE, PlayerType.MACHINE, 11);

        if (types[1] == 0)  //player 2 is human
            controller.setPlayer(Sides.RED, PlayerType.HUMAN, 0);
        else
            controller.setPlayer(Sides.RED, PlayerType.MACHINE, 11);

        if (types[2] == 0)  //player 3 is human
            controller.setPlayer(Sides.BLACK, PlayerType.HUMAN, 0);
        else
            controller.setPlayer(Sides.BLACK, PlayerType.MACHINE, 11);

        if (types[3] == 0)  //player 4 is human
            controller.setPlayer(Sides.BLUE, PlayerType.HUMAN, 0);
        else
            controller.setPlayer(Sides.BLUE, PlayerType.MACHINE, 11);

        // start the game

        //add time,name and color information to jpanel4 and jpanel5

        playerInformationPanel.getRedplayercolorLabel().setVisible(true);
        playerInformationPanel.getRedplayernameLabel().setVisible(true);
        playerInformationPanel.getRedplayertimeTextField().setVisible(true);
        playerInformationPanel.getBlueplayercolorLabel().setVisible(true);
        playerInformationPanel.getBlueplayernameLabel().setVisible(true);
        playerInformationPanel.getBlueplayertimeTextField().setVisible(true);

        //update UI information

        playerInformationPanel.getRedplayernameLabel().setText(names[1]);
        playerInformationPanel.getRedplayertimeTextField().setText(times[1]);

        playerInformationPanel.getBlueplayernameLabel().setText(names[3]);
        playerInformationPanel.getBlueplayertimeTextField().setText(times[3]);

        playerInformationPanel.getBlackplayernameLabel().setText(names[2]);
        playerInformationPanel.getBlackplayertimeTextField().setText(times[2]);

        playerInformationPanel.getWhiteplayernameLabel().setText(names[0]);
        playerInformationPanel.getWhiteplayertimeTextField().setText(times[0]);

        //disable newtwoMenuItem and openMenuItem
        newMenu.setEnabled(false);
        openMenuItem.setEnabled(false);
        saveMenuItem.setEnabled(false);

        //associate the board with the game controller
        chessBoardPanel.setGameController(controller);

        //start observing what might be happening in the game
        controller.addObserver(this);

        //start timers for each side
        chessTimer.start();

        //gc.startGame();
        if (onGoingGame == null)
            (onGoingGame = new Thread(controller)).start();
        else
            throw new RuntimeException("GameController has a game running before a new game starts");
        moveCount = 0;

        //change the color of label of the next player
        int nextTurn = controller.getNextTurn();

        if (nextTurn == 0)
            playerInformationPanel.getWhiteplayercolorLabel().setBackground(new java.awt.Color(255, 255, 0));

        if (nextTurn == 1)
            playerInformationPanel.getBlackplayercolorLabel().setBackground(new java.awt.Color(255, 255, 0));

        if (nextTurn == 2)
            playerInformationPanel.getRedplayercolorLabel().setBackground(new java.awt.Color(255, 255, 0));

        if (nextTurn == 3)
            playerInformationPanel.getBlueplayercolorLabel().setBackground(new java.awt.Color(255, 255, 0));

    }

    /**
     * Exit the Application
     */
    private void exitApplication(WindowEvent evt) {
        System.exit(0);
    }

    public boolean isShowTime() {
        return showTime;
    }

    public void setShowTime(boolean showTime) {
        this.showTime = showTime;
    }

    public boolean isShowMoves() {
        return showMoves;
    }

    public void setShowMoves(boolean showMoves) {
        this.showMoves = showMoves;
    }

    public boolean isShowPieces() {
        return showPieces;
    }

    public void setShowPieces(boolean showPieces) {
        this.showPieces = showPieces;
    }

    public Controller getController() {
        return controller;
    }
}