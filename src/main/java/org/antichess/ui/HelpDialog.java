/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.ui;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import java.awt.*;

/**
 *
 */
public class HelpDialog extends JDialog {

    public HelpDialog(Frame parent) {
        super(parent);

        setSize(new Dimension(800, 600));
        setLayout(new GridBagLayout());
        setTitle(TextFields.ProgramName + TextFields.Version + TextFields.Help);

        JEditorPane jEditorPane1 = new JEditorPane();
        try {
            jEditorPane1.setPage(HelpDialog.class.getResource("/data/documentation/help.html"));
        } catch (Exception e) {
        }
        jEditorPane1.setBorder(null);
        jEditorPane1.setEditable(false);
        jEditorPane1.setContentType("text/html");
        jEditorPane1.addHyperlinkListener(this::jEditorPane1HyperlinkUpdate);

        JScrollPane jScrollPane2 = new JScrollPane();
        jScrollPane2.setViewportView(jEditorPane1);

        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.fill = GridBagConstraints.BOTH;
        gridBagConstraints1.weightx = 0.5;
        gridBagConstraints1.weighty = 0.5;
        gridBagConstraints1.insets = new Insets(10, 10, 10, 10);
        getContentPane().add(jScrollPane2, gridBagConstraints1);

        // help cancel button
        JButton helpcancelButton = new JButton();
        helpcancelButton.setText(TextFields.Cancel);
        helpcancelButton.addActionListener(evt -> setVisible(false));

        JPanel jPanel17 = new JPanel();
        jPanel17.add(helpcancelButton);

        gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 1;
        gridBagConstraints1.fill = GridBagConstraints.BOTH;
        gridBagConstraints1.weightx = 0.5;
        getContentPane().add(jPanel17, gridBagConstraints1);
    }

    private void jEditorPane1HyperlinkUpdate(HyperlinkEvent evt) {
        if (evt.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            JEditorPane pane = (JEditorPane) evt.getSource();
            if (evt instanceof javax.swing.text.html.HTMLFrameHyperlinkEvent) {
                javax.swing.text.html.HTMLFrameHyperlinkEvent e = (javax.swing.text.html.HTMLFrameHyperlinkEvent) evt;
                javax.swing.text.html.HTMLDocument doc = (javax.swing.text.html.HTMLDocument) pane.getDocument();
                doc.processHTMLFrameHyperlinkEvent(e);
            } else {
                try {
                    pane.setPage(evt.getURL());
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }

    }
}
