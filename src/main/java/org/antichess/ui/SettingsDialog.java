/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Settings dialog
 */
public class SettingsDialog extends JDialog {

    public SettingsDialog(MainFrame parent, boolean modal) {
        super(parent, modal);

        setSize(new Dimension(263, 165));
        setTitle(TextFields.Settings);
        getContentPane().setLayout(new FlowLayout());

        // panel
        JPanel jPanel14 = new JPanel();
        jPanel14.setLayout(new BoxLayout(jPanel14, BoxLayout.Y_AXIS));

        JPanel jPanel16 = new JPanel();
        jPanel16.setLayout(new GridLayout(3, 0, 10, 0));
        jPanel16.setBorder(new javax.swing.border.EtchedBorder());

        final JCheckBox jCheckBox1 = new JCheckBox(TextFields.ShowLegalMoves);
        jPanel16.add(jCheckBox1);

        final JCheckBox jCheckBox2 = new JCheckBox(TextFields.ShowTime);
        jCheckBox2.setHorizontalAlignment(SwingConstants.LEFT);
        jPanel16.add(jCheckBox2);

        final JCheckBox jCheckBox3 = new JCheckBox(TextFields.ShowPieces);
        jCheckBox3.setHorizontalAlignment(SwingConstants.LEFT);
        jPanel16.add(jCheckBox3);

        // add panel to panel
        jPanel14.add(jPanel16);

        // panel
        JPanel jPanel15 = new JPanel();

        JButton settingsokButton = new JButton(TextFields.OK);
        settingsokButton.addActionListener(evt -> settingsokButtonActionPerformed(evt, jCheckBox1, jCheckBox2, jCheckBox3, parent));
        jPanel15.add(settingsokButton);

        JButton settingscancelButton = new JButton(TextFields.Cancel);
        settingscancelButton.addActionListener(evt -> settingscancelButtonActionPerformed(evt, jCheckBox1, jCheckBox2, jCheckBox3, parent));
        jPanel15.add(settingscancelButton);

        // add panel to panel
        jPanel14.add(jPanel15);

        // add panel to settings dialog
        getContentPane().add(jPanel14);
    }

    /**
     * <p> Fired when 'ok' button is selected in settings Dialog
     */
    private void settingsokButtonActionPerformed(ActionEvent evt,
                                                 JCheckBox jCheckBox1,
                                                 JCheckBox jCheckBox2,
                                                 JCheckBox jCheckBox3,
                                                 MainFrame mainFrame) {
        mainFrame.setShowTime(jCheckBox2.isSelected());
        mainFrame.setShowMoves(jCheckBox1.isSelected());
        mainFrame.setShowPieces(jCheckBox3.isSelected());

        //make the changes for the ongoing move
        if (mainFrame.getController() != null) {
            mainFrame.getController().setShowLegalMoves(mainFrame.isShowMoves());
            mainFrame.getController().setShowLegalPieces(mainFrame.isShowPieces());
        }

        setVisible(false);
    }

    /**
     * <p> Fired when 'cancel' button is selected in settings Dialog
     */
    private void settingscancelButtonActionPerformed(ActionEvent evt,
                                                     JCheckBox jCheckBox1,
                                                     JCheckBox jCheckBox2,
                                                     JCheckBox jCheckBox3,
                                                     MainFrame mainFrame) {
        jCheckBox1.setSelected(mainFrame.isShowMoves());
        jCheckBox2.setSelected(mainFrame.isShowTime());
        jCheckBox3.setSelected(mainFrame.isShowPieces());

        setVisible(false);
    }
}
