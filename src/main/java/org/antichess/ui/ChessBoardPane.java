/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.ui;

import org.antichess.controller.Controller;
import org.antichess.controller.HumanPlayer;
import org.antichess.model.Board;
import org.antichess.model.Move;
import org.antichess.model.Players;
import org.antichess.model.Sides;
import org.antichess.model.pieces.PieceImplementation;
import org.antichess.model.pieces.PieceType;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * XChessBoard creates an antichess board image on the board, places pieces on it,
 * and animates pieces for drag and drop.
 *
 * <p><u>Here is the outline of how XChessBoard handles an antichess board:</u><br>
 * Everytime setGameController is called, a new background image is created
 * by first calling initBoard() and then, setBoardImage().
 *
 * <p>placePieces() creates labels with piece images for every non-empty square.
 * It places this accordingly on the board.
 *
 * <p>updateBoard() goes through all the squares of the board. It updates the positions of
 * already created pieces. If there are not enough labels, it creates new labels. If
 * there are any excess labels it gets rid of them.
 *
 * <p>Drag and drop is implemented through JLabel.setLocation() method. This provides a flawless
 * animation compared to other methods.
 */
public class ChessBoardPane extends JLayeredPane implements MouseListener, MouseMotionListener {

    // TODO vertical indices go from 1-9 then a,b,c should be 10, 11, ... for 4-player board
    private static Controller controller; //GameController for the game, assumed to be fixed, only supplied in the constructor // TODO should not be a static variable or at least not here
    private final int SIZEX = 600;
    private final int SIZEY = 600;          //total size for the board
    private final Color WHITECOLOR = new Color(204, 204, 244); //square colors
    //for adding position letters on the side
    private final Color BLACKCOLOR = Color.black;
    private final int boardMargin = 3; //margin of the chessboard from the sides
    private final Image[] pieceImageList;
    private final int piecePaddingX = 8;//margin of the labelList from the square sides
    private final int piecePaddingY = 3;//margin of the labelList from the square sides
    private Board board;
    private int squareSizeX, squareSizeY; //square size for the board
    private int fourtwoMargin; //margin for 4 vs 2 player
    private int startCol, startRow, endCol, endRow; //moving labelList coordinates

    private ArrayList<JLabel>[] labelList; //list of pieces, pieces are arrays
    private JLabel movingLabel; //the dragged label/piece

    private int arbX, arbY; //how much arbitrage we need in the chess icon itself
    private boolean mousePress; //true if mouse is pressed
    private JLabel[][] circlesValidPieces; //stores the circles for valid pieces
    private JLabel[][] circlesValidMoves; //stores the circles for valid moves

    private HumanPlayer human; //human player

    /**
     * Creates new form XChessBoard
     */
    public ChessBoardPane() {

        /*
          Initializes global variables

          @requires topology is a reference to a two dimensional integer array
         * @effects assigns topology the topology of the game
         */

        squareSizeX = squareSizeY = 1;
        arbX = arbY = 0;
        startCol = startRow = endRow = endCol = 0;
        movingLabel = null;

        //Load the images
        pieceImageList = new Image[4 * PieceImplementation.PIECECOUNT];

        setLayout(new BorderLayout());
        setPreferredSize(new Dimension(SIZEX, SIZEY));
        addMouseListener(this);
        addMouseMotionListener(this);

    }

    /**
     * Invoked when a mouse button is pressed on a component and then
     * dragged.  {@code MOUSE_DRAGGED} events will continue to be
     * delivered to the component where the drag originated until the
     * mouse button is released (regardless of whether the mouse position
     * is within the bounds of the component).
     * <p>
     * Due to platform-dependent Drag and Drop implementations,
     * {@code MOUSE_DRAGGED} events may not be delivered during a native
     * Drag and Drop operation.
     */
    public void mouseDragged(MouseEvent e) {
        if (controller == null || movingLabel == null || !human.isMyTurn())
            return;
        movingLabel.setLocation(e.getX() + arbX + piecePaddingX, e.getY() - fourtwoMargin + arbY + piecePaddingY);
    }

    @Override
    public void mouseMoved(MouseEvent e) {
    }

    @Override
    public void mouseClicked(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
        if (controller == null || !human.isMyTurn())
            return;

        int posX = e.getX();
        int posY = e.getY();

        startCol = (posX - boardMargin) / squareSizeX;
        arbX = (boardMargin - posX) % squareSizeX;
        startRow = (SIZEY - (posY - fourtwoMargin - boardMargin)) / squareSizeY;
        arbY = (-SIZEY + (boardMargin - posY - fourtwoMargin)) % squareSizeY;

        if (startCol > Board.getMAXCOL()) startCol = Board.getMAXCOL();
        if (startCol < Board.getMINCOL()) startCol = Board.getMINCOL();

        if (startRow > Board.getMAXROW()) startRow = Board.getMAXROW();
        if (startRow < Board.getMINROW()) startRow = Board.getMINROW();

        PieceImplementation piece;

        if ((piece = board.getPieceAt(startCol, startRow)) != null)

            if (piece.getType() != PieceType.EMPTY) {
                for (int j = 0; j < controller.getPlayers().getNumber() * PieceImplementation.PIECECOUNT; j++) {
                    movingLabel = null;
                    if (labelList[j] != null)
                        for (int i = 0; i < labelList[j].size(); i++)
                            if (labelList[j].get(i).getBounds().contains(posX, posY)) {
                                movingLabel = labelList[j].get(i);
                                moveToFront(movingLabel);
                                break;
                            }
                    if (movingLabel != null) break;
                }

                List<Move> moves = board.getAllValidMoves(piece.getColor());
                //makes the red circles visible

                if (controller.getShowLegalPieces())
                    for (Move move : moves) {
                        int sCol = move.getStartCol();
                        int sRow = move.getStartRow();
                        JLabel circles = circlesValidPieces[sCol][sRow];
                        circles.setLocation(sCol * squareSizeX + boardMargin, SIZEY - (sRow + 1) * squareSizeY + boardMargin + fourtwoMargin);
                        circles.setVisible(true);

                    }

                //makes the blue circles visible
                if (controller.getShowLegalMoves())
                    for (Move move : moves) {
                        int eCol = move.getDestCol();
                        int eRow = move.getDestRow();
                        if ((move.getStartCol() == startCol) && (move.getStartRow() == startRow)) {
                            JLabel circles = circlesValidMoves[eCol][eRow];
                            circles.setVisible(true);

                        }

                    }

                mousePress = true;
            }
    }

    /**
     * Invoked when a mouse button has been released on a component.
     *
     * @requires startCol and startRow non-zero starting row and col of the {@code movingLabel && movingLabel!=null && gc!=null && ( squareSizeX!=0 && squareSizeY!=0)}
     */
    public void mouseReleased(MouseEvent e) {
        if (controller == null)
            return;

        //hides the red circles once the piece is not being moved
        for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
            for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++) {
                circlesValidPieces[i][j].setVisible(false);
            }
        }

        //hides the blue circles once the piece is not being moved
        for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
            for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++) {
                circlesValidMoves[i][j].setVisible(false);
            }
        }

        if (movingLabel == null || !human.isMyTurn())
            return;

        if (mousePress)
            mousePress = false;

        endCol = (e.getX() - boardMargin) / squareSizeX;
        endRow = ((SIZEY - e.getY() + fourtwoMargin) + boardMargin) / squareSizeY;

        //if the piece is dropped out of the board then,
        //put it into the board
        if (endCol > Board.getMAXCOL()) endCol = Board.getMAXCOL();
        if (endCol < Board.getMINCOL()) endCol = Board.getMINCOL();

        if (endRow > Board.getMAXROW()) endRow = Board.getMAXROW();
        if (endRow < Board.getMINROW()) endRow = Board.getMINROW();

        //check if the moveMade is in the possible moves list
        //if it is, make the move and tell it to humanPlayer.
        //Else, put the piece into the old place.

        Move moveMade = new Move(startCol, startRow, endCol, endRow);

        if (human.getMoveList().contains(moveMade)) {
            movingLabel.setLocation(endCol * squareSizeX + piecePaddingX + boardMargin,
                    SIZEY - (endRow + 1) * squareSizeY + piecePaddingY + boardMargin + fourtwoMargin);
            human.setMove(moveMade);
        } else
            movingLabel.setLocation(startCol * squareSizeX + piecePaddingX + boardMargin,
                    SIZEY - (startRow + 1) * squareSizeY + piecePaddingY + boardMargin + fourtwoMargin);

        moveToBack(movingLabel);
        movingLabel = null;

    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }

    /**
     * Sets the GameController handler to the specified game controller
     *
     * @requires gc instance of GameController
     * @effects GameController=gc
     */
    public void setGameController(Controller controller) {

        removeAll(); //remove all the previously registered components
        repaint();

        ChessBoardPane.controller = controller;

        if (controller != null) {

            // initializes the board variables

            board = controller.getBoard();
            squareSizeX = SIZEX / (Board.getMAXCOL() - Board.getMINCOL() + 1);
            squareSizeY = SIZEY / (Board.getMAXROW() - Board.getMINROW() + 1);
            human = controller.getHumanPlayer();

            labelList = new ArrayList[controller.getPlayers().getNumber() * PieceImplementation.PIECECOUNT];

            int pieceSizeX = squareSizeX - piecePaddingX * (controller.getPlayers().getNumber() == 2 ? 4 : 2);
            // piece size
            int pieceSizeY = squareSizeY - piecePaddingY * (controller.getPlayers().getNumber() == 2 ? 4 : 2);

            //for position, the first letter and number
            char startNumber;
            char startLetter;
            if (controller.getPlayers() == Players.TWO) {
                //position start number/letter
                startLetter = 'a';
                startNumber = '8';
                //margin
                fourtwoMargin = 0;
            } else {
                fourtwoMargin = (-10);
                //position start number/letter
                startLetter = 'a';
                startNumber = 'e';
            }
            //for red circles
            circlesValidPieces = new JLabel[Board.getMAXCOL() + 1][];
            //for blue circles
            circlesValidMoves = new JLabel[Board.getMAXCOL() + 1][];

            //  Loads the images from the directory
            pieceImageList[Sides.WHITE * PieceImplementation.PIECECOUNT + PieceType.PAWN.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Wpawn.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLACK * PieceImplementation.PIECECOUNT + PieceType.PAWN.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/BLpawn.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.WHITE * PieceImplementation.PIECECOUNT + PieceType.ROOK.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Wrook.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLACK * PieceImplementation.PIECECOUNT + PieceType.ROOK.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/BLrook.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.WHITE * PieceImplementation.PIECECOUNT + PieceType.BISHOP.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Wbishop.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLACK * PieceImplementation.PIECECOUNT + PieceType.BISHOP.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/BLbishop.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.WHITE * PieceImplementation.PIECECOUNT + PieceType.KNIGHT.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Wknight.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLACK * PieceImplementation.PIECECOUNT + PieceType.KNIGHT.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/BLknight.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.WHITE * PieceImplementation.PIECECOUNT + PieceType.KING.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Wking.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLACK * PieceImplementation.PIECECOUNT + PieceType.KING.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/BLking.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.WHITE * PieceImplementation.PIECECOUNT + PieceType.QUEEN.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Wqueen.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLACK * PieceImplementation.PIECECOUNT + PieceType.QUEEN.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/BLqueen.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLUE * PieceImplementation.PIECECOUNT + PieceType.PAWN.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Bpawn.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.RED * PieceImplementation.PIECECOUNT + PieceType.PAWN.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Rpawn.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLUE * PieceImplementation.PIECECOUNT + PieceType.ROOK.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Brook.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.RED * PieceImplementation.PIECECOUNT + PieceType.ROOK.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Rrook.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLUE * PieceImplementation.PIECECOUNT + PieceType.BISHOP.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Bbishop.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.RED * PieceImplementation.PIECECOUNT + PieceType.BISHOP.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Rbishop.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLUE * PieceImplementation.PIECECOUNT + PieceType.KNIGHT.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Bknight.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.RED * PieceImplementation.PIECECOUNT + PieceType.KNIGHT.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Rknight.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLUE * PieceImplementation.PIECECOUNT + PieceType.KING.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Bking.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.RED * PieceImplementation.PIECECOUNT + PieceType.KING.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Rking.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.BLUE * PieceImplementation.PIECECOUNT + PieceType.QUEEN.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Bqueen.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);
            pieceImageList[Sides.RED * PieceImplementation.PIECECOUNT + PieceType.QUEEN.getValue()] = (new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/pieces/Rqueen.gif"))).getImage().getScaledInstance(pieceSizeX, pieceSizeY, Image.SCALE_DEFAULT);

            // Loads the red  circle images from the directory that indicate valid pieces to be moved
            for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
                //creates the second dimension for i'th entry
                circlesValidPieces[i] = new JLabel[Board.getMAXROW() + 1];
                //for this second dimension, loads the images from the XChessBoard.class directory
                for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++)
                    circlesValidPieces[i][j] = new JLabel(new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/Rdot.gif")));
            }

            // Loads the blue  circle images from the directory that indicate valid moves that can be made by piece being moved
            for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
                //creates the second dimension for i'th entry
                circlesValidMoves[i] = new JLabel[Board.getMAXROW() + 1];
                //for this second dimension, loads the images from the XChessBoard.class directory
                for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++)
                    circlesValidMoves[i][j] = new JLabel(new ImageIcon(ChessBoardPane.class.getResource("/data/artwork/Gdot.gif")));
            }

            // Sets the board image for background.
            // Paints squares in given BLACKCOLOR and WHITECOLOR.
            // Adds position letters/numbers at the bottom and on
            // the right
            //letter margin added to the board size
            int letterMargin = 20;
            Image boardImage = createImage(SIZEX + letterMargin, SIZEY + letterMargin);
            Graphics g = boardImage.getGraphics();

            //draw squares
            for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
                for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++)
                    if (board.getUsable(i, j)) {
                        g.setColor(BLACKCOLOR);
                        g.drawRect(i * squareSizeX, j * squareSizeY, squareSizeX, squareSizeY);
                        if ((i + j) % 2 == 0) {
                            g.setColor(WHITECOLOR);
                        } else {
                            g.setColor(BLACKCOLOR);
                        }
                        g.fillRect(i * squareSizeX, j * squareSizeY, squareSizeX, squareSizeY);
                    }
            }

            //draw position letters/numbers
            //this uses java.lang.Character.forDigit() and and digit()
            //in order to accommodate changing number of rows and columns
            //this also uses getFontMetrics.getHeight() for bottom letters
            //to be far away enough from the actual board.
            char[] position = new char[1];
            g.setColor(BLACKCOLOR);
            int padding = g.getFontMetrics().getHeight();

            for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
                position[0] = Character.forDigit(Character.digit(startLetter,
                        Character.MAX_RADIX) + i, Character.MAX_RADIX);
                g.drawChars(position, 0, 1, i * squareSizeX + squareSizeX / 2, SIZEY + padding);
            }

            for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++) {
                position[0] = Character.forDigit(Character.digit(startNumber,
                        Character.MAX_RADIX) - j, Character.MAX_RADIX);

                g.drawChars(position, 0, 1, SIZEX + letterMargin / 2, j * squareSizeY + squareSizeY / 2);
            }

            //now set the image as background image
            //the board: background image
            JLabel background = new JLabel(new ImageIcon(boardImage));
            background.setBounds(boardMargin, boardMargin, SIZEX + letterMargin, SIZEY + letterMargin);
            add(background, JLayeredPane.DEFAULT_LAYER);

            // place pieces on the board
            JLabel newLabel;
            PieceImplementation piece;
            for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
                for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++)
                    if ((piece = board.getPieceAt(i, j)) != null)
                        if (piece.getType() != PieceType.EMPTY) {
                            int pIndex = piece.getType().getValue() + PieceImplementation.PIECECOUNT * piece.getColor();
                            //check if there are any pieces at all
                            //if not create a new piece Arraylist
                            if (labelList[pIndex] == null)
                                labelList[pIndex] = new ArrayList<>();
                            //create the new label and place it on the board
                            newLabel = new JLabel(new ImageIcon(pieceImageList[pIndex]));
                            newLabel.setBounds(i * squareSizeX + boardMargin + piecePaddingX, SIZEY - (j + 1) * squareSizeY + boardMargin + fourtwoMargin + piecePaddingY,
                                    squareSizeX - 2 * piecePaddingX, squareSizeY - 2 * piecePaddingY);
                            //add the label to the labelList and to the board
                            labelList[pIndex].add(newLabel);
                            add(newLabel, JLayeredPane.PALETTE_LAYER);
                        }
            }

            // place red circles on the board that indicate the pieces that can be moved
            for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
                for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++) {
                    JLabel circles = circlesValidPieces[i][j];
                    circles.setBounds(i * squareSizeX + boardMargin, SIZEY - (j + 1) * squareSizeY + boardMargin,
                            squareSizeX / 5, squareSizeY / 5);
                    add(circles, JLayeredPane.PALETTE_LAYER);
                    circles.setVisible(false);
                }
            }

            //show green circles on the board for valid moves that can be made given the starting position of the piece being moved.
            //only keeps track of the normal moves ..need to add switch moves and fix it for 4 player chess
            for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
                for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++) {
                    JLabel circles = circlesValidMoves[i][j];
                    circles.setBounds(i * squareSizeX + 12 * boardMargin, SIZEY - (j + 1) * squareSizeY + boardMargin,
                            squareSizeX / 5, squareSizeY / 5);
                    add(circles, JLayeredPane.PALETTE_LAYER);
                    circles.setVisible(false);
                }
            }
        }
    }

    /**
     * Method to update pieces on the board
     * Goes through board, and if there is a piece at (i,j)
     * takes a label from labelList, puts the necessary image on it
     * places it on the right place.
     * This method discards labels if there are less pieces on the board.
     *
     * @modifies the piece arrangement on the screen and labelList
     * @requires {@code labelList.size() >= numberOfPiecesonBoard}
     */
    public void updateBoard() {

        int[] alive = new int[controller.getPlayers().getNumber() * PieceImplementation.PIECECOUNT];
        JLabel newLabel;
        PieceImplementation piece;

        for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++) {
            for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++)
                if ((piece = board.getPieceAt(i, j)) != null)
                    if (piece.getType() != PieceType.EMPTY) {
                        int pIndex = piece.getType().getValue() + PieceImplementation.PIECECOUNT * piece.getColor();
                        //check if there are any pieces at all
                        //if not create a new piece Arraylist

                        if (labelList[pIndex] == null)
                            labelList[pIndex] = new ArrayList<>();

                        //check if we still have not-used labels
                        //if not create a new one
                        if (labelList[pIndex].size() == alive[pIndex]) {
                            newLabel = new JLabel(new ImageIcon(pieceImageList[pIndex]));
                            //add the label to the labelList and to the board
                            newLabel.setBounds(i * squareSizeX + boardMargin + piecePaddingX, SIZEY - (j + 1) * squareSizeY + boardMargin + fourtwoMargin + piecePaddingY,
                                    squareSizeX - 2 * piecePaddingX, squareSizeY - 2 * piecePaddingY);
                            labelList[pIndex].add(newLabel);
                            add(newLabel, JLayeredPane.PALETTE_LAYER);

                        }

                        //get the next label from the labelList
                        newLabel = labelList[pIndex].get(alive[pIndex]);
                        alive[pIndex]++;
                        newLabel.setVisible(true);

                        //set its location accordingly
                        newLabel.setLocation(i * squareSizeX + boardMargin + piecePaddingX, SIZEY - (j + 1) * squareSizeY + boardMargin + fourtwoMargin + piecePaddingY);

                    }
        }

        //discard unnecessary labels
        //remove them as well
        for (int i = 0; i < controller.getPlayers().getNumber() * PieceImplementation.PIECECOUNT; i++)
            if (labelList[i] != null) {
                int aliveCopy = alive[i];
                for (; alive[i] < labelList[i].size(); alive[i]++)
                    labelList[i].get(alive[i]).setVisible(false);
                while (aliveCopy < labelList[i].size())
                    labelList[i].remove(aliveCopy);
            }

    }

}

