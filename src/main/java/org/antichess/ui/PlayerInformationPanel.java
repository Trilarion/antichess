/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.ui;

import javax.swing.*;
import java.awt.*;

/**
 * Player information panel right at the chessboard.
 */
public class PlayerInformationPanel extends JPanel {

    private final JTextField whiteplayertimeTextField;
    private final JTextField blackplayertimeTextField;
    private final JTextArea jTextArea1;
    private final JLabel whiteplayernameLabel;
    private final JLabel redplayercolorLabel;
    private final JLabel blueplayercolorLabel;
    private final JLabel whiteplayercolorLabel;
    private final JLabel blackplayercolorLabel;
    private final JLabel redplayernameLabel;
    private final JLabel blueplayernameLabel;
    private final JTextField redplayertimeTextField;
    private final JTextField blueplayertimeTextField;
    private final JLabel blackplayernameLabel;

    public PlayerInformationPanel() {

        setLayout(new BorderLayout());
        setBackground(new Color(214, 204, 204));
        setPreferredSize(new Dimension(220, 62));

        jTextArea1 = new JTextArea("         Moves");
        jTextArea1.setEditable(false);
        JScrollPane jScrollPane1 = new JScrollPane();
        jScrollPane1.setViewportView(jTextArea1);
        add(jScrollPane1, BorderLayout.CENTER);

        JPanel jPanel4 = new JPanel();
        jPanel4.setLayout(new GridLayout(2, 3));
        jPanel4.setBackground(new Color(204, 174, 174));
        redplayercolorLabel = new JLabel(TextFields.Red);
        redplayercolorLabel.setVisible(false);
        redplayercolorLabel.setBackground(new Color(255, 0, 51));
        redplayercolorLabel.setOpaque(true);
        redplayercolorLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel4.add(redplayercolorLabel);
        redplayernameLabel = new JLabel(TextFields.Name);
        redplayernameLabel.setVisible(false);
        redplayernameLabel.setBackground(new Color(255, 0, 51));
        redplayernameLabel.setOpaque(true);
        redplayernameLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel4.add(redplayernameLabel);
        redplayertimeTextField = new JTextField(TextFields.Time);
        redplayertimeTextField.setEditable(false);
        redplayertimeTextField.setVisible(false);
        redplayertimeTextField.setBackground(new Color(255, 0, 51));
        redplayertimeTextField.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel4.add(redplayertimeTextField);

        blackplayercolorLabel = new JLabel(TextFields.Black);
        blackplayercolorLabel.setVisible(true);
        blackplayercolorLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        blackplayercolorLabel.setBackground(new Color(204, 174, 174));
        blackplayercolorLabel.setOpaque(true);
        jPanel4.add(blackplayercolorLabel);
        blackplayernameLabel = new JLabel(TextFields.Name);
        blackplayernameLabel.setVisible(true);
        blackplayernameLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel4.add(blackplayernameLabel);
        blackplayertimeTextField = new JTextField();
        blackplayertimeTextField.setBackground(new Color(204, 174, 174));
        blackplayertimeTextField.setEditable(false);
        blackplayertimeTextField.setText(TextFields.Time);
        blackplayertimeTextField.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        blackplayertimeTextField.setVisible(true);
        jPanel4.add(blackplayertimeTextField);
        add(jPanel4, BorderLayout.NORTH);

        JPanel jPanel5 = new JPanel();
        jPanel5.setLayout(new GridLayout(2, 3));
        jPanel5.setBackground(new Color(194, 204, 204));
        whiteplayercolorLabel = new JLabel(TextFields.White);
        whiteplayercolorLabel.setVisible(true);
        whiteplayercolorLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        whiteplayercolorLabel.setBackground(new Color(194, 204, 204));
        whiteplayercolorLabel.setOpaque(true);
        jPanel5.add(whiteplayercolorLabel);
        whiteplayernameLabel = new JLabel(TextFields.Name);
        whiteplayernameLabel.setVisible(true);
        whiteplayernameLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel5.add(whiteplayernameLabel);
        whiteplayertimeTextField = new JTextField(TextFields.Time);
        whiteplayertimeTextField.setBackground(new Color(194, 204, 204));
        whiteplayertimeTextField.setEditable(false);
        whiteplayertimeTextField.setVisible(true);
        whiteplayertimeTextField.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel5.add(whiteplayertimeTextField);

        blueplayercolorLabel = new JLabel(TextFields.Purple);
        blueplayercolorLabel.setVisible(false);
        blueplayercolorLabel.setBackground(new Color(225, 133, 255));
        blueplayercolorLabel.setOpaque(true);
        blueplayercolorLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel5.add(blueplayercolorLabel);
        blueplayernameLabel = new JLabel(TextFields.Name);
        blueplayernameLabel.setVisible(false);
        blueplayernameLabel.setBackground(new Color(225, 133, 255));
        blueplayernameLabel.setOpaque(true);
        blueplayernameLabel.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel5.add(blueplayernameLabel);
        blueplayertimeTextField = new JTextField(TextFields.Time);
        blueplayertimeTextField.setEditable(false);
        blueplayertimeTextField.setVisible(false);
        blueplayertimeTextField.setBackground(new Color(225, 133, 255));
        blueplayertimeTextField.setBorder(BorderFactory.createLineBorder(Color.GRAY));
        jPanel5.add(blueplayertimeTextField);
        add(jPanel5, BorderLayout.SOUTH);

        JPanel jPanel6 = new JPanel();
        jPanel6.setBackground(new Color(204, 174, 174));
        jPanel6.setMinimumSize(new Dimension(40, 20));
        jPanel6.setPreferredSize(new Dimension(40, 20));
        add(jPanel6, BorderLayout.EAST);

        JPanel jPanel7 = new JPanel();
        jPanel7.setBackground(new Color(194, 204, 204));
        jPanel7.setMinimumSize(new Dimension(40, 20));
        jPanel7.setPreferredSize(new Dimension(40, 20));
        add(jPanel7, BorderLayout.WEST);
    }

    public JTextField getWhiteplayertimeTextField() {
        return whiteplayertimeTextField;
    }

    public JTextField getBlackplayertimeTextField() {
        return blackplayertimeTextField;
    }

    public JTextArea getjTextArea1() {
        return jTextArea1;
    }

    public JLabel getWhiteplayernameLabel() {
        return whiteplayernameLabel;
    }

    public JLabel getRedplayercolorLabel() {
        return redplayercolorLabel;
    }

    public JLabel getBlueplayercolorLabel() {
        return blueplayercolorLabel;
    }

    public JLabel getWhiteplayercolorLabel() {
        return whiteplayercolorLabel;
    }

    public JLabel getBlackplayercolorLabel() {
        return blackplayercolorLabel;
    }

    public JLabel getRedplayernameLabel() {
        return redplayernameLabel;
    }

    public JLabel getBlueplayernameLabel() {
        return blueplayernameLabel;
    }

    public JTextField getRedplayertimeTextField() {
        return redplayertimeTextField;
    }

    public JTextField getBlueplayertimeTextField() {
        return blueplayertimeTextField;
    }

    public JLabel getBlackplayernameLabel() {
        return blackplayernameLabel;
    }
}
