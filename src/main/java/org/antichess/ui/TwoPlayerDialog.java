/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.ui;

import org.antichess.controller.Controller;
import org.antichess.model.Sides;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Calendar;
import java.util.Objects;

/**
 * Two player new game dialog
 */
public class TwoPlayerDialog extends JDialog {

    private final JTextField player1nameTextField;
    private final JRadioButton player1whitesideButton;
    private final JRadioButton player1blacksideButton;
    private final JLabel player1styleLabel;
    private final JComboBox<String> player1styleComboBox;
    private final JTextField player1timeTextField;

    private final JTextField player2nameTextField;
    private final JRadioButton player2whitesideButton;
    private final JRadioButton player2blacksideButton;
    private final JLabel player2styleLabel;
    private final JComboBox<String> player2styleComboBox;
    private final JTextField player2timeTextField;

    private final JTextField gameTextField;

    private final JRadioButton playersRadioButton2;
    private final JRadioButton playersRadioButton1;

    private final JCheckBox cylinderCheckBox;

    private final JButton gameokButton;


    public TwoPlayerDialog(Frame parent) {
        super(parent);

        setModal(true);
        setSize(new Dimension(320, 360));
        setLayout(new GridBagLayout());
        setTitle(TextFields.TwoPlayerTitle);
        setLocation(100, 100);
        setResizable(false);

        ButtonGroup playersGroup = new ButtonGroup();
        ButtonGroup player1sideGroup = new ButtonGroup();
        ButtonGroup player2sideGroup = new ButtonGroup();

        // panel
        JPanel jPanel10 = new JPanel();
        jPanel10.setLayout(new GridBagLayout());
        jPanel10.setBorder(new javax.swing.border.TitledBorder(TextFields.Player1));

        JLabel player1nameLabel = new JLabel();
        player1nameLabel.setText(TextFields.Name);
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel10.add(player1nameLabel, gridBagConstraints);

        JLabel player1sideLabel = new JLabel();
        player1sideLabel.setText(TextFields.Side);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel10.add(player1sideLabel, gridBagConstraints);

        player1nameTextField = new JTextField();
        player1nameTextField.setText(" ");
        player1nameTextField.setPreferredSize(new Dimension(100, 25));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel10.add(player1nameTextField, gridBagConstraints);

        player1whitesideButton = new JRadioButton();
        player1whitesideButton.setText(TextFields.White);
        player1whitesideButton.addActionListener(this::player1whitesideButtonActionPerformed);
        player1sideGroup.add(player1whitesideButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanel10.add(player1whitesideButton, gridBagConstraints);

        player1blacksideButton = new JRadioButton();
        player1blacksideButton.setText(TextFields.Black);
        player1blacksideButton.addActionListener(this::player1blacksideButtonActionPerformed);

        player1sideGroup.add(player1blacksideButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        jPanel10.add(player1blacksideButton, gridBagConstraints);

        player1styleLabel = new JLabel();
        player1styleLabel.setText(TextFields.Style);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel10.add(player1styleLabel, gridBagConstraints);

        player1styleComboBox = new JComboBox<>();
        player1styleComboBox.setModel(new DefaultComboBoxModel<>(new String[]{"Aggressive", "depth 1", "depth 2", "depth 3", "depth 4", "depth 5", "depth 6", "depth 7", "depth 8", "depth 9", "depth 10", "Dumb"}));

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel10.add(player1styleComboBox, gridBagConstraints);

        JLabel player1timeLabel = new JLabel();
        player1timeLabel.setText(TextFields.Time);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel10.add(player1timeLabel, gridBagConstraints);

        player1timeTextField = new JTextField();
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel10.add(player1timeTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        getContentPane().add(jPanel10, gridBagConstraints);

        // panel
        JPanel jPanel11 = new JPanel();
        jPanel11.setLayout(new GridBagLayout());
        jPanel11.setBorder(new javax.swing.border.TitledBorder(TextFields.Player2));

        JLabel player2nameLabel = new JLabel();
        player2nameLabel.setText(TextFields.Name);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel11.add(player2nameLabel, gridBagConstraints);

        JLabel player2sideLabel = new JLabel();
        player2sideLabel.setText(TextFields.Side);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel11.add(player2sideLabel, gridBagConstraints);

        player2whitesideButton = new JRadioButton();
        player2whitesideButton.setText(TextFields.White);
        player2whitesideButton.addActionListener(this::player2whitesideButtonActionPerformed);

        player2sideGroup.add(player2whitesideButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        jPanel11.add(player2whitesideButton, gridBagConstraints);

        player2blacksideButton = new JRadioButton();
        player2blacksideButton.setText(TextFields.Black);
        player2blacksideButton.addActionListener(this::player2blacksideButtonActionPerformed);

        player2sideGroup.add(player2blacksideButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        jPanel11.add(player2blacksideButton, gridBagConstraints);

        player2nameTextField = new JTextField(" ");
        player2nameTextField.setPreferredSize(new Dimension(100, 25));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel11.add(player2nameTextField, gridBagConstraints);

        player2styleLabel = new JLabel(TextFields.Style);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel11.add(player2styleLabel, gridBagConstraints);

        player2styleComboBox = new JComboBox<>();
        player2styleComboBox.setModel(new DefaultComboBoxModel<>(new String[]{"Aggressive", "depth 1", "depth 2", "depth 3", "depth 4", "depth 5", "depth 6", "depth 7", "depth 8", "depth 9", "depth 10", "Dumb"}));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel11.add(player2styleComboBox, gridBagConstraints);

        JLabel player2timeLabel = new JLabel();
        player2timeLabel.setText(TextFields.Time);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel11.add(player2timeLabel, gridBagConstraints);

        player2timeTextField = new JTextField();
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel11.add(player2timeTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        getContentPane().add(jPanel11, gridBagConstraints);

        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                gamecancelButtonActionPerformed(null);

            }
        });

        // panel
        JPanel jPanel12 = new JPanel();

        gameokButton = new JButton();
        gameokButton.setText(TextFields.OK);
        jPanel12.add(gameokButton);

        JButton gamecancelButton = new JButton();
        gamecancelButton.setText(TextFields.Cancel);
        gamecancelButton.addActionListener(this::gamecancelButtonActionPerformed);
        jPanel12.add(gamecancelButton);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        getContentPane().add(jPanel12, gridBagConstraints);

        // panel
        JPanel jPanel13 = new JPanel();
        jPanel13.setLayout(new GridBagLayout());

        gameTextField = new JTextField();
        gameTextField.setHorizontalAlignment(JTextField.LEFT);
        gameTextField.setText(" ");
        gameTextField.setPreferredSize(new Dimension(159, 25));
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel13.add(gameTextField, gridBagConstraints);

        JLabel gamestyleLabel = new JLabel();
        gamestyleLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        gamestyleLabel.setText(TextFields.GameStyle);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel13.add(gamestyleLabel, gridBagConstraints);

        JLabel gameLabel = new JLabel();
        gameLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        gameLabel.setText(TextFields.GameName);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        jPanel13.add(gameLabel, gridBagConstraints);

        playersRadioButton1 = new JRadioButton();
        playersRadioButton1.setFont(new Font("Dialog", Font.PLAIN, 12));
        playersRadioButton1.setText(TextFields.Human + TextFields.Vs + TextFields.Human);
        playersGroup.add(playersRadioButton1);
        playersRadioButton1.addActionListener(this::playersRadioButton1ActionPerformed);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel13.add(playersRadioButton1, gridBagConstraints);

        playersRadioButton2 = new JRadioButton();
        playersRadioButton2.setFont(new Font("Dialog", Font.PLAIN, 12));
        playersRadioButton2.setText(TextFields.Human + TextFields.Vs + TextFields.Computer);
        playersGroup.add(playersRadioButton2);
        playersRadioButton2.addActionListener(this::playersRadioButton2ActionPerformed);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel13.add(playersRadioButton2, gridBagConstraints);

        JRadioButton playersRadioButton3 = new JRadioButton();
        playersRadioButton3.setFont(new Font("Dialog", Font.PLAIN, 12));
        playersRadioButton3.setText(TextFields.Computer + TextFields.Vs + TextFields.Computer);
        playersGroup.add(playersRadioButton3);
        playersRadioButton3.addActionListener(this::playersRadioButton3ActionPerformed);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel13.add(playersRadioButton3, gridBagConstraints);

        JLabel boardstyleLabel = new JLabel();
        boardstyleLabel.setText(TextFields.BoardStyle);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel13.add(boardstyleLabel, gridBagConstraints);

        cylinderCheckBox = new JCheckBox();
        cylinderCheckBox.setFont(new Font("Dialog", Font.PLAIN, 12));
        cylinderCheckBox.setText(TextFields.Cylinderical);
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 4;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        jPanel13.add(cylinderCheckBox, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = 2;
        gridBagConstraints.ipady = 30;
        getContentPane().add(jPanel13, gridBagConstraints);

    }

    private void gamecancelButtonActionPerformed(ActionEvent evt) {
        //GameController should die now
        // controller = null; TODO no controller here
        setVisible(false);
    }

    private void playersRadioButton3ActionPerformed(ActionEvent evt) {
        player1nameTextField.setText(TextFields.Computer);
        player2nameTextField.setText(TextFields.Computer);
        player1styleLabel.setEnabled(true);
        player1styleComboBox.setEnabled(true);
        player2styleLabel.setEnabled(true);
        player2styleComboBox.setEnabled(true);

    }

    private void playersRadioButton2ActionPerformed(ActionEvent evt) {
        player1nameTextField.setText(TextFields.Human);
        player2nameTextField.setText(TextFields.Computer);
        player2styleComboBox.setEnabled(true);
        player1styleLabel.setEnabled(false);
        player2styleLabel.setEnabled(true);
        player1styleComboBox.setEnabled(false);
    }

    private void playersRadioButton1ActionPerformed(ActionEvent evt) {
        player1nameTextField.setText(TextFields.Human);
        player2nameTextField.setText(TextFields.Human);
        player2styleComboBox.setEnabled(false);
        player1styleLabel.setEnabled(false);
        player2styleLabel.setEnabled(false);
        player1styleComboBox.setEnabled(false);
    }

    private void player2blacksideButtonActionPerformed(ActionEvent evt) {
        player1whitesideButton.setSelected(true);
        player2blacksideButton.setSelected(true);
    }

    private void player2whitesideButtonActionPerformed(ActionEvent evt) {
        player2whitesideButton.setSelected(true);
        player1blacksideButton.setSelected(true);
    }

    private void player1blacksideButtonActionPerformed(ActionEvent evt) {
        player1blacksideButton.setSelected(true);
        player2whitesideButton.setSelected(true);
    }

    private void player1whitesideButtonActionPerformed(ActionEvent evt) {
        player1whitesideButton.setSelected(true);
        player2blacksideButton.setSelected(true);
    }

    public void setOkActionListener(ActionListener listener) {
        gameokButton.addActionListener(listener);
    }

    /**
     * Initializes the game by reading the default values
     * from the file supplied, if the filename is null or
     * does not exist, the game is initialized using the standard
     * format. (refer to GameController) and the name is determined
     * using today's month and day.
     *
     * @effects creates a new game controller
     * @requires gameDialog is on, gc!=null
     */
    public void initGameDialog(String gameName, Controller controller) {

        //change the default game to Human vs. Computer
        playersRadioButton2.setSelected(true);
        player1nameTextField.setText(TextFields.Human);
        player2nameTextField.setText(TextFields.Computer);
        //get default values from GameController and put them
        //into the gameDialog
        //for player1 which is white and HumanPLayer
        player1whitesideButton.setSelected(true);
        player1timeTextField.setText(controller.getTimeLeftString(Sides.WHITE));
        player1styleComboBox.setEnabled(false);
        player1styleLabel.setEnabled(false);

        //for player2 which is black and AChess Player
        //it plays with style 0 as default
        player2blacksideButton.setSelected(true);
        player2timeTextField.setText(controller.getTimeLeftString(Sides.BLACK));

        player2styleComboBox.setEnabled(true);
        player2styleComboBox.setSelectedIndex(0);
        player2styleLabel.setEnabled(true);

        //board type=Board.REGULAR
        cylinderCheckBox.setSelected(false);

        //game name
        Calendar rightNow = Calendar.getInstance();

        //deal with the name
        gameTextField.setText(Objects.requireNonNullElseGet(gameName, () -> "New_Game" + (rightNow.get(Calendar.MONTH) + 1) + "_" + rightNow.get(Calendar.DAY_OF_MONTH) + ".ac"));
    }

    public JTextField getPlayer1nameTextField() {
        return player1nameTextField;
    }

    public JRadioButton getPlayer1whitesideButton() {
        return player1whitesideButton;
    }

    public JRadioButton getPlayer1blacksideButton() {
        return player1blacksideButton;
    }

    public JLabel getPlayer1styleLabel() {
        return player1styleLabel;
    }

    public JComboBox<String> getPlayer1styleComboBox() {
        return player1styleComboBox;
    }

    public JTextField getPlayer1timeTextField() {
        return player1timeTextField;
    }

    public JTextField getPlayer2nameTextField() {
        return player2nameTextField;
    }

    public JRadioButton getPlayer2whitesideButton() {
        return player2whitesideButton;
    }

    public JRadioButton getPlayer2blacksideButton() {
        return player2blacksideButton;
    }

    public JLabel getPlayer2styleLabel() {
        return player2styleLabel;
    }

    public JComboBox<String> getPlayer2styleComboBox() {
        return player2styleComboBox;
    }

    public JTextField getPlayer2timeTextField() {
        return player2timeTextField;
    }

    public JTextField getGameTextField() {
        return gameTextField;
    }

    public JRadioButton getPlayersRadioButton2() {
        return playersRadioButton2;
    }

    public JRadioButton getPlayersRadioButton1() {
        return playersRadioButton1;
    }

    public JCheckBox getCylinderCheckBox() {
        return cylinderCheckBox;
    }

    public JButton getGameokButton() {
        return gameokButton;
    }
}
