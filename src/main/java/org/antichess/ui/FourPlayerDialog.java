/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.ui;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * 4 Player game settings dialog
 */
public class FourPlayerDialog extends JDialog {

    // player 1
    private final JTextField player1nameTextField;
    private final JTextField player1timeTextField;
    private final JComboBox<String> player1typeComboBox;

    // player 2
    private final JTextField player2nameTextField;
    private final JTextField player2timeTextField;
    private final JComboBox<String> player2typeComboBox;

    // player 3
    private final JTextField player3nameTextField;
    private final JTextField player3timeTextField;
    private final JComboBox<String> player3typeComboBox;

    // player 4
    private final JTextField player4nameTextField;
    private final JTextField player4timeTextField;
    private final JComboBox<String> player4typeComboBox;

    public FourPlayerDialog(Frame parent, boolean modal) {
        super(parent, modal);

        getContentPane().setLayout(new GridBagLayout());
        setResizable(false);
        setTitle(TextFields.FourPlayerTitle);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent evt) {
                setVisible(false);
            }
        });

        // 1st player panel
        JPanel player1Panel = new JPanel();
        player1Panel.setLayout(new GridBagLayout());
        player1Panel.setBorder(new TitledBorder(TextFields.Player1));

        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player1nameLabel = new JLabel(TextFields.Name);
        player1Panel.add(player1nameLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player1colorLabel = new JLabel(TextFields.Color);
        player1Panel.add(player1colorLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player1nameTextField = new JTextField(TextFields.Human);
        player1nameTextField.setPreferredSize(new Dimension(100, 25));
        player1Panel.add(player1nameTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player1typeLabel = new JLabel(TextFields.Type);
        player1Panel.add(player1typeLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player1typeComboBox = new JComboBox<>();
        player1typeComboBox.setModel(new DefaultComboBoxModel<>(new String[]{TextFields.Human, TextFields.Computer}));
        player1typeComboBox.addActionListener(this::typeComboBoxActionPerformed1);
        player1Panel.add(player1typeComboBox, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player1timeLabel = new JLabel(TextFields.Time);
        player1Panel.add(player1timeLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JTextField player1colorTextField = new JTextField(TextFields.White);
        player1colorTextField.setBackground(new Color(255, 255, 255));
        player1colorTextField.setEditable(false);
        player1colorTextField.setFont(new Font("Dialog", Font.BOLD, 12));
        player1colorTextField.setHorizontalAlignment(JTextField.CENTER);
        player1Panel.add(player1colorTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player1timeTextField = new JTextField(TextFields.InitialTime);
        player1Panel.add(player1timeTextField, gridBagConstraints);

        // add panel to pane
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(10, 10, 0, 0);
        getContentPane().add(player1Panel, gridBagConstraints);

        // 2nd player panel
        JPanel player2Panel = new JPanel();
        player2Panel.setLayout(new GridBagLayout());
        player2Panel.setBorder(new TitledBorder(TextFields.Player2));

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player2nameLabel = new JLabel(TextFields.Name);
        player2Panel.add(player2nameLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player2colorLabel = new JLabel(TextFields.Color);
        player2Panel.add(player2colorLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player2nameTextField = new JTextField(TextFields.Human);
        player2nameTextField.setPreferredSize(new Dimension(100, 25));
        player2Panel.add(player2nameTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player2typeLabel = new JLabel(TextFields.Type);
        player2Panel.add(player2typeLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player2typeComboBox = new JComboBox<>();
        player2typeComboBox.setModel(new DefaultComboBoxModel<>(new String[]{TextFields.Human, TextFields.Computer}));
        player2typeComboBox.addActionListener(this::typeComboBoxActionPerformed2);
        player2Panel.add(player2typeComboBox, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player2timeLabel = new JLabel(TextFields.Time);
        player2Panel.add(player2timeLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player2timeTextField = new JTextField(TextFields.InitialTime);
        player2Panel.add(player2timeTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JTextField player2colorTextField = new JTextField(TextFields.Red);
        player2colorTextField.setBackground(new Color(255, 0, 51));
        player2colorTextField.setFont(new Font("Dialog", Font.BOLD, 12));
        player2colorTextField.setHorizontalAlignment(JTextField.CENTER);
        player2Panel.add(player2colorTextField, gridBagConstraints);

        // add two player panel to pane
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.insets = new Insets(10, 0, 0, 10);
        getContentPane().add(player2Panel, gridBagConstraints);

        // button panel
        JPanel buttonPanel = new JPanel();

        JButton gameokButton = new JButton(TextFields.OK);
        gameokButton.addActionListener(this::gameokButtonActionPerformed);
        buttonPanel.add(gameokButton);

        JButton gamecancelButton = new JButton(TextFields.Cancel);
        gamecancelButton.addActionListener(evt -> setVisible(false));
        buttonPanel.add(gamecancelButton);

        // add panel to pane
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.gridwidth = 2;
        getContentPane().add(buttonPanel, gridBagConstraints);

        // 3rd player panel
        JPanel player3Panel = new JPanel();
        player3Panel.setLayout(new GridBagLayout());
        player3Panel.setBorder(new TitledBorder(TextFields.Player3));

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player3nameLabel = new JLabel(TextFields.Name);
        player3Panel.add(player3nameLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player3colorLabel = new JLabel(TextFields.Color);
        player3Panel.add(player3colorLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player3nameTextField = new JTextField(TextFields.Human);
        player3nameTextField.setPreferredSize(new Dimension(100, 25));
        player3Panel.add(player3nameTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player3typeLabel = new JLabel(TextFields.Type);
        player3Panel.add(player3typeLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player3typeComboBox = new JComboBox<>();
        player3typeComboBox.setModel(new DefaultComboBoxModel<>(new String[]{TextFields.Human, TextFields.Computer}));
        player3typeComboBox.addActionListener(this::typeComboBoxActionPerformed3);
        player3Panel.add(player3typeComboBox, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player3timeLabel = new JLabel(TextFields.Time);
        player3Panel.add(player3timeLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JTextField player3colorTextField = new JTextField(TextFields.Black);
        player3colorTextField.setEditable(false);
        player3colorTextField.setFont(new Font("Dialog", Font.BOLD, 12));
        player3colorTextField.setHorizontalAlignment(JTextField.CENTER);
        player3Panel.add(player3colorTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player3timeTextField = new JTextField(TextFields.InitialTime);
        player3Panel.add(player3timeTextField, gridBagConstraints);

        // add 3rd player panel to pane
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(0, 10, 10, 0);
        getContentPane().add(player3Panel, gridBagConstraints);

        // 4th player panel
        JPanel player4Panel = new JPanel();
        player4Panel.setLayout(new GridBagLayout());
        player4Panel.setBorder(new TitledBorder(TextFields.Player4));

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player4nameLabel = new JLabel(TextFields.Name);
        player4Panel.add(player4nameLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player4colorLabel = new JLabel(TextFields.Color);
        player4Panel.add(player4colorLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player4nameTextField = new JTextField(TextFields.Human);
        player4nameTextField.setPreferredSize(new Dimension(100, 25));
        player4Panel.add(player4nameTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player4typeLabel = new JLabel(TextFields.Type);
        player4Panel.add(player4typeLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player4typeComboBox = new JComboBox<>();
        player4typeComboBox.setModel(new DefaultComboBoxModel<>(new String[]{TextFields.Human, TextFields.Computer}));
        player4typeComboBox.addActionListener(this::typeComboBoxActionPerformed4);
        player4Panel.add(player4typeComboBox, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JLabel player4timeLabel = new JLabel(TextFields.Time);
        player4Panel.add(player4timeLabel, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        JTextField player4colorTextField = new JTextField(TextFields.Purple);
        player4colorTextField.setBackground(new Color(225, 133, 225));
        player4colorTextField.setEditable(false);
        player4colorTextField.setFont(new Font("Dialog", Font.BOLD, 12));
        player4colorTextField.setHorizontalAlignment(JTextField.CENTER);
        player4Panel.add(player4colorTextField, gridBagConstraints);

        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        player4timeTextField = new JTextField(TextFields.InitialTime);
        player4Panel.add(player4timeTextField, gridBagConstraints);

        // add 4th player panel to pane
        gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.insets = new Insets(0, 0, 10, 10);
        getContentPane().add(player4Panel, gridBagConstraints);

        pack();
    }

    /**
     * Start the game
     */
    private void gameokButtonActionPerformed(ActionEvent evt) { // TODO names, times, types should be part of a model for that dialog
        setVisible(false);

        String[] names = new String[4];
        names[0] = player1nameTextField.getText();
        names[1] = player2nameTextField.getText();
        names[2] = player3nameTextField.getText();
        names[3] = player4nameTextField.getText();

        String[] times = new String[4];
        times[0] = player1timeTextField.getText();
        times[1] = player2timeTextField.getText();
        times[2] = player3timeTextField.getText();
        times[3] = player4timeTextField.getText();

        int[] types = new int[4];
        types[0] = player1typeComboBox.getSelectedIndex();
        types[1] = player2typeComboBox.getSelectedIndex();
        types[2] = player3typeComboBox.getSelectedIndex();
        types[3] = player4typeComboBox.getSelectedIndex();

        ((MainFrame) getOwner()).startfourPlayerGame(names, times, types);

    }

    /**
     * Changes textfield
     */
    private void typeComboBoxActionPerformed1(ActionEvent evt) {
        if (((JComboBox<String>) evt.getSource()).getSelectedIndex() == 0) {
            player1nameTextField.setEditable(true);
            player1nameTextField.setText(TextFields.Human);
        } else {
            player1nameTextField.setEditable(false);
            player1nameTextField.setText(TextFields.Computer);
        }

    }

    /**
     * Changes textfield
     */
    private void typeComboBoxActionPerformed2(ActionEvent evt) {
        if (((JComboBox<String>) evt.getSource()).getSelectedIndex() == 0) {
            player2nameTextField.setEditable(true);
            player2nameTextField.setText(TextFields.Human);
        } else {
            player2nameTextField.setEditable(false);
            player2nameTextField.setText(TextFields.Computer);
        }

    }

    /**
     * Changes textfield
     */
    private void typeComboBoxActionPerformed3(ActionEvent evt) {
        if (((JComboBox<String>) evt.getSource()).getSelectedIndex() == 0) {
            player3nameTextField.setEditable(true);
            player3nameTextField.setText(TextFields.Human);
        } else {
            player3nameTextField.setEditable(false);
            player3nameTextField.setText(TextFields.Computer);
        }

    }

    /**
     * Changes textfield
     */
    private void typeComboBoxActionPerformed4(ActionEvent evt) {
        if (((JComboBox<String>) evt.getSource()).getSelectedIndex() == 0) {
            player4nameTextField.setEditable(true);
            player4nameTextField.setText(TextFields.Human);
        } else {
            player4nameTextField.setEditable(false);
            player4nameTextField.setText(TextFields.Computer);
        }

    }
}
