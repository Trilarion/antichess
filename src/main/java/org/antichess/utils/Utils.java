/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.utils;

import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 * A bunch of static methods.
 */
public final class Utils {

    private Utils() {
    }

    /**
     * Reads a file's contents into a string
     * takes String
     **/
    public static String fileRead(@NotNull String filename) {
        StringBuilder answer = new StringBuilder();
        try {
            BufferedReader in;
            //if (filename.equals(standardGameFile))
            in = new BufferedReader(new InputStreamReader(Utils.class.getResourceAsStream(filename)));
            //else
            //in= new BufferedReader(new FileReader(filename));
            // read each line until the end of the file and parse it into the script
            for (String line = in.readLine(); line != null; line = in.readLine()) {
                answer.append(line).append("\n");

            }
        } catch (Exception e) {
            // e.printStackTrace();
            throw new RuntimeException("File not accessible\n" + filename);
        }
        //System.out.println ("File read:\n" + answer);
        return answer.toString();
    }

    /**
     * Converts a time string such as [minutes]:[seconds] to an long value
     * specifying the time in milliseconds.
     *
     * @param time
     * @return
     */
    public static int convertTimeStringToValue(@NotNull String time) {
        String[] parts = time.split(":");
        if (parts.length != 2) {
            throw new RuntimeException("Invalid time string. Need [minutes]:[seconds].");
        }
        int minutes = Integer.parseInt(parts[0]);
        int seconds = Integer.parseInt(parts[1]);
        return (minutes * 60 + seconds) * 1000;
    }

    /**
     * Converts a time value given in milliseconds to [minutes]:[seconds] with seconds being
     * padded by zeros to have at least width 2.
     *
     * @param value
     * @return
     */
    public static String convertValueToTimeString(int value) {
        int seconds = value / 1000;
        int minutes = seconds / 60;
        seconds %= 60;
        return String.format("%d:%02d", minutes, seconds);
    }
}
