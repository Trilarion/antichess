/*
 * AntiChess
 * Copyright (C) 2021 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.utils;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Type-safe, thread safe Observable for the Observer interface. Uses CopyOnWriteArrayList for thread safety.
 */
public class Observable<E> {
    private final Collection<Observer<E>> observers = new CopyOnWriteArrayList<>();

    public void addObserver(@NotNull Observer<E> observer) {
        observers.add(observer);
    }

    public void removeObserver(@NotNull Observer<E> observer) {
        observers.remove(observer);
    }

    protected void notifyObservers(@NotNull E notification) {
        for (Observer<E> observer : observers) {
            observer.update(notification);
        }
    }
}
