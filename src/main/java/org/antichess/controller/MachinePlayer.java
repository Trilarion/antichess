/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.controller;

import org.antichess.engine.BetterEngine;
import org.antichess.engine.EvaluatedMove;
import org.antichess.engine.EvaluatedMoveType;
import org.antichess.engine.NewEngine;
import org.antichess.model.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

/**
 * MachinePlayer is the machine player for AChess Program.
 **/
public class MachinePlayer { // TODO extract player as interface

    private final Board board;
    private final int level;
    private final Map<Long, EvaluatedMove> boardTable;
    private long startTime;
    private long halfTime;
    private int color;
    private Controller controller;
    private BetterEngine engine;

    /**
     * @requires bd is a string representing a valid board, as
     * described in Figure 5.
     * @effects Creates a new MachinePlayer in the same way described
     * by the constructor MachinePlayer(boolean, int), except
     * that the initial board configuration is set to be the one
     * described by bd.
     */
    public MachinePlayer(boolean isWhite, int level, String bd, boolean cylinder) {
        // ** NOT FULLY IMPLEMENTED

        //Read and find out about the board
        Controller controller1 = new Controller();
        controller1.readFromString(bd);
        controller = controller1;
        board = new Board(controller.getBoard());
        if (cylinder)
            board.setBoardType(BoardType.CYLINDER);
        else board.setBoardType(BoardType.REGULAR);
        startTime = controller.getTimeLeft(color);
        halfTime = startTime / 2;
        this.level = level;
        color = (isWhite ? Sides.WHITE : Sides.BLACK);
        boardTable = new Hashtable<>();
        engine = new BetterEngine(board, boardTable,
                ((level == 0) ? 22 : level),
                color);
        loadOpenBook();
    }

    /**
     * Constructs a machineplayer from a given board. For 4 players
     **/
    public MachinePlayer(int color, int level, Board board) {
        this.board = new Board(board);
        this.level = level;
        this.color = color;
        boardTable = new Hashtable<>();
    }

    /**
     * @requires opponentMove is either a string representing a valid
     * move by the opponent on the board stored in this (see
     * Figure 4), or the empty string. timeLeft and
     * opponentTimeLeft are both {@literal >} 0
     * @effects returns a valid next move for this player in a string
     * of the appropriate format, given the opponent's move,
     * the time left for this player, and the time left for the
     * opponent. Both times are in milliseconds. If
     * opponentMove is the empty string, then the board for this
     * player should be considered up to date (as would be the
     * case if this player is asked to make the first move of the
     * game).
     * <p>
     * NOTE: This procedure may run greater than timeLeft, but
     * this would mean losing the game.
     * @modifies this
     */
    public String makeMove(String opponentMove, int timeLeft, int opponentTimeLeft) {
        List<Move> validMoves;

        if (opponentMove != null) {
            board.makeMove(new Move(opponentMove));
        }

        if (startTime == 0) {
            startTime = timeLeft;
            halfTime = startTime / 2;
        }

        System.out.println("timeleft" + timeLeft);
        long TIME_LIMIT;
        if (timeLeft > (1.9 * halfTime)) {
            System.out.println("1.ceyrek");
            TIME_LIMIT = startTime / 60;
        } else if (timeLeft < (1.9 * halfTime) && (timeLeft > (0.5 * halfTime))) {
            System.out.println("2.ceyrek");
            TIME_LIMIT = startTime / 20;
        } else if ((timeLeft > (halfTime / 6)) && (timeLeft < (0.5 * halfTime))) {
            System.out.println("3ceyrek");
            TIME_LIMIT = timeLeft / 25;
        } else {
            System.out.println("4.ceyrek");
            TIME_LIMIT = timeLeft / 100;
        }

        // check to see if we need to do our own search
        Move move = null;
        if (move == null) {
            // Do our own search here

            if (level == 11) {
                //Choose random player
                move = NewEngine.randomMove(board, color);
                // NewEngine.randomMove made move
                return move.toString(); // we do not ponder when playing randomly
            } else {

                // Start a new thread and terminate it if we run out of time
                System.out.println("starting thread with TIME_LIMIT: " + TIME_LIMIT);
                engine.setSearchInProgress(true);
                engine.setSearchFinished(false);
                Thread thread = new Thread(engine);
                thread.start();
                long threadStartTime = System.currentTimeMillis();

                while (!engine.isSearchFinished()) {
                    try {
                        Thread.sleep(200L);
                    } catch (Exception e) {
                    }

                    if (System.currentTimeMillis() - threadStartTime >= TIME_LIMIT) {
                        System.out.println("search out time passed: " + (System.currentTimeMillis() - threadStartTime));
                        engine.setSearchInProgress(false);
                        // Wait for thread to finish
                        while (!engine.isSearchFinished()) {
                            try {
                                Thread.sleep(200L);
                            } catch (Exception e) {
                            }
                        }
                        break;
                    }
                }
                move = engine.getFoundMove();
            }

        }

        // Apply the move
        if (move == null) {
            System.out.println("bestMove should not be null");
            // throw new RuntimeException("move returned was null");
            System.out.println(board);
            validMoves = board.getValidMoves(color);
            move = validMoves.get(0);
            board.makeGeneratedMove(move);
            //System.out.println ("applied move: " + m + "\n" + board);
            return (move.toString());
        }
        validMoves = board.getValidMoves(color);
        // check if the move is valid

        if (!validMoves.contains(move)) {
            System.out.println("move returned by BetterEngine is invalid: " + move);
            // throw new RuntimeException("move returned was invalid: " + move);
            //System.out.println (board);
            move = validMoves.get(0);
            board.makeGeneratedMove(move);
            //System.out.println ("applied move: " + m + "\n" + board);
            try {
                Thread.sleep(2000L);
            } catch (Exception e) {
            }
            return (move.toString());

        }
        board.makeGeneratedMove(move);

        return move.toString(); // TODO why a conversion here? not needed really
    }

    /**
     * This is used in four player chess to update every machine player what
     * some other player did, and to get the next move.
     *
     * @param lastMoves a List of movestrings made by the previous players
     * @return the next move this player makes
     **/
    public String makeMove(List<Move> lastMoves) {
        for (Move lastMove : lastMoves) {
            if (lastMove != null) {
                board.makeMove(new Move(lastMove));
                // Machine player made opponent move: " + lastMove + "\n" + board);
            }
        }
        Move move = NewEngine.randomMove(board, color);
        board.makeMove(move);
        return move.toString(); // TODO why a conversion here? not needed really
    }

    /* Gets the board */
    public Board getBoard() {
        return board;
    }

    /**
     * For two player game, if there exists an opening book file for the board style,
     * initializes {@code boardTable} with this opening book
     *
     * @requires gc!=null
     * @modifies boardTable
     */
    private void loadOpenBook() {

        //if this is not a 2-player game return
        if (controller.getPlayers() != Players.TWO)
            return;

        //if boardTAble is not null
        if (boardTable == null)
            return;

        //load the necessary file
        StringBuilder answer = new StringBuilder();

        if (controller.getBoardType() == BoardType.REGULAR) //for regular board
        {

            try {
                BufferedReader in = new BufferedReader(new
                        InputStreamReader(Controller.class.getResourceAsStream("/data/regular.txt")));
                // read each line until the end of the file and parse it into the script
                for (String line = in.readLine(); line != null; line = in.readLine())
                    answer.append(line).append("\n");

            } catch (Exception e) {
                System.out.println("No regular.txt in where GameController.class is");
                //e.printStackTrace();
                return;
            }

        } else { //for cylindrical board
            try {
                BufferedReader in = new BufferedReader(new
                        InputStreamReader(Controller.class.getResourceAsStream("/data/cylinder.txt")));
                // read each line until the end of the file and parse it into the script
                for (String line = in.readLine(); line != null; line = in.readLine())
                    answer.append(line).append("\n");

            } catch (Exception e) {
                System.out.println("No cylinder.txt in where GameController.class is");
                //		e.printStackTrace();
                return;
            }
        }

        //now, answer has all the opening book entries

        StringTokenizer fileLine = new StringTokenizer(answer.toString(), "\n");

        //following values are read from the file
        //and they are necessary to create a HashEntry
        int nextTurn; //next player color to move
        long key;     //key of the Entry
        Move bestMove; //move of hte Entry
        EvaluatedMoveType type;     //type of the Entry
        int value;      //value of the Entry
        int depth;    //depth of the Entry
        StringBuilder token; //token we are talking about

        EvaluatedMove entry; //specific hashEntry

        label:
        while (fileLine.hasMoreTokens())
        //parse each line accordingly
        {
            //take nextTurn
            token = new StringBuilder(fileLine.nextToken());

            if ("white".equals(token.toString()))
                nextTurn = Sides.WHITE;
            else {
                if ("black".equals(token.toString()))
                    nextTurn = Sides.BLACK;
                else {
                    System.out.println("Error in getting the color");
                    break;
                }
            }

            //take bestMove
            bestMove = new Move(fileLine.nextToken());

            //take type
            token = new StringBuilder(fileLine.nextToken());

            switch (token.toString()) {
                case "exact":
                    type = EvaluatedMoveType.EXACT;
                    break;
                case "alpha":
                    type = EvaluatedMoveType.ALPHA;
                    break;
                case "beta":
                    type = EvaluatedMoveType.BETA;
                    break;
                default:
                    System.out.println("Error in getting the type");
                    break label;
            }

            //take value
            token = new StringBuilder(fileLine.nextToken());
            value = Integer.parseInt(token.toString());

            //take depth
            token = new StringBuilder(fileLine.nextToken());
            depth = Integer.parseInt(token.toString());

            //create the key
            //read the board string in
            token = new StringBuilder();
            for (int i = Board.getMINROW(); i <= Board.getMAXROW(); i++)
                token.append(fileLine.nextToken());
            key = board.zobristCode(token.toString(), nextTurn);

            //create the hashEntry and put it into the table
            entry = new EvaluatedMove(key, bestMove, type, value, depth);
            boardTable.put(key, entry);

            //System.out.println("Added entry:"+entry);

        }
        //System.out.println("End of opening file entry");
    }
}
