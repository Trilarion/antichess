/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.controller;

import org.antichess.model.Move;

import java.util.List;

/**
 * <p> HumanPlayer is the human player for AChess Program.
 * It handles the communication between a human and a GameController.
 *
 * <p> When it is a human player's turn, GameController should first call
 * setMoveList() with possible moves. Then, it should wait for getMove()
 * to return a non-null move. When there is a moveMade, UI should first
 * call getMyTurn() and if it returns true, it should call getMoveList()
 * to get a list of possible moves. If the moveMade is among this, UI
 * should call setMove() with moveMade.
 *
 * <p>It is necessarily true that when {@code myTurn} is true,
 * {@code moveList} is non-null, however it can be empty. It is
 * also necessarily true that when a getMove() returns non-null, subsequent
 * calls to getMyTurn() will return false.
 *
 * moveList list of moves
 * move a selected move from the moveList
 * myTurn true if it is HumanPlayers turn
 **/
public class HumanPlayer {

    private List<Move> moveList;
    private Move move;
    private boolean myTurn;

    public HumanPlayer() {
        moveList = null;
        move = null;
        myTurn = false;
    }

    /**
     * Returns whether it is human players turn.
     * Should be called by UI.
     *
     * @return myTurn
     */
    public boolean isMyTurn() {
        return myTurn;
    }

    /**
     * Sets myTurn=turn and move=null. We need to
     * have move set to null, so that when a move
     * is made, GameController will know it by comparing
     * getMove() to null.
     *
     * @effects myTurn=turn && move=null
     */
    private void setMyTurn(boolean turn) {
        move = null;
        myTurn = turn;
    }

    /**
     * Returns move, if move is not null (ie a move is selected by UI)
     * this method sets the myTurn false.
     * Should be called by GameController.
     *
     * @return move
     * @effects if (move!=null) {myTurn=false; move=null}
     */
    public synchronized Move getMove() {
        // Make a copy of move first because
        // setMyTurn will set it to null
        Move move1 = move;

        if (move != null)
            setMyTurn(false);
        return move1;
    }

    /**
     * Sets move, if it is myTurn. Should be
     * called by UI.
     *
     * @requires myTurn==true, move isElementof moveList
     * @effects move
     */
    public synchronized void setMove(Move move) {
        if (myTurn)
            this.move = move;
    }

    /**
     * Returns moveList
     * Should be called by UI.
     *
     * @return moveList
     */
    public synchronized List<Move> getMoveList() {
        return moveList;
    }

    /**
     * Sets moveList. Should be called by GameController.
     *
     * @effects {@code moveList && (if moveList!=null then myTurn=true) && (if moveList==null then myTurn=false)}
     */
    public synchronized void setMoveList(List<Move> moveList) { // TODO this should not affect myturn!
        this.moveList = moveList;
        setMyTurn(this.moveList != null);
    }

}
