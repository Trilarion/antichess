/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.controller;

/**
 * A timer to count down a given time.
 *
 * timeleft: int // milliseconds left
 * isTiming          : boolean   state of timer (on or off)
 */
public class Timer {

    private long timeLeft; // timeleft in milliseconds
    private boolean isTiming;
    private long temp; // the last time retrieved from system

    /**
     * Initializes a timer with starting time
     *
     * @param startTime starting time in milliseconds
     **/
    public Timer(long startTime) {
        timeLeft = startTime;
    }

    public int getTimeLeft() {
        if (isTiming) {
            long timeUsed = System.currentTimeMillis() - temp;
            return (int) (timeLeft - timeUsed);
        } else
            return (int) (timeLeft);
    }

    public void setTimeLeft(int time) {
        timeLeft = time;
    }

    /**
     * See if the timer is running
     *
     * @return true if timer running
     **/
    public boolean getIsTiming() {
        return isTiming;
    }

    // TIMER FUNCTIONS
    public void startTimer() {
        isTiming = true;
        temp = System.currentTimeMillis();
    }

    public void stopTimer() {
        isTiming = false;
        long timeUsed = System.currentTimeMillis() - temp;
        timeLeft -= timeUsed;
    }

}


