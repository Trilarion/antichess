/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.controller;

import org.antichess.Options;
import org.antichess.model.*;
import org.antichess.model.pieces.*;
import org.antichess.utils.Observable;
import org.antichess.utils.Utils;
import org.jetbrains.annotations.NotNull;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.*;

/**
 * This class controls the main flow of the game.
 * This class also performs reading in of a saved game file, and the parsing of strings
 * in the game file format.
 * GameController is mutable.
 *
 * board : Board  /// the resulting board of parsing a string/file
 * nextTurn : White | Black // the color of the next player to make a move
 * timeLeft : int // amount of time left for each player, in ms
 **/
public class Controller extends Observable<Message> implements Runnable {

    private final Timer[] timer = new Timer[Options.maxPlayers];
    private final PlayerType[] playerType = new PlayerType[Options.maxPlayers];
    private final int[] level = new int[Options.maxPlayers]; // TODO level never read
    private final Players players;
    private final List<Move> moveHistory = new ArrayList<>();
    private final MachinePlayer[] machinePlayers = new MachinePlayer[Options.maxPlayers]; // TODO make it a list
    private boolean gameInProgress;
    private Board board;
    private int nextTurn;
    private String gameName = ""; // TODO make all these variables part of a model
    private String newMoveString, oldMoveString;
    private boolean showLegalPieces;
    private boolean showLegalMoves;
    private HumanPlayer humanPlayer = new HumanPlayer();

    /**
     * Creates a new game from the standard file GameController.standardGameFile.
     * Use setPlayer initialize the players and timers;
     * Use readFromFile/readFromString to initialize the board
     * and time and nextTurn information;
     * Use setTime to change the time
     *
     * @effects creates a new gameController, initialized with standard board
     **/
    public Controller() {
        this(Players.TWO);
    }

    /**
     * Creates a game with the specified number of players
     *
     * @requires numPlayers = 2 or 4
     * @modifies Piece.LASTCOLOR to 3 if numPlayers == 4
     **/
    public Controller(@NotNull Players players) {
        this.players = players;
        switch (players) {
            case TWO:
                PieceImplementation.LASTCOLOR = Sides.BLACK;
                // Fills the GameFileManager with information from a file
                readFromString(Utils.fileRead(Options.standardGameFile));
                break;
            case FOUR:
                PieceImplementation.LASTCOLOR = Sides.BLUE;
                // create board
                // Creates a 4 player board

                // Create 4 player topology
                //     int topology[][] = {
                //       {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
                //       {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
                //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                //       {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                //       {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0},
                //       {0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0}};
                int[][] topology = {
                        {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
                        {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
                        {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1},
                        {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
                        {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0},
                        {0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0}};
                board = new Board(topology);
                // Create pieces for each player
                PieceImplementation piece;
                char c; // All pieces of same type have same character, regardless of color

                // WHITE (South)
                // Pawns
                c = 'P';
                piece = new Pawn(c, board, 3, 1, Sides.WHITE);
                piece = new Pawn(c, board, 4, 1, Sides.WHITE);
                piece = new Pawn(c, board, 5, 1, Sides.WHITE);
                piece = new Pawn(c, board, 6, 1, Sides.WHITE);
                piece = new Pawn(c, board, 7, 1, Sides.WHITE);
                piece = new Pawn(c, board, 8, 1, Sides.WHITE);
                piece = new Pawn(c, board, 9, 1, Sides.WHITE);
                piece = new Pawn(c, board, 10, 1, Sides.WHITE);
                // Rooks
                c = 'R';
                piece = new Rook(c, board, 3, 0, Sides.WHITE);
                piece = new Rook(c, board, 10, 0, Sides.WHITE);
                // Knights
                c = 'N';
                piece = new Knight(c, board, 4, 0, Sides.WHITE);
                piece = new Knight(c, board, 9, 0, Sides.WHITE);
                // Bishops
                c = 'B';
                piece = new Bishop(c, board, 5, 0, Sides.WHITE);
                piece = new Bishop(c, board, 8, 0, Sides.WHITE);
                // Queen
                c = 'Q';
                piece = new Queen(c, board, 6, 0, Sides.WHITE);
                // King
                c = 'K';
                piece = new King(c, board, 7, 0, Sides.WHITE);

                // Black (North)
                // Pawns
                c = 'P';
                piece = new Pawn(c, board, 3, 12, Sides.BLACK);
                piece = new Pawn(c, board, 4, 12, Sides.BLACK);
                piece = new Pawn(c, board, 5, 12, Sides.BLACK);
                piece = new Pawn(c, board, 6, 12, Sides.BLACK);
                piece = new Pawn(c, board, 7, 12, Sides.BLACK);
                piece = new Pawn(c, board, 8, 12, Sides.BLACK);
                piece = new Pawn(c, board, 9, 12, Sides.BLACK);
                piece = new Pawn(c, board, 10, 12, Sides.BLACK);
                // Rooks
                c = 'R';
                piece = new Rook(c, board, 3, 13, Sides.BLACK);
                piece = new Rook(c, board, 10, 13, Sides.BLACK);
                // Knights
                c = 'N';
                piece = new Knight(c, board, 4, 13, Sides.BLACK);
                piece = new Knight(c, board, 9, 13, Sides.BLACK);
                // Bishops
                c = 'B';
                piece = new Bishop(c, board, 5, 13, Sides.BLACK);
                piece = new Bishop(c, board, 8, 13, Sides.BLACK);
                // Queen
                c = 'Q';
                piece = new Queen(c, board, 6, 13, Sides.BLACK);
                // King
                c = 'K';
                piece = new King(c, board, 7, 13, Sides.BLACK);


                // Red (West)
                // Pawns
                c = 'P';
                piece = new Pawn(c, board, 1, 3, Sides.RED);
                piece = new Pawn(c, board, 1, 4, Sides.RED);
                piece = new Pawn(c, board, 1, 5, Sides.RED);
                piece = new Pawn(c, board, 1, 6, Sides.RED);
                piece = new Pawn(c, board, 1, 7, Sides.RED);
                piece = new Pawn(c, board, 1, 8, Sides.RED);
                piece = new Pawn(c, board, 1, 9, Sides.RED);
                piece = new Pawn(c, board, 1, 10, Sides.RED);
                // Rooks
                c = 'R';
                piece = new Rook(c, board, 0, 3, Sides.RED);
                piece = new Rook(c, board, 0, 10, Sides.RED);
                // Knights
                c = 'N';
                piece = new Knight(c, board, 0, 4, Sides.RED);
                piece = new Knight(c, board, 0, 9, Sides.RED);
                // Bishops
                c = 'B';
                piece = new Bishop(c, board, 0, 5, Sides.RED);
                piece = new Bishop(c, board, 0, 8, Sides.RED);
                // Queen
                c = 'Q';
                piece = new Queen(c, board, 0, 6, Sides.RED);
                // King
                c = 'K';
                piece = new King(c, board, 0, 7, Sides.RED);

                // blue (east)
                // Pawns
                c = 'P';
                piece = new Pawn(c, board, 12, 3, Sides.BLUE);
                piece = new Pawn(c, board, 12, 4, Sides.BLUE);
                piece = new Pawn(c, board, 12, 5, Sides.BLUE);
                piece = new Pawn(c, board, 12, 6, Sides.BLUE);
                piece = new Pawn(c, board, 12, 7, Sides.BLUE);
                piece = new Pawn(c, board, 12, 8, Sides.BLUE);
                piece = new Pawn(c, board, 12, 9, Sides.BLUE);
                piece = new Pawn(c, board, 12, 10, Sides.BLUE);
                // Rooks
                c = 'R';
                piece = new Rook(c, board, 13, 3, Sides.BLUE);
                piece = new Rook(c, board, 13, 10, Sides.BLUE);
                // Knights
                c = 'N';
                piece = new Knight(c, board, 13, 4, Sides.BLUE);
                piece = new Knight(c, board, 13, 9, Sides.BLUE);
                // Bishops
                c = 'B';
                piece = new Bishop(c, board, 13, 5, Sides.BLUE);
                piece = new Bishop(c, board, 13, 8, Sides.BLUE);
                // Queen
                c = 'Q';
                piece = new Queen(c, board, 13, 6, Sides.BLUE);
                // King
                c = 'K';
                piece = new King(c, board, 13, 7, Sides.BLUE);

                // Fill up board with empty pieces
                board.addEmpty();

                // Set board type
                board.setBoardType(BoardType.FOURPLAYERS);

                // init zobrist
                board.initZobrist();
                // create timer
                for (int color = PieceImplementation.FIRSTCOLOR; color <= PieceImplementation.LASTCOLOR; color++) {
                    timer[color] = new Timer(300000);
                }
                // sets first player to white
                nextTurn = Sides.WHITE;
                break;
        }
    }

    /**
     * Creates a new game from the given filename.
     * Use setPlayer initialize the players and timers;
     * Use readFromFile/readFromString to initialize the board
     * and time and nextTurn information;
     * Use setTime to change the time.
     *
     * @throws RuntimeException if there is an error opening the file
     * @requires filename exists
     **/
    public Controller(String fileName) throws RuntimeException {
        players = Players.TWO;
        PieceImplementation.LASTCOLOR = Sides.BLACK;
        // readFromFile/String will throw an exception which
        // should be handled by the calling code
        // Fills the GameFileManager with information from a file
        readFromString(Utils.fileRead(Objects.requireNonNullElse(fileName, Options.standardGameFile)));
    }

    /**
     * Writes the game file to the specified filename, in game file format.
     *
     * @param controller
     * @param fileName   full path of file to write to
     **/
    public static void saveGameToFile(Controller controller, String fileName) {
        try {
            PrintStream output = new PrintStream(new FileOutputStream(fileName));
            output.println(controller.getBoardString());
        } catch (Exception e) {
            throw new RuntimeException("ERROR WRITING TO " + fileName + "\n" + e);
        }
    }

    /**
     * Gets the number of the player
     **/
    public Players getPlayers() {
        return players;
    } // TODO should be part of the model

    /**
     * Gets if the legally moved pieces should be moved
     **/
    public boolean getShowLegalPieces() {
        return showLegalPieces;
    } // TODO should be part of a display model

    /**
     * Sets if the legally moved pieces should be moved
     **/
    public void setShowLegalPieces(boolean value) {
        showLegalPieces = value;
    }

    /**
     * Gets if the legally moved pieces should be moved
     **/
    public boolean getShowLegalMoves() {
        return showLegalMoves;
    }

    /**
     * Sets if the legally moved pieces should be moved
     **/
    public void setShowLegalMoves(boolean value) {
        showLegalMoves = value;
    }

    /**
     * Gets the humanPlayer object.
     *
     * @return the humanPlayer object
     **/
    public HumanPlayer getHumanPlayer() {
        return humanPlayer;
    }

    /**
     * Gets the name of the game
     *
     * @return name of the game
     **/
    public @NotNull String getName() {
        return gameName;

    }

    /**
     * Sets the name of the game
     *
     * @effects name of the game
     **/
    public void setName(@NotNull String name) {
        if (gameInProgress)
            return;
        gameName = name;

    }

    /**
     * Gets the time left for a particular color
     *
     * @param color the color of the player to get the timeleft for
     * @requires color = {Piece.WHITE | Piece.BLACK}
     * @return amount of time left in ms
     **/
    public int getTimeLeft(int color) { // TODO should be part of a model
        if (color < players.getNumber())
            return timer[color].getTimeLeft();
        return 0;
    }

    /**
     * Gets the time left for a particular color
     *
     * @param color the color of the player to get the timeleft for
     * @requires color = {Piece.WHITE | Piece.BLACK}
     * @return amount of time left in [minutes]:[seconds] format
     * appends '0' if [seconds] is one digit
     **/
    public String getTimeLeftString(int color) { // TODO make utility method (seconds to time) and use only getTimeLeft
        int l = getTimeLeft(color);
        return Utils.convertValueToTimeString(l);
    }

    /**
     * Sets the time left for a player given a time string
     * such as [minutes]:[seconds], if the time string has multiple
     * ':', then the first one is used to separate [minutes] and
     * [seconds]
     *
     * @requires color = {Piece.BLACK | Piece.WHITE}
     * @modifies this, this.timer[color]
     **/
    public void setTimeLeftString(int color, String timeLeft) { // TODO better expose set time left
        timer[color].setTimeLeft(Utils.convertTimeStringToValue(timeLeft));
    }

    /**
     * Gets the Board representing the state of the game
     *
     * @return board representing state of game
     **/
    public Board getBoard() {
        return board;
    }

    /**
     * Gets the color of the next player to make a move
     *
     * @return {Piece.WHITE | Piece.BLACK}
     **/
    public int getNextTurn() {
        return nextTurn;
    }

    /**
     * Sets the color of the nextplayer to make a move
     *
     * @modifies this.nextTurn
     * @requires next = {Piece.WHITE | Piece.BLACK}
     **/
    public void setNextTurn(int next) {
        nextTurn = next;
    }

    /**
     * Gets the type of board to use
     *
     * @return boardType = Board.{CYLINDER | REGULAR}
     **/
    public BoardType getBoardType() { // TODO should be part of a Model
        return board.getBoardType();
    }

    /**
     * Sets the type of board to use
     *
     * @requires boardType = Board.{CYLINDER | REGULAR}
     **/
    public void setBoardType(BoardType type) {
        board.setBoardType(type);
    }

    /**
     * Gets the last three moves made in the game. If number of moves
     * is less than 3, all previous moves are returned.
     */
    private List<Move> getLast3Moves() {
        List<Move> l = new ArrayList<>();
        for (int i = Math.max(0, moveHistory.size() - 3); i < moveHistory.size(); i++) {
            l.add(moveHistory.get(i));
        }
        return l;
    }

    /**
     * Checks if the game is over
     */
    private boolean checkGameOver(List<Move> validMoves) { // TODO why is this only called in start game? how is game over determined otherwise?
        // System.out.println("Checking game over for: " + PieceImplementation.playerColorString[nextTurn] + "\n" + validMoves);
        if (board.isGameOver(nextTurn, validMoves)) {
            if (board.isTie(nextTurn, validMoves)) {
                notifyObservers(new Message("Draw!"));
            } else { // Assume that it is a win
                //notifyObservers(new GameMessage((nextTurn == Piece.WHITE ? "White" : "Black") +" wins!"));
                notifyObservers(new Message(PieceImplementation.playerColorString[nextTurn] + " wins!"));
            }
            return true;
        }
        return false;
    }

    /**
     * Sets gameInProgress variable
     * Should be used by UI's to indicate that game is Over!
     *
     * @effects gameInProgress
     **/
    public void setGameInProgress(boolean value) { // TODO should be part of a model
        gameInProgress = value;
    }

    /**
     * Makes a new player and timer with the given color, type and level.
     *
     * @requires {@code color = Piece.{WHITE | BLACK} && type == {HUMAN | MACHINE} && gameInProgress == false}
     * @effects creates a new MachinePlayer if {@code type == MACHINE}
     **/
    public void setPlayer(int color, PlayerType type, int level) {
        if (gameInProgress)
            return;
        playerType[color] = type;
        this.level[color] = level;
        if (type == PlayerType.MACHINE) {
            // check number of players
            if (PieceImplementation.LASTCOLOR == Sides.BLACK)
                machinePlayers[color] = new MachinePlayer(color == Sides.WHITE, level, getBoardString(), false);
            else
                machinePlayers[color] = new MachinePlayer(color, level, getBoard());

        } else if (type == PlayerType.HUMAN) {
            humanPlayer = new HumanPlayer(); // TODO what if there are more than one human players?
        } else
            throw new RuntimeException("GameController.setPlayer: invalid player type");
    }

    /**
     * Starts the game and sets gameInProgress to true; NOT FULLY IMPLEMENTED;
     * see code for more details
     **/
    public void run() {
        runGame(); // TODO when closing the mainframe we are not waiting for this thread??
    }

    // String and File Parsing // TODO move to file utils if possible

    /**
     * Starts the game and sets gameInProgress to true; NOT FULLY IMPLEMENTED;
     * see code for more details
     **/
    private void runGame() {
        gameInProgress = true;
        newMoveString = null;
        oldMoveString = null;
        List<Move> validMoves = board.getAllValidMoves(nextTurn);

        // check if the game is already over
        if (checkGameOver(validMoves)) {
            return;
        }

        // We only need to do this for human player in textUI if a human player starts a game
        // This should not have any effect on GUI
        if (playerType[nextTurn] == PlayerType.HUMAN) {
            humanPlayer.setMoveList(validMoves);
        } else {
            humanPlayer.setMoveList(null);
        }

        // Start timer
        boolean timedGame = true;
        if (timedGame)
            timer[nextTurn].startTimer();

        // central loop
        while (gameInProgress) {
            // Getting moves from players

            // Get the valid moves for the current player
            if (playerType[nextTurn] == PlayerType.MACHINE) {
                // Getting move from machine player

                // starting a thread which listens to the machine player
                newMoveString = null; // make the move null first
                if (players == Players.FOUR) {
                    // get the last 3 moves and give to the next player
                    newMoveString = machinePlayers[nextTurn].makeMove(getLast3Moves());
                } else {
                    // TWO Players
                    (new Thread(() -> newMoveString = machinePlayers[nextTurn].makeMove(oldMoveString, timer[nextTurn].getTimeLeft(),
                            timer[Helper.getOpponentColor(nextTurn)].getTimeLeft()))).start();
                }

                // wait for the thread to finish (obviously it never happens currently)
                while (newMoveString == null && timer[nextTurn].getTimeLeft() > 0 && gameInProgress) {
                }

            } else if (playerType[nextTurn] == PlayerType.HUMAN) {
                // get a string input from GUI
                Move humanMove = null;
                // Getting move from human player

                // System.out.println("GetMyTurn: " + humanPlayer.getMyTurn());
                // loops until there is a move
                while (humanMove == null && timer[nextTurn].getTimeLeft() > 0 && gameInProgress) {
                    humanMove = humanPlayer.getMove();
                }
                // System.out.println("Gotten move from human: " + humanMove);
                if (humanMove != null)
                    newMoveString = humanMove.toString();
            }

            // stop timer
            if (timedGame)
                timer[nextTurn].stopTimer();

            // Check for time constraints and notify users if they are violated
            if (timer[nextTurn].getTimeLeft() <= 0) {//Time is up, game ended
                // System.out.println("Time has ended:Notifying observers");
                //notify observers that there has been a move by the last player
                //notifyObservers(new GameMessage((Piece.WHITE==nextTurn ? "Black":"White")+" wins: Time run out!"));
                notifyObservers(new Message(PieceImplementation.playerColorString[Helper.getOpponentColor(nextTurn)]
                        + " wins: " + PieceImplementation.playerColorString[nextTurn] + " run out of time!"));
                //now return from GameController thread
                return;
            }

            if (gameInProgress) {
                //make oldMoveString to point to the old move
                oldMoveString = newMoveString;
                // create move and update the board
                Move move = new Move(newMoveString);
                // System.out.println("gc:\n" + board);
                // System.out.println("validmoves: \n" + validMoves);
                System.out.println(PieceImplementation.playerColorString[nextTurn] + " Move: " + move + "(string: " + newMoveString + ")");
                // update the board
                board.makeMove(move);
                moveHistory.add(move);
                // TODO maybe there should be only one board for each game!
                // check that machine player's board is updated
                if (playerType[nextTurn] == PlayerType.MACHINE) {
                    if (!machinePlayers[nextTurn].getBoard().toString().equals(board.toString())) {
                        System.out.println("Machine board:\n" + machinePlayers[nextTurn].getBoard());
                        System.out.println("GC board:\n" + board);
                        throw new RuntimeException("Machine board for " + PieceImplementation.playerColorString[nextTurn] + " not updated correctly");
                    }

                }
                // System.out.println("gc2:\n" + board);
                // switch player
                setNextTurn(Helper.getOpponentColor(getNextTurn()));
                // Update the list of validMoves before notifying
                // This is done first so that humanPlayer will always be able
                // to access the movelist. getting valid moves might take too long,
                // so it has to be done first before notifying
                // System.out.println("gc getvalidmoves 1");
                validMoves = board.getAllValidMoves(nextTurn);
                if (playerType[nextTurn] == PlayerType.HUMAN) {
                    humanPlayer.setMoveList(validMoves);
                } else {
                    humanPlayer.setMoveList(null);
                }
                // System.out.println("gc3:\n" + board);
                // System.out.println("Notifying observers");
                if (checkGameOver(validMoves)) {
                    return;
                }

                // Start timer for next player
                if (timedGame)
                    timer[nextTurn].startTimer();
                //notify observers that there has been a move by the last player
                notifyObservers(new Message(move));
            }
        }
    }

    /**
     * Get the string representation of the current game, as specified in the game file format
     *
     * @return string representation of game
     **/
    public String getBoardString() {
        StringBuilder str = new StringBuilder();
        // LINE 0: Cylindrical Board
        if (board.getBoardType() == BoardType.CYLINDER) {
            str.append("Cylinder Board\n");
        }
        // LINE 1: Next player
        String nextPlayerString = (nextTurn == Sides.WHITE ? "white" : "black") + "\n";
        str.append(nextPlayerString);
        // LINE 2: Time left
        String timeLeftString = timer[Sides.WHITE].getTimeLeft() + "\t";
        timeLeftString += timer[Sides.BLACK].getTimeLeft() + "\n";
        str.append(timeLeftString);
        // LINE 3: Switch Info
        String switchString = (board.getSpecial(Sides.WHITE) ? "1" : "0") + "\t";
        switchString += (board.getSpecial(Sides.BLACK) ? "1" : "0") + "";
        str.append(switchString);
        // LINE 4 ONWARDS: Piece Info
        String pieceStr;
        //Loop through all pieces
        String colLetters = "abcdefgh";
        List<String> pieceStrList = new ArrayList<>();
        for (int color = PieceImplementation.FIRSTCOLOR; color <= PieceImplementation.LASTCOLOR; color++) {
            PieceImplementation[][] pieces = board.getPiecesOnBoard(color);
            for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
                int pieceCount = board.getPieceCount(color, i);
                for (int j = 0; j < pieceCount; j++) {
                    PieceImplementation piece = pieces[i][j];
                    pieceStr = "";
                    if (piece == null) continue;
                    // Line break
                    pieceStr += "\n";
                    // Get char
                    pieceStr += piece.getChar() + "\t";
                    // Get cur position
                    pieceStr += colLetters.charAt(piece.getCol()) + "" + (piece.getRow() + 1) + "\t";
                    // Get last position
                    pieceStr += colLetters.charAt(piece.getLastCol()) + "" + (piece.getLastRow() + 1) + "\t";
                    // Check if moved
                    pieceStr += (piece.getMoved() ? "1" : "0") + "\t";
                    // Check Last Piece Move
                    //pieceStr += (piece.equals (board.getLastPieceMoved(piece.getColor())) ? "1" : "0");
                    pieceStr += (board.isLastPieceMoved(piece) ? "1" : "0");
                    pieceStrList.add(pieceStr);
                }
            }
        }
        // Sort the strings
        Collections.sort(pieceStrList);
        for (String o : pieceStrList) {
            pieceStr = o;
            str.append(pieceStr);
        }
        str.append(";");// terminate with ";" and NO END LINE
        return str.toString();

    }

    /**
     * Fills the GameFileManager with information from a game string
     *
     * @param gameStr the string containing the game information
     * @requires gameStr is in the gamefile format
     * @modifies this
     * @effects sets GameFileManager with information extracted
     **/
    public void readFromString(String gameStr) { // TODO is this for two or four players?
        StringTokenizer st = new StringTokenizer(gameStr, "\n");
        String str;
        boolean outputToScreen = false;
        int[][] topology = {{1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1},
                {1, 1, 1, 1, 1, 1, 1, 1}};
        // Overwrites board with a new board
        board = new Board(topology);
        // LINE 0 (optional)
        // Cylinder board
        str = (String) st.nextElement();
        if (str.equals("Cylinder Board")) {
            board.setBoardType(BoardType.CYLINDER);
            str = (String) st.nextElement();
        }
        // LINE 1
        // Get next player
        //System.out.println (str + ".");
        if (str.equals("white")) {
            nextTurn = Sides.WHITE;
            if (outputToScreen) System.out.println("white");
        } else if (str.equals("black")) {
            nextTurn = Sides.BLACK;
            if (outputToScreen) System.out.println("black");
        } else
            throw new RuntimeException("Error getting next player color");
        // LINE 2
        // Get Time Left for each player
        try {
            str = (String) st.nextElement();
            StringTokenizer timeSt = new StringTokenizer(str, "\t");
            String whiteTimeStr = (String) timeSt.nextElement();
            if (!timeSt.hasMoreElements())
                throw new RuntimeException("Error in input file format for time. missing tab");
            String blackTimeStr = (String) timeSt.nextElement();
            timer[Sides.WHITE] = new Timer(Integer.parseInt(whiteTimeStr));
            timer[Sides.BLACK] = new Timer(Integer.parseInt(blackTimeStr));
            if (outputToScreen) System.out.println("white time: " +
                    timer[Sides.WHITE].getTimeLeft());
            if (outputToScreen) System.out.println("black time: " +
                    timer[Sides.BLACK].getTimeLeft());
        } catch (Exception e) {
            throw new RuntimeException("Error getting timeleft\n" + e);
        }
        // LINE 3
        // Get switch info
        str = (String) st.nextElement();
        if (str.charAt(0) == '1') {
            board.setSpecial(Sides.WHITE, true);
            if (outputToScreen) System.out.println("white used special: " +
                    board.getSpecial(Sides.WHITE));
        }
        if (str.charAt(2) == '1') {
            board.setSpecial(Sides.BLACK, true);
            if (outputToScreen) System.out.println("black used special: " +
                    board.getSpecial(Sides.BLACK));
        }
        // LINE 4 UNTIL LINE ENDING with ";"
        // Get piece info
        int row, col;
        while (st.hasMoreElements()) {
            str = (String) st.nextElement();
            // Get current pos
            // current col a-h
            col = str.charAt(2) - 'a';// + 1;
            // current row '1'-'8'
            row = str.charAt(3) - '1'; //- '0';
            // Get Piece Type
            char pUp = str.toUpperCase().charAt(0);
            char p = str.charAt(0);
            PieceImplementation piece;
            int color;
            // Get Color
            if (p == pUp) // White are upper case
                color = Sides.WHITE;
            else
                color = Sides.BLACK;
            if (outputToScreen)
                System.out.println("creating..");
            if (pUp == 'P') {
                piece = new Pawn(p, board, col, row, color);
            } else if (pUp == 'K') {
                piece = new King(p, board, col, row, color);
            } else if (pUp == 'N') {
                piece = new Knight(p, board, col, row, color);
            } else if (pUp == 'R') {
                piece = new Rook(p, board, col, row, color);
            } else if (pUp == 'Q') {
                piece = new Queen(p, board, col, row, color);
            } else if (pUp == 'B') {
                piece = new Bishop(p, board, col, row, color);
            } else
                throw new RuntimeException("Unrecognized piece type:" + pUp);
            // Get last pos
            // last col a-h
            int lastCol = str.charAt(5) - 'a';// + 1;
            // last row '1'-'8'
            int lastRow = str.charAt(6) - '1';//'0';
            piece.setLastLocation(lastCol, lastRow);
            // Get has-moved
            char moved = str.charAt(8);
            if (moved == '1')
                piece.setMoved(true);
            // Get last-piece-moved
            moved = str.charAt(10);
            if (moved == '1') {
                //SET LAST PIECE MOVE IN BOARD
                if (board.getLastPieceMoved(piece.getColor()) == null) {
                    board.setLastPieceMoved(piece);
                } else {
                    board.setLastPieceMoved(piece, 1);
                }
            }
            if (outputToScreen) {
                System.out.println(piece);
                System.out.println("islastpiece: " + moved);
            }
            // End of piece info
            if (str.charAt(str.length() - 1) == ';') {
                break;
            }
        }
        // fill up board with empty pieces
        board.addEmpty();
        // init Zobrist
        board.initZobrist();
    }

}