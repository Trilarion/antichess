/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.controller;

import org.antichess.model.Move;
import org.jetbrains.annotations.NotNull;

/**
 * <p>GameMessage keeps the information about the changes in the flow
 * of an antichess game.
 *
 * <p>When a GameController is running a game, it notifies all its observers
 * with a GameMessage. GameMessages are immutable.
 **/
public class Message { // TODO instead of MessageType, just inherit vom Message (tag interface), so we don't need MessageType

    private final MessageType type;
    private final Move move;
    private final String messageBody;

    /**
     * Constructs a message of a GameMessage.MOVE type
     * Holds information about the actual move and the color making move
     */
    public Message(@NotNull Move move) {
        type = MessageType.MOVE;
        this.move = move;
        messageBody = null;
    }

    /**
     * Constructs a message of a GameMessage.MESSAGE type
     * Holds information about message text
     */
    public Message(@NotNull String messageBody) {
        type = MessageType.MESSAGE;
        move = null;
        this.messageBody = messageBody;
    }

    /**
     * Returns type
     *
     * @return type
     */
    public MessageType getType() {
        return type;
    }

    /**
     * Returns move
     *
     * @return move
     */
    public Move getMove() {
        return move;
    }

    /**
     * Returns message
     *
     * @return message
     */
    public String getMessage() {
        return messageBody;
    }

}
