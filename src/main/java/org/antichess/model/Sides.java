/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.model;

/**
 * The possible sides/colors in a game.
 */
public final class Sides { // TODO make it an enum when the time comes
    /**
     * Constant for the white color
     */
    public static final int WHITE = 0;
    /**
     * Constant for the color black
     */
    public static final int BLACK = 1;
    /**
     * Constant for the color black
     */
    public static final int RED = 2;
    /**
     * Constant for the color black
     */
    public static final int BLUE = 3;

    private Sides() {
    }
}
