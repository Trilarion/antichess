/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model;

import org.antichess.model.pieces.PieceImplementation;

import java.util.StringTokenizer;

/**
 * <p>A move consists of a start and destination location, and information about
 * whether the move is a capture move, switch, en passant, a castling move, a
 * pawn moving 2 squares, or a promotion. Move is mutable.
 * A move can be undone using Piece.undoMove. The move must passed into undoMove
 * must be the same move pass into makeMove, because makeMove will store additional
 * information necessary for undo in the move after applying the move.
 * <p>
 *
 * startCol : int
 * startRow : int
 * destCol : int
 * destRow : int
 * capture: boolean // whether move is a capture
 * castle : boolean // whether move is a castling
 * special: boolean // whether move is a special
 * ep     : boolean // whether move is an en passant
 * pawn   : boolean // whether move is a pawn moving 2 squares
 * promote: boolean // whether move is a promotion
 * normal: boolean // whether a move is none of the above
 **/
/*
 * DEPRECATED
 * startLocation: Location // starting location of a move
 * destLocation: Location // destination location of a move
 */
public class Move { // TODO a move should be immutable
// TODO Move should be an interface and then there should be inherited stuff like SimpleMove, ...
    private static final int maxPiecesModified = 4;
    private static final int captureMASK = 1;
    private static final int castleMASK = 1 << 1;
    private static final int specialMASK = 1 << 2;
    private static final int epMASK = 1 << 3;
    private static final int pawnMASK = 1 << 4;
    private static final int promoteMASK = 1 << 5;

    // NONSTATIC FIELDS
    // FIELDS FOR UNDO MOVE
    // Reference to removed pieces
    public final PieceImplementation[] modifiedPieces = new PieceImplementation[maxPiecesModified];
    // Reference to last piece moved on board
    public final PieceImplementation[][] lastPieces = new PieceImplementation[PieceImplementation.LASTCOLOR + 1][Board.maxLastPieceMoved];
    // Piece info
    private final boolean[] moved = new boolean[maxPiecesModified]; // has moved
    private final int[] row = new int[maxPiecesModified]; // current row
    private final int[] col = new int[maxPiecesModified]; // current col
    private final int[] lastRow = new int[maxPiecesModified]; // TODO use 2D Position (row, col) instead
    private final int[] lastCol = new int[maxPiecesModified];

    // BASIC MOVE INFO
    /* info about this move */
    private int info;
    private String string;
    /* starting row */
    private int startRow; // TODO have a generic 2D location instead of row, column
    /* starting col */
    private int startCol;
    /* dest row */
    private int destRow;
    /* dest col */
    private int destCol;

    /**
     * Creates a new move. This will make copies of the locations.
     *
     * @requires string representation xn-xn, {@code "xn" | x = {a|b|c|d|e|f|g|h} && n = {1|2|3|4|5|6|7|8|9}}
     * @modifies this
     * @effects creates a new move
     **/
    public Move(String str) {

        if (str == null) {
            setInvalid();
            return;
        }

        string = str;
        // check if board is 4player
        //    if (Piece.LASTCOLOR!=Piece.BLACK){
        if (str.length() > 5) {
            // 4player move
            StringTokenizer st = new StringTokenizer(str, "-");
            if (!st.hasMoreTokens()) {
                setInvalid();
                return;
            }

            // start
            String s = st.nextToken();

            if (s.length() != 2 && s.length() != 3) {
                System.out.println("invalid move: string length error");
                setInvalid();
                return;
            }

            startRow = Helper.getRowFromString(s);
            startCol = Helper.getColFromString(s);

            if (!st.hasMoreTokens()) {
                System.out.println("invalid move: no more tokens");
                setInvalid();
                return;
            }

            // destination
            s = st.nextToken();
            if (s.length() != 2 && s.length() != 3) {
                System.out.println("invalid move: string length error");
                setInvalid();
                return;
            }

            destRow = Helper.getRowFromString(s);
            destCol = Helper.getColFromString(s);

        } else {

            if (str.length() != 5) {
                setInvalid();
                return;
            }
            startRow = Helper.getRowFromString(str.substring(0, 2));
            startCol = Helper.getColFromString(str.substring(0, 2));
            destRow = Helper.getRowFromString(str.substring(3, 5));
            destCol = Helper.getColFromString(str.substring(3, 5));
        }
    }

    /**
     * Creates a new move from a string representation.
     *
     * @modifies this
     * @effects creates a new move
     **/
    public Move(int startCol, int startRow, int destCol, int destRow) {
        this.startRow = startRow;
        this.startCol = startCol;
        this.destRow = destRow;
        this.destCol = destCol;
        string = Helper.locationToString(startCol, startRow) + "-" + Helper.locationToString(destCol, destRow);
    }

    /**
     * Creates a new move from a given move
     **/
    public Move(Move move) {
        startRow = move.startRow;
        startCol = move.startCol;
        destRow = move.destRow;
        destCol = move.destCol;
        string = move.string;
        info = move.info;
    }

    public static int getMaxPiecesModified() {
        return maxPiecesModified;
    }

    public static int getCaptureMASK() {
        return captureMASK;
    }

    public static int getCastleMASK() {
        return castleMASK;
    }

    public static int getSpecialMASK() {
        return specialMASK;
    }

    public static int getEpMASK() {
        return epMASK;
    }

    public static int getPawnMASK() {
        return pawnMASK;
    }

    public static int getPromoteMASK() {
        return promoteMASK;
    }

    /* Makes coordinates of this move invalid */
    private void setInvalid() { // TODO is this really needed? Better to throw an exception in the constructor
        startRow = -1;
        startCol = -1;
        destRow = -1;
        destCol = -1;
    }

    /**
     * Stores piece information of p at location i
     *
     * @modifies this
     **/
    public void storePieceInfo(PieceImplementation piece, int i) {
        row[i] = (piece.getRow());
        col[i] = (piece.getCol());
        lastRow[i] = (piece.getLastRow());
        lastCol[i] = (piece.getLastCol());
        moved[i] = (piece.getMoved());
    }

    public boolean isValid() {
        return startRow >= 0 && startCol >= 0 && destRow >= 0 && destCol >= 0;
    }

    /**
     * Restore piece information of p at location i
     *
     * @modifies this
     **/
    public void restorePieceInfo(PieceImplementation piece, int i) {
        piece.setLocation(col[i], row[i]);
        piece.setLastLocation(lastCol[i], lastRow[i]);
        piece.setMoved(moved[i]);

    }

    /**
     * @return normal
     **/
    public boolean isNormal() {
        return (info == 0);
    }

    /**
     * @return capture
     **/
    public boolean isCapture() {
        return (info & captureMASK) > 0;
    }

    /**
     * @modifies this
     * @effects set capture to x
     **/
    public void setCapture(boolean x) {
        if (x)
            info |= captureMASK;
        else {
            int info1 = info & (~captureMASK);
            info = info1;
        }

    }

    /**
     * @return castle
     **/
    public boolean getCastle() {
        return (info & castleMASK) > 0;
    }

    /**
     * @modifies this
     * @effects set castle to x
     **/
    public void setCastle(boolean x) {
        if (x)
            info |= castleMASK;
        else {
            int info1 = info & (~castleMASK);
            info = info1;
        }
    }

    /**
     * @return special
     **/
    public boolean isSpecial() {
        return (info & specialMASK) > 0;
    }

    /**
     * @modifies this
     * @effects set special to x
     **/
    public void setSpecial(boolean x) {
        if (x)
            info |= specialMASK;
        else {
            int info1 = info & (~specialMASK);
            info = info1;
        }
    }

    /**
     * @return ep
     **/
    public boolean getEp() {
        return (info & epMASK) > 0;
    }

    /**
     * @modifies this
     * @effects set ep to x
     **/
    public void setEp(boolean x) {
        if (x)
            info |= epMASK;
        else {
            int info1 = info & (~epMASK);
            info = info1;
        }
    }

    /**
     * @return pawn
     **/
    public boolean getPawn() {
        return (info & pawnMASK) > 0;
    }

    /**
     * @modifies this
     * @effects set pawn to x
     **/
    public void setPawn(boolean x) {
        if (x)
            info |= pawnMASK;
        else {
            int info1 = info & (~pawnMASK);
            info = info1;
        }
    }

    /**
     * @return promote
     **/
    public boolean getPromote() {
        return (info & promoteMASK) > 0;
    }

    /**
     * @modifies this
     * @effects set promote to x
     **/
    public void setPromote(boolean x) {
        if (x)
            info |= promoteMASK;
        else {
            int info1 = info & (~promoteMASK);
            info = info1;
        }
    }

    public int getStartRow() {
        return startRow;
    }

    public int getStartCol() {
        return startCol;
    }

    public int getDestRow() {
        return destRow;
    }

    public int getDestCol() {
        return destCol;
    }

    /**
     * @return true if m is equal to this
     * Checks only start/dest row/col.
     **/
    public boolean equals(Move move) {
        //return (m.getStart().equals(start)) && (m.getDest().equals(dest));
        return (startRow == move.startRow &&
                startCol == move.startCol &&
                destRow == move.destRow &&
                destCol == move.destCol &&
                string.equals(move.toString()));
    }

    public boolean equals(Object o) {
        if (o instanceof Move)
            return equals((Move) o);
        else return false;
    }

    /**
     * @return string representation xn-xn, {@code "xn" | x = {a|b|c|d|e|f|g|h} && n = {1|2|3|4|5|6|7|8|9}}
     **/
    public String toString() {
        return string;
    }

    public int getInfo() {
        return info;
    }

    public String getString() {
        return string;
    }

    public void setInfo(int info) {
        this.info = info;
    }
}
