/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.model;

import org.antichess.model.pieces.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * <p>
 * Board represents an antichess board. Board is mutable.
 *
 * <p>
 * The topology of the board could vary and is supplied as an argument
 */
public class Board { // TODO does it also work for 4 players?

    public static final int maxLastPieceMoved = 2; // maximum of last pieces moved
    /**
     * Constant value for the value of pieces
     **/
    public static final int[] PIECEVALUE = {15, 50, 30, 30, 90};
    private static int NUMCOLS;
    private static int MINROW;
    private static int MAXROW;
    private static int MINCOL;
    private static int MAXCOL;


    private final PieceImplementation[][] gameBoard;   //board the game is played on
    private final PieceImplementation[][][] pieceArray; //pieces on the board for each side

    //Piece[Pawn] would be the pawn in the board
    //if there exists one
    private final PieceImplementation[][] lastPieceMoved; // lastPieceMoved[WHITE | BLACK][]
    private final int[][] pieceCount;  //number of maximum available pieces during anytime in the game
    private final int[][] aliveCount;  //number of pieces on the board
    private final boolean[] checked;
    private final boolean[] special;
    private final int[][] topology;
    private BoardType boardType;
    private Zobrist zobrist;

    /**
     * Creates an MAXCOLxMAXROW antichess board.
     * The topology of the board is supplied as an argument of two
     * dimensional integer array.if topology[i][j]!=0 then,
     * that square is unusable. For topology[i][j]==1, the
     * square is a usable one.
     * <p>
     * The constructor initialized checked and special to false.
     * pieceCount and aliveCount is initialized to 0 for all pieces
     * for both sides.
     *
     * @throws IndexOutOfBoundsException {@code if !(MINCOL=0 && MINROW=0 && MAXROW>=0 && MAXCOL>=0)}
     * @requires topology be an immutable integer matrix of 0's and nonzero
     * integers {@code && MINCOL=0 && MINROW=0 && MAXROW>=0 && MAXCOL>=0}
     * @requires topology has to be zero-based
     * @effects creates an org.antichess Board
     */
    public Board(int[][] topology) {

        //    * topology is a  integer
        //    * matrix of 0's and nonzero integers if topology[i][j]!=0 then,
        //    * it means that a square exists at (i,j) if topology==0 then, it means
        //    * there does not exist a square there.
        //    * The constructor initializes checked and special fields to false.
        //    * pieceCount and alivepieceCount is initialized to 0 for all pieces
        //    * for both sides.

        this.topology = topology;

        //create the board
        MINROW = 0;
        MINCOL = 0;
        MAXCOL = topology.length - 1; // 7
        MAXROW = topology[0].length - 1; // 7
        NUMCOLS = getMAXCOL() - getMINCOL() + 1; // 8
        gameBoard = new PieceImplementation[getMAXCOL() + 1][getMAXROW() + 1]; // 0 to 7

        //initialize
        checked = new boolean[PieceImplementation.LASTCOLOR + 1];
        checked[Sides.WHITE] = checked[Sides.BLACK] = false;

        special = new boolean[PieceImplementation.LASTCOLOR + 1];
        special[Sides.WHITE] = special[Sides.BLACK] = false;

        pieceCount = new int[PieceImplementation.LASTCOLOR + 1][PieceImplementation.LASTPIECE + 1];
        aliveCount = new int[PieceImplementation.LASTCOLOR + 1][PieceImplementation.LASTPIECE + 1];
        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            pieceCount[Sides.WHITE][i] = pieceCount[Sides.BLACK][i] = 0;
            aliveCount[Sides.WHITE][i] = aliveCount[Sides.BLACK][i] = 0;
        }

        pieceArray = new PieceImplementation[PieceImplementation.LASTCOLOR + 1][PieceImplementation.LASTPIECE + 1][];
        //lastPieceMoved=new Piece[Piece.LASTCOLOR+1];

        // This assumes that the number of last pieces moved for each color is less
        // than Piece.LASTPIECE
        lastPieceMoved = new PieceImplementation[PieceImplementation.LASTCOLOR + 1][maxLastPieceMoved];
        for (int i = PieceImplementation.FIRSTCOLOR; i <= PieceImplementation.LASTCOLOR; i++) {
            lastPieceMoved[i] = new PieceImplementation[maxLastPieceMoved];
        }
    }

    /**
     * Creates a copy of a given board, deep copying every piece
     *
     * @requires b.topology be an immutable two dimensional array
     */
    public Board(Board board) {

        topology = board.topology;
        int i, j;
        //create the board
        MINROW = 0;
        MINCOL = 0;
        MAXCOL = topology.length - 1; // 7
        MAXROW = topology[0].length - 1; // 7
        NUMCOLS = getMAXCOL() - getMINCOL() + 1; // 8
        gameBoard = new PieceImplementation[getMAXCOL() + 1][getMAXROW() + 1]; // 0 to 7
        boardType = board.boardType;

        //initialize
        checked = new boolean[PieceImplementation.LASTCOLOR + 1];
        checked[Sides.WHITE] = board.checked[Sides.WHITE];
        checked[Sides.BLACK] = board.checked[Sides.BLACK];

        special = new boolean[PieceImplementation.LASTCOLOR + 1];
        special[Sides.WHITE] = board.special[Sides.WHITE];
        special[Sides.BLACK] = board.special[Sides.BLACK];

        pieceCount = new int[PieceImplementation.LASTCOLOR + 1][PieceImplementation.LASTPIECE + 1];
        aliveCount = new int[PieceImplementation.LASTCOLOR + 1][PieceImplementation.LASTPIECE + 1];
        for (i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            pieceCount[Sides.WHITE][i] = pieceCount[Sides.BLACK][i] = 0;
            aliveCount[Sides.WHITE][i] = aliveCount[Sides.BLACK][i] = 0;
        }

        pieceArray = new PieceImplementation[PieceImplementation.LASTCOLOR + 1][PieceImplementation.LASTPIECE + 1][];
        //lastPieceMoved=new Piece[Piece.LASTCOLOR+1];

        // This assumes that the number of last pieces moved for each color is less
        // than Piece.LASTPIECE
        lastPieceMoved = new PieceImplementation[PieceImplementation.LASTCOLOR + 1][maxLastPieceMoved];
        for (i = PieceImplementation.FIRSTCOLOR; i <= PieceImplementation.LASTCOLOR; i++) {
            lastPieceMoved[i] = new PieceImplementation[maxLastPieceMoved];
        }

        //Deep copy all the pieces from given board and add to this board
        for (i = 0; i < board.gameBoard.length; i++)
            for (j = 0; j < board.gameBoard[i].length; j++)
                if (board.gameBoard[i][j] != null) {
                    //	  System.out.println ("At " + i + " " + j);
                    Piece piece = board.gameBoard[i][j];
                    piece.copyPiece(this);
                }

        for (i = 0; i < board.lastPieceMoved.length; i++)
            if (board.lastPieceMoved[i] != null) {
                for (j = 0; j < board.lastPieceMoved[i].length; j++) {
                    if (board.lastPieceMoved[i][j] == null) continue;

                    lastPieceMoved[i][j] = gameBoard[board.lastPieceMoved[i][j].getCol()][board.lastPieceMoved[i][j].getRow()];

                }
            }

        // We can do this here because we are completely done
        // creating a new board from the given board
        initZobrist();
    }

    /**
     * Constant which holds the number of columns on the board = MAXCOL-MINCOL+1
     */
    public static int getNUMCOLS() {
        return NUMCOLS;
    }  // TODO this could be a property of the topology

    /**
     * Constant which holds the smallest possible row
     */
    public static int getMINROW() {
        return MINROW;
    }

    /**
     * Constant which holds the biggest possible row
     */
    public static int getMAXROW() {
        return MAXROW;
    }

    /**
     * Constant which holds the smallest possible row
     */
    public static int getMINCOL() {
        return MINCOL;
    }

    /**
     * Constant which holds the biggest possible row
     */
    public static int getMAXCOL() {
        return MAXCOL;
    }

    /**
     * Returns if a particular side has lost.
     * Note that this method will recompute all the valid moves in order
     * to determine whether the game is a loss or not.
     * To prevent re-computation, use getValidMoves, store the returned list
     * and pass it into isWin(Board board, int nextTurn, List validMoves)
     *
     * @requires {@code board != null && king is on board}
     * @return true/false
     */
    public static boolean isLoseNormalChess(Board board, int nextTurn) {
        // check if king is checkmated
        return board.getValidMoves(nextTurn).isEmpty() &&
                board.kingInCheck(nextTurn);
    }

    /**
     * Returns if a game is draw given a board,
     * with nextTurn being the next player to move
     * Note that this method will recompute all the valid moves in order
     * to determine whether the game is a tie or not.
     * To prevent re-computation, use getValidMoves, store the returned list
     * and pass it into isTie(Board board int nextTurn, List validMoves)
     *
     * @requires {@code board!= null && isGameOver == true}
     * @return true/false
     */
    public static boolean isTieNormalChess(Board board, int nextTurn) {
        // Check that next turn is not a lose, and that has no more moves
        return (!isLoseNormalChess(board, nextTurn) &&
                board.getValidMovesNormalChess(nextTurn).isEmpty());
    }

    /**
     * Returns if a game is over given a board,
     * with nextTurn being the next player to move
     * Note that this method will recompute all the valid moves in order
     * to determine whether the game is over or not.
     * To prevent re-computation, use getValidMoves, store the returned list
     * and pass it into isGameOver(Board board int nextTurn, List validMoves)
     *
     * @requires board!= null, nextturn = {WHITE | BLACK}
     * @return true/false
     */
    public static boolean isGameOverNormalChess(Board board, int nextTurn) {
        boolean tie = isTieNormalChess(board, nextTurn);
        boolean lose = isLoseNormalChess(board, nextTurn);
        return (isTieNormalChess(board, nextTurn) || isLoseNormalChess(board, nextTurn));
    }

    /**
     * Finds all Normal chess legal moves given a board and a color to move.
     *
     * @requires b!=null color = {WHITE | BLACK}
     * @return a List of legal moves
     **/
    public List<Move> getValidMovesNormalChess(int color) {
        List<Move> captureMoves = new ArrayList<>();
        List<Move> nonCaptureMoves = new ArrayList<>();

        // Iterate through all pieces
        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            int pieceCount = getPieceCount(color, i);
            for (int j = 0; j < pieceCount; j++) {
                // piece at (i, j)
                PieceImplementation[][] pieces = getPiecesOnBoard(color);
                PieceImplementation piece = pieces[i][j];

                // Check if it is a piece
                if (piece == null)
                    continue;

                piece.getPossibleMoves(captureMoves, nonCaptureMoves);
            }
        }

        // remove check moves in capturemoves
        removeCheckMoves(captureMoves);

        // remove check moves in switch and non capture moves
        nonCaptureMoves.addAll(getPossibleSwitchMoves(color));
        removeCheckMoves(nonCaptureMoves);

        // combine capturemoves and non capturemoves
        captureMoves.addAll(nonCaptureMoves);
        return captureMoves;

    }

    /**
     * Undoes a move.
     *
     * @requires move has been applied to the board in the last turn using
     * and main piece to undo is at destCol destRow
     * makeMove/makeGeneratedMove
     **/
    public void undoMove(Move move) {
        int destRow = move.getDestRow();
        int destCol = move.getDestCol();
        PieceImplementation piece = getPieceAt(destCol, destRow);

        if (move.isNormal())
            piece.undoNormalMove(move);
        else if (move.isSpecial())
            piece.undoSwitchMove(move);
        else if (move.isCapture())
            piece.undoCaptureMove(move);
        else
            piece.undoOtherMove(move);
    }

    /**
     * Makes a computer generated move.
     * This move must be generated from getValidMoves or one of the
     * methods in Piece, and move has information about what kind of move it is.
     * The move from a human player should be passed into makeMove.
     **/
    public void makeGeneratedMove(Move move) {
        int startRow = move.getStartRow();
        int startCol = move.getStartCol();
        PieceImplementation piece = getPieceAt(startCol, startRow);
        piece.makeGeneratedMove(move);
    }

    /**
     * Makes a given move on a board, modifying it, recomputing the validMoves.
     * Note that this will recompute all the valid moves first.
     * Use getValidMoves, store the returned list, and then use
     * makeMove (Board board Move move List validMoves) to avoid re-computation
     *
     * @modifies b
     **/
    public void makeMove(Move move) { // TODO make checked move, make unchecked move (call)

        if (move == null || !move.isValid())
            throw new RuntimeException();
        int startRow = move.getStartRow();
        int startCol = move.getStartCol();
        // Getting piece to make move
        PieceImplementation piece = getPieceAt(startCol, startRow);
        if (piece == null)
            throw new RuntimeException();
        if (piece.getType() == PieceType.EMPTY)
            throw new RuntimeException();
        // get valid moves
        List<Move> validMoves = getAllValidMoves(piece.getColor());
        // We need to get the piece again because undo move might have changed it
        piece = getPieceAt(startCol, startRow);
        piece.makeMove(move, validMoves);
    }

    /**
     * Removes illegal and duplicate moves from a List
     *
     * @requires non null
     * @modifies moves
     * @effects removes illegal moves and duplicate moves
     **/
    private void removeCheckMoves(List<Move> moves) { // TODO what are illegal moves

        int startRow; // TODO can this be static
        int startCol;
        int color;

        PieceImplementation currentPiece, kingPiece;
        Iterator<Move> iter = moves.iterator();
        Move move;

        while (iter.hasNext()) {
            // Check for duplicates
            move = iter.next();

            // Create a copy of the board
            //tmpBoard = new Board (b);

            startRow = move.getStartRow();
            startCol = move.getStartCol();

            currentPiece = getPieceAt(startCol, startRow);
            color = currentPiece.getColor();
            makeGeneratedMove(move);

            // Get king and see if it is in check
            kingPiece = getKing(color);

            // If there is no king
            if (kingPiece == null) {
                undoMove(move);
                continue;
            }

            boolean result = (((King) kingPiece).inCheck());

            undoMove(move);
            if (result) {
                // invalid move
                // King is in check
                iter.remove();
            } else {
                // King is not in check
            }
        }
    }

    /**
     * Returns a list of possible switch (not necessarily legal) moves a piece can make
     * The Moves are not necessarily legal.
     *
     * @return list of moves representing the switches the piece can make
     * @requires b!= null && color == {WHITE | BLACK}
     **/
    private List<Move> getPossibleSwitchMoves(int color) { // TODO why not necessarily legal here?
        // First check if special move has been made.
        // Go through all locations on the board.
        // For each location, see if it contains a piece of the same color.
        // If so, add the move.

        // if 4-players, no special move
        if (getBoardType() == BoardType.FOURPLAYERS) {
            return new ArrayList<>();
        }

        int startRow;
        int startCol;
        int destRow;
        int destCol;

        List<Move> specialMoves = new ArrayList<>();
        Move move;

        PieceImplementation piece1, piece2;

        PieceImplementation[][] pieces = getPiecesOnBoard(color);

        // check if special move already has been used
        // if so, just return an empty arraylist
        if (getSpecial(color))
            return specialMoves;

        // Iterate through all pieces

        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            int pieceCount = getPieceCount(color, i);
            if (pieces[i] == null) continue;
            for (int j = 0; j < pieceCount; j++) {

                if (pieces[i][j] == null) continue;
                if (!pieces[i][j].nonEmptyPiece(color))
                    continue;
                piece1 = pieces[i][j];

                //for (int ii = Piece.FIRSTPIECE; ii <= Piece.LASTPIECE; ii++){
                for (int ii = i; ii <= PieceImplementation.LASTPIECE; ii++) {
                    int pieceCount2 = getPieceCount(color, ii);
                    if (pieces[ii] == null) continue;
                    //for (int jj = 0; jj < pieceCount2; jj++){
                    int jjstart;
                    if (i == ii)
                        jjstart = j + 1;
                    else
                        jjstart = 0;
                    for (int jj = jjstart; jj < pieceCount2; jj++) {

                        // System.out.println (ii + " " + jj);
                        if (pieces[ii][jj] == null) continue;

                        if (!pieces[ii][jj].nonEmptyPiece(color))
                            continue;
                        piece2 = pieces[ii][jj];

                        startRow = piece1.getRow();
                        startCol = piece1.getCol();
                        destRow = piece2.getRow();
                        destCol = piece2.getCol();

                        // different location
                        if (destRow != startRow || destCol != startCol) {

                            // If the destination square contains a pawn,
                            // check that the starting square is not the first or
                            // last rank

                            if (piece2.getType() == PieceType.PAWN) {
                                if ((startRow == getMINROW()) ||
                                        (startRow == getMAXROW())) {
                                    continue; // skip adding this move
                                }
                            }

                            // If the starting square contains a pawn,
                            // check that the destination square is not the first or
                            // last rank
                            if (piece1.getType() == PieceType.PAWN) {
                                if ((destRow == getMINROW()) ||
                                        (destRow == getMAXROW())) {
                                    continue; // skip adding this move
                                }
                            }

                            // Create and add the move

                            // switch move should be alphanumeric
                            if (destCol > startCol ||
                                    (destCol == startCol && destRow > startRow)) {
                                move = new Move(startCol, startRow, destCol, destRow);
                            } else {
                                move = new Move(destCol, destRow, startCol, startRow);
                            }
                            move.setSpecial(true);
                            specialMoves.add(move);
                        }
                    }

                }
            }
        }

        return specialMoves;
    }

    /**
     * Returns a list of valid switch moves.
     *
     * @return list of moves representing the switches the piece can make
     * {@code @requires b!= null && color == {WHITE | BLACK}}
     **/
    public List<Move> getSwitchMoves(int color) {
        List<Move> validSwitchMoves = getPossibleSwitchMoves(color);
        removeCheckMoves(validSwitchMoves);
        return validSwitchMoves;
    }

    /**
     * Checks if a move is legal given a board and the color to move.
     * If the move is a switch move, it will be modified to be in alphanumeric order.
     * Modifies move
     *
     * @return true | false
     **/
    public boolean isValidMove(Move move, int color) {
        // Check if the move string is a valid one
        if (move.toString().length() != 5)
            return false;

        int destRow = move.getDestRow();
        int destCol = move.getDestCol();
        int startRow = move.getStartRow();
        int startCol = move.getStartCol();

        if (destRow > getMAXROW() ||
                destRow < getMINROW() ||
                destCol > getMAXCOL() ||
                destCol < getMINCOL())
            return false;

        if (startRow > getMAXROW() ||
                startRow < getMINROW() ||
                startCol > getMAXCOL() ||
                startCol < getMINCOL())
            return false;

        // piece at (startCol, startRow)
        PieceImplementation piece = getPieceAt(move.getStartCol(), move.getStartRow());
        if (piece == null)
            return false;
        if (piece.getType() == PieceType.EMPTY)
            return false;
        if (piece.getColor() != color)
            return false;

        List<Move> moves = getValidMoves(color);

        List<Move> switchMoves = new ArrayList<>();
        List<Move> reversedSwitchMoves = new ArrayList<>();
        Helper.extractSwitchMoves(moves, switchMoves, reversedSwitchMoves);
        if (switchMoves.contains(move))
            return true;
        if (reversedSwitchMoves.contains(move)) {
            // reverse move
            int index = reversedSwitchMoves.indexOf(move);
            Move move1 = switchMoves.get(index);
            move = new Move(move1);
        }
        return moves.contains(move);
    }

    /**
     * Finds all legal moves given a board and a color to move,
     * EXCLUDING switch moves.
     *
     * @requires b!=null color = {WHITE | BLACK}
     * @return a List of legal moves
     **/
    public List<Move> getStandardValidMoves(int color) {
        List<Move> captureMoves = new ArrayList<>();
        List<Move> nonCaptureMoves = new ArrayList<>();

        // Iterate through all pieces
        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            int pieceCount = getPieceCount(color, i);
            for (int j = 0; j < pieceCount; j++) {
                // piece at (i, j)
                PieceImplementation[][] pieces = getPiecesOnBoard(color);
                PieceImplementation piece = pieces[i][j];

                // Check if it is a piece
                if (piece == null)
                    continue;

                piece.getPossibleMoves(captureMoves, nonCaptureMoves);
            }
        }

        removeCheckMoves(captureMoves);
        if (!captureMoves.isEmpty())
            return captureMoves;

        // WE DO NOT ADD ANY SWITCH MOVES HERE // TODO why not?
        //nonCaptureMoves.addAll (getPossibleSwitchMoves(b, color));

        removeCheckMoves(nonCaptureMoves);
        return nonCaptureMoves;

    }

    /**
     * Finds all legal moves, including switch moves in both directions
     * eg: both a1-a2 and a2-a1 will be included.
     *
     * @requires b!=null
     * @return a List of legal moves
     **/
    public List<Move> getAllValidMoves(int color) {
        List<Move> captureMoves = new ArrayList<>();
        List<Move> nonCaptureMoves = new ArrayList<>();

        // Iterate through all pieces
        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            int pieceCount = getPieceCount(color, i);
            for (int j = 0; j < pieceCount; j++) {
                // piece at (i, j)
                PieceImplementation[][] pieces = getPiecesOnBoard(color);
                PieceImplementation piece = pieces[i][j];

                // Check if it is a piece
                if (piece == null)
                    continue;

                piece.getPossibleMoves(captureMoves, nonCaptureMoves);
            }
        }

        removeCheckMoves(captureMoves);
        if (!captureMoves.isEmpty())
            return captureMoves;

        nonCaptureMoves.addAll(getPossibleSwitchMoves(color));

        // add in the reversed switch moves
        List<Move> switchMoves = new ArrayList<>();
        List<Move> reversedSwitchMoves = new ArrayList<>();
        Helper.extractSwitchMoves(nonCaptureMoves, switchMoves, reversedSwitchMoves);
        nonCaptureMoves.addAll(reversedSwitchMoves);

        // remove any moves that result in a check
        removeCheckMoves(nonCaptureMoves);
        return nonCaptureMoves;

    } // TODO public static order set fix

    /**
     * Finds all legal moves given a board and a color to move.
     *
     * @requires b!=null color = {WHITE | BLACK}
     * @return a List of legal moves
     **/
    public List<Move> getValidMoves(int color) {
        List<Move> captureMoves = new ArrayList<>();
        List<Move> nonCaptureMoves = new ArrayList<>();

        // Iterate through all pieces
        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            int pieceCount = getPieceCount(color, i);
            for (int j = 0; j < pieceCount; j++) {
                // getting piece at (i, j)
                PieceImplementation[][] pieces = getPiecesOnBoard(color);
                PieceImplementation piece = pieces[i][j];

                // Check if it is a piece
                if (piece == null)
                    continue;

                piece.getPossibleMoves(captureMoves, nonCaptureMoves);
            }
        }

        removeCheckMoves(captureMoves);
        if (!captureMoves.isEmpty())
            return captureMoves;

        // Removing check moves from non capture moves
        nonCaptureMoves.addAll(getPossibleSwitchMoves(color));
        removeCheckMoves(nonCaptureMoves);
        return nonCaptureMoves;
    }

    public boolean kingInCheck(int color) {
        // Check if king is in check
        PieceImplementation king = getKing(color);
        if (king == null) // TODO can this ever happen? (@notnull on getKing)
            return false;
        return ((King) king).inCheck();
    }

    /**
     * Returns if a particular side has won, given a board,
     * the nextTurn being the next player to move
     * and the list of valid moves.
     *
     * @requires {@code board!= null&&  nextturn = {WHITE | BLACK} && validMoves} is a list of valid moves returned by getValidMoves
     * @return true/false
     */
    public boolean isWin(int nextTurn, List<Move> validMoves) {

        // Check if all pieces lost except king
        if (onlyKingLeft(nextTurn))
            return true;

        // check if king is checkmated
        return validMoves.isEmpty() &&
                kingInCheck(nextTurn);

    }

    /**
     * Returns if a particular side has won.
     * Note that this method will recompute all the valid moves in order
     * to determine whether the game is a win or not.
     * To prevent re-computation, use getValidMoves, store the returned list
     * and pass it into isWin(Board board int nextTurn, List validMoves)
     *
     * @requires {@code board != null && king is on board}
     * @return true/false
     */
    public boolean isWin(int nextTurn) {
        // Check if all pieces lost except king
        if (onlyKingLeft(nextTurn))
            return true;

        // check if king is checkmated
        return getValidMoves(nextTurn).isEmpty() &&
                kingInCheck(nextTurn);

    }

    /**
     * Returns if a game is draw given a board,
     * the nextTurn being the next player to move
     * and the list of valid moves.
     *
     * @requires {@code board!= null&&  nextturn = {WHITE | BLACK} && validMoves} is a list of valid moves returned by getValidMoves
     * @return true/false
     */
    public boolean isTie(int nextTurn, List<Move> validMoves) {
        // Check that nextturn is not in win, and that
        // has no more moves
        return (!isWin(nextTurn, validMoves) &&
                validMoves.isEmpty());
    }

    /**
     * Returns if a game is draw given a board,
     * with nextTurn being the next player to move
     * Note that this method will recompute all the valid moves in order
     * to determine whether the game is a tie or not.
     * To prevent re-computation, use getValidMoves, store the returned list
     * and pass it into isTie(Board board int nextTurn, List validMoves)
     *
     * @requires {@code board!= null && isGameOver == true}
     * @return true/false
     */
    public boolean isTie(int nextTurn) {
        // Check that next turn is not in win, and that
        // has no more moves
        return (!isWin(nextTurn) &&
                getValidMoves(nextTurn).isEmpty());
    }

    /**
     * Returns if a game is over given a board,
     * the nextTurn being the next player to move
     * and the list of valid moves.
     *
     * @requires {@code board!= null&&  nextturn = {WHITE | BLACK} && validMoves} is a list of valid moves returned by getValidMoves
     * @return true/false
     */
    public boolean isGameOver(int nextTurn, List<Move> validMoves) {
        boolean tie = isTie(nextTurn, validMoves);
        boolean win = isWin(nextTurn, validMoves);
        return (tie || win);
    }

    /**
     * Returns if a game is over given a board,
     * with nextTurn being the next player to move
     * Note that this method will recompute all the valid moves in order
     * to determine whether the game is over or not.
     * To prevent re-computation, use getValidMoves, store the returned list
     * and pass it into isGameOver(Board board int nextTurn, List validMoves)
     *
     * @requires board!= null, nextturn = {WHITE | BLACK}
     * @return true/false
     */
    public boolean isGameOver(int nextTurn) {
        return (isTie(nextTurn) || isWin(nextTurn));
    }

    /**
     * Returns if the king is the only piece left for a side
     *
     * @requires board!=null, nextturn = {WHITE | BLACK}
     **/
    public boolean onlyKingLeft(int nextTurn) {
        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            if (i == PieceType.KING.getValue())
                continue;
            if (getAliveCount(nextTurn, i) > 0)
                return false;
        }
        return true;
    }

    /**
     * Adds empty pieces to the rest of the board
     *
     * @throws RuntimeException if board==null
     * @effects adds new Empty pieces to the rest of the board.
     * @requires topology has to be zero-based
     * board!=null
     */
    public void addEmpty() {

        for (int i = getMINCOL(); i <= getMAXCOL(); i++)
            for (int j = getMINROW(); j <= getMAXROW(); j++)
                if (getUsable(i, j) && getPieceAt(i, j) == null)
                    new Empty(this, i, j); // TODO empty could just be null actually
    }

    /**
     * Returns a hashCode for this board
     * Currently uses Zobrist.getKey()
     *
     * @requires zobrist!=null
     */
    public long zobristCode(int nextTurn) {
        return zobrist.getKey(this, nextTurn);
    }
    // TODO inline this ^ and below too
    /**
     * Returns a hashCode for this board
     * Currently uses Zobrist.getKey()
     *
     * @requires zobrist!=null
     */
    public long zobristCode(String boardStr, int nextTurn) {
        return zobrist.getKey(boardStr, nextTurn);
    }

    /**
     * Gets the board type
     *
     * @return {REGULAR | CYLINDER}
     **/
    public BoardType getBoardType() {
        return boardType;
    }

    /**
     * Sets the board type
     *
     * @requires type = {REGULAR | CYLINDER}
     **/
    public void setBoardType(BoardType type) {
        boardType = type;
    } // TODO do this in constructor only

    /**
     * Gets the number of maximum available pieces on the board
     * during any time in the game for a particular side.
     *
     * @return a two dimensional array of initial number
     * of pieces on the board for a particular side.
     * Entries of this array could be null.
     **/
    public int getPieceCount(int side, int piece) {
        return pieceCount[side][piece];
    }

    /**
     * Gets the weighted difference of number of alive pieces
     * on the board during any time in the game for a particular
     * side.
     *
     * @return the weighted difference of pieces on the board
     * for a particular side.
     **/
    public int getDifference(int side) {
        int result = 0;
        for (int i = PieceImplementation.FIRSTPIECE; i < PieceImplementation.LASTPIECE; i++) //last piece is king, we don't need it
            result += aliveCount[side][i] * PIECEVALUE[i];

        int otherSide = Helper.getOpponentColor(side);
        for (int i = PieceImplementation.FIRSTPIECE; i < PieceImplementation.LASTPIECE; i++) //last piece is king, we don't need it
            result -= aliveCount[otherSide][i] * PIECEVALUE[i];
        return -result;
    }

    /**
     * Gets the weighted count of alive pieces on the board
     * during any time in the game for a particular side.
     *
     * @return the number of a particular piece on the board
     * for a particular side.
     **/
    public int getAliveCount(int side, int piece) {
        return aliveCount[side][piece];

    }

    /**
     * Gets the weighted count of alive pieces on the board
     * during any time in the game for a particular side.
     *
     * @return the number of a particular piece on the board
     * for a particular side.
     **/
    public int getTotalAliveCount(int side) {
        int result = 0;
        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++)
            result += aliveCount[side][i];
        return (result);
    }

    /**
     * Checks if a side is in check
     *
     * @requires side == {Piece.WHITE | Piece.BLACK}
     * @return true if in check, false otherwise
     **/
    public boolean getChecked(int side) {
        return checked[side];
    }

    /**
     * Gets the king Piece of a particular side
     *
     * @requires {@code color = {Piece.WHITE | Piece.Black} &&}
     * there is only and only one KingPiece from the beginning to the end of the game
     * @return a KingPiece
     **/
    public PieceImplementation getKing(int color) { // TODO should return a King
        //     if (aliveCount[color][Piece.KING]==0) return null;
        //     for (int i=0; i<pieceCount[color][Piece.KING]; i++)
        //       if (pieceArray[color][Piece.KING][i]!=null)
        // 	return pieceArray[color][Piece.KING][i];

        // We can have test cases which don't have a king,
        // so we have to check if the array is null first
        if (pieceArray[color][PieceType.KING.getValue()] != null)
            return pieceArray[color][PieceType.KING.getValue()][0];
        else
            return null;

    }

    /**
     * Checks if a side has used special move
     *
     * @requires side == {Piece.WHITE | Piece.BLACK}
     * @return true if used special, else false
     **/
    public boolean getSpecial(int side) {
        return special[side];
    }

    /**
     * Gets a piece at a particular row and column.
     *
     * @requires {@code 1<= location.row < = numberOfRows & & 1 < = location.col < = numberOfCols}
     * @return a piece at the specified location
     **/
    public PieceImplementation getPieceAt(int col, int row) {
        return gameBoard[col][row];
    }

    /**
     * Gets an array of Piece that are present on the board.
     * This array contains only Pieces that are on the board.
     *
     * @return A Piece[] containing the pieces currently on the board
     **/
    public PieceImplementation[][] getPiecesOnBoard(int side) {
        return pieceArray[side];
    }

    /**
     * Gets the last piece move to a piece
     *
     * @return lastPieceMoved
     **/
    public PieceImplementation getLastPieceMoved(int color) {
        return lastPieceMoved[color][0];
    }

    /**
     * Gets the last piece move to a piece at an index
     *
     * @return lastPieceMoved
     **/
    public PieceImplementation getLastPieceMoved(int color, int index) {
        return lastPieceMoved[color][index];
    }

    /**
     * Checks if a piece is the last piece moved
     *
     * @requires Piece != null
     **/
    public boolean isLastPieceMoved(PieceImplementation piece) {
        int color = piece.getColor();
        if (lastPieceMoved[color] == null)
            return false;

//     System.out.println ("isLastPieceMoved: \n" + p);
        for (int i = 0; i < maxLastPieceMoved; i++) {
            if (lastPieceMoved[color][i] != null) {
// 	System.out.println ("lastpiecemoved:  " + i + ": \n" + lastPieceMoved[color][i]);
                if (piece.equals(lastPieceMoved[color][i]))
                    return true;
            }
        }
        return false;
    }

    /**
     * Gets whether given location is usable
     *
     * @return true if given location is usable
     **/
    public boolean getUsable(int col, int row) {
        if (getMINROW() <= row && row <= getMAXROW() && getMINCOL() <= col && col <= getMAXCOL())
            return topology[col][row] != 0;
        return false;
    }

    /**
     * Returns a string representation of the board
     *
     * @return a string representing the board
     * unusable squares is a '*'
     **/
    public String toString() {
        StringBuilder value = new StringBuilder();
        for (int i = getMAXROW(); getMINROW() <= i; i--) {
            for (int j = getMINCOL(); j <= getMAXCOL(); j++)
                if (topology[j][i] != 0)            //check that there is no null square
                {
                    if (gameBoard[j][i] != null)
                        value.append((gameBoard[j][i]).getChar());
                    else value.append(" ");
                } else value.append("*");
            value.append("\n");
        }
        return value.toString();
    }

    /**
     * Returns the zobrist
     **/
    public Zobrist getZobrist() {
        return zobrist;
    }

    /**
     * Sets a side to be in check or not to be in check
     *
     * @requires side == {Piece.WHITE | Piece.BLACK}
     * @modifies this
     * @effects sets a side to be in check/not in check
     **/
    public void setChecked(int side, boolean inCheck) {
        checked[side] = inCheck;
    } // TODO only used in tests

    /**
     * Sets whether a side has used special move
     *
     * @requires side == {Piece.WHITE | Piece.BLACK}
     * @modifies this
     * @effects sets the side to have used / have not used special move
     **/
    public void setSpecial(int side, boolean usedSpecial) {
        special[side] = usedSpecial;
    }

    /**
     * Modify a Piece at a location
     *
     * @requires {@code 1<= row < = numberOfRows & & 1 < = col < = numberOfCols}
     * @effects Sets the Piece at ( row, col ) to point to p
     **/
    public void setPieceAt(int col, int row, PieceImplementation piece) {
        gameBoard[col][row] = piece;
    }

    /**
     * Sets the last piece moved
     *
     * @effects Sets lastPieceMoved[color][0] to be p, lastPieceMoved[color][1] to be null
     **/
    public void setLastPieceMoved(PieceImplementation piece) {
        if (piece == null)
            return;
        lastPieceMoved[piece.getColor()][0] = piece;
        lastPieceMoved[piece.getColor()][1] = null;
    }

    /**
     * Sets the last pieces moved
     *
     * @effects Sets lastPieceMoved to be p1 and p2
     **/
    public void setLastPieceMoved(PieceImplementation piece1, PieceImplementation piece2, int color) {
        //lastPieceMoved[p.getColor()]=p;
        lastPieceMoved[color][0] = piece1;
        lastPieceMoved[color][1] = piece2;
    }

    /**
     * Sets the last piece move to a piece
     *
     * @requires piece is on board
     * @effects Sets lastPieceMoved[color][0] to be p, lastPieceMoved[color][1] to be null
     **/
    public void setLastPieceMoved(PieceImplementation piece, int color) {
        lastPieceMoved[color][0] = piece;
        lastPieceMoved[color][1] = null;
    }

    /**
     * Sets the last piece move to a piece
     *
     * @requires piece is on board
     * @effects Sets lastPieceMoved[color][index] to be p
     **/
    public void setLastPieceMoved(PieceImplementation piece, int color, int index) {
        lastPieceMoved[color][index] = piece;
    }

    /**
     * Checks for equality. <br>
     * Overrides Object.equals
     *
     * @return true if given object is same as this
     **/
    public boolean equals(Object o) {
        if (o instanceof Board) {
            return equals((Board) o);
        } else
            return false;
    }

    /**
     * Checks for equality. <br>//HALF IMPLEMENTED//
     * Overrides Object.equals
     *
     * @return true if given board is same
     **/
    public boolean equals(Board other) {
        return (other.getChecked(Sides.WHITE) == checked[Sides.WHITE] &&
                other.getChecked(Sides.BLACK) == checked[Sides.BLACK] &&
                other.getLastPieceMoved(Sides.WHITE).equals(lastPieceMoved[Sides.WHITE]) &&
                other.getLastPieceMoved(Sides.BLACK).equals(lastPieceMoved[Sides.BLACK]));
    }

    /**
     * Registers a piece to the board at (col,row)
     *
     * @throws RuntimeException {@code if !(MINCOL<=col<=MAXCOL && MINROW<=row<=MAXROW && Board(col,row) is a usable square)}
     * @requires piece instance of {@code Piece && MINCOL<=col<=MAXCOL && MINROW<=row<=MAXROW && Board(col,row)} is a usable square
     */
    public void registerPiece(int col, int row, PieceImplementation piece) {
        if (topology[col][row] == 0)
            throw new RuntimeException("Board.registerPiece: trying to write on unusable square!");

        //System.out.println ("register");

        //delete the old piece if there was a piece at that location

        if (gameBoard[col][row] != null) {
            PieceImplementation oldpiece = gameBoard[col][row];
            PieceType oldtype = oldpiece.getType();
            int oldcolor = oldpiece.getColor();

            //System.out.println ("deleting old type");

            if (oldtype != PieceType.EMPTY) {
                //System.out.println ("capture");

                //decrease the number of alive pieces (but not the number of maximum pieces)
                aliveCount[oldcolor][oldtype.getValue()]--;

                //delete it from piecearray
                for (int i = 0; i < pieceCount[oldcolor][oldtype.getValue()]; i++)

                    if (pieceArray[oldcolor][oldtype.getValue()][i] != null)
                        if (pieceArray[oldcolor][oldtype.getValue()][i].equals(oldpiece)) {
                            pieceArray[oldcolor][oldtype.getValue()][i] = null;
                            //System.out.println("deleting in: "+i+" piece:"+oldpiece);
                        }
            }

        }
        //delete it from the board, put the new piece on
        gameBoard[col][row] = piece;

        PieceType newtype = piece.getType();
        int newcolor = piece.getColor();

        if (newtype != PieceType.EMPTY) {

            //increase the number of pieces
            aliveCount[newcolor][newtype.getValue()]++;
            int oldpiececount = pieceCount[newcolor][newtype.getValue()];

            //we are adding a piece to the pieceArray into a null place
            if (aliveCount[newcolor][newtype.getValue()] <= oldpiececount) {
                //go through the piecearray and add to the
                //first null place
                for (int i = 0; i < oldpiececount; i++)
                    if (pieceArray[newcolor][newtype.getValue()][i] == null) { //System.out.println("Adding in:"+i+" piece:"+ piece);
                        pieceArray[newcolor][newtype.getValue()][i] = piece;
                        break;
                    }
            }

            //we are appending the piece to the pieceArray
            if (aliveCount[newcolor][newtype.getValue()] > oldpiececount) {

                //create a new array of size+1
                PieceImplementation[] newArray = new PieceImplementation[oldpiececount + 1];
                //put old guys in a new array

                if (pieceArray[newcolor][newtype.getValue()] != null)
                    System.arraycopy(pieceArray[newcolor][newtype.getValue()], 0, newArray, 0, oldpiececount);

                //for (int i=0;i<oldpiececount;i++)
                //  newArray[i]=pieceArray[newcolor][newtype][i];

                //append the piece to the new array
                newArray[oldpiececount] = piece;
                // make the new pieceArray
                pieceArray[newcolor][newtype.getValue()] = newArray;
                // increase the pieceCount accordingly
                pieceCount[newcolor][newtype.getValue()]++;
                // make sure there are no differences between aliveCount and pieceCount
                aliveCount[newcolor][newtype.getValue()] = pieceCount[newcolor][newtype.getValue()];
            }
        }
    }

    /**
     * Initializes the zobrist matrix for this board
     * CALL THIS ONLY AFTER YOU PUT ALL THE PIECES INTO THE BOARD
     *
     * @requires board to be full
     */
    public void initZobrist() {
        zobrist = new Zobrist(this);
    } // TODO does the board need a zobrist instance, what about static methods?

    /**
     * Retrieves and stores the last pieces information from a board
     *
     * @param move move
     */
    public void setLastPieces(Move move) {
        for (int i = PieceImplementation.FIRSTCOLOR; i <= PieceImplementation.LASTCOLOR; i++) {
            for (int j = 0; j < maxLastPieceMoved; j++)
                move.lastPieces[i][j] = getLastPieceMoved(i, j);
        }

    }

    /**
     * Restores the last pieces information to a board
     *
     * @param move move
     */
    public void restoreLastPieces(Move move) {
        for (int i = PieceImplementation.FIRSTCOLOR; i <= PieceImplementation.LASTCOLOR; i++) {
            for (int j = 0; j < maxLastPieceMoved; j++)
                setLastPieceMoved(move.lastPieces[i][j], i, j);

        }
    }
}
