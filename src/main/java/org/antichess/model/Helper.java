/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.model;

import org.antichess.model.pieces.PieceImplementation;

import java.util.Collection;
import java.util.List;

public final class Helper {

    private Helper() {
    }

    static String locationToString(int col, int row) {
        String colStr = String.valueOf(col + 1);
        char colChr = (colStr.length() == 1 ? colStr.charAt(0) : colStr.charAt(1));
        colChr -= '1';
        colChr += 'a';
        if (colStr.length() == 2)
            colChr += 10;
        return colChr + String.valueOf(row + 1);
    }

    /* Gets row number from a string location
     * "xn" | x = {a|b|c|d|e|f|g|h} && n = {1|2|3|4|5|6|7|8|9}
     * @requires str is in notation of alphabet for column and number for row
     **/
    static int getRowFromString(String str) {
        if (str.length() == 2)
            return str.charAt(1) - '1';//'0';
        else {
            String st = str.substring(1);
            return Integer.parseInt(st) - 1;
        }
    }

    /* Gets col number from a string location
     * "xn" | x = {a|b|c|d|e|f|g|h} && n = {1|2|3|4|5|6|7|8|9}
     * @requires str is in notation of alphabet for column and number for row
     **/
    static int getColFromString(String str) {
        return str.charAt(0) - 'a';// + 1;
    }

    /**
     * Gets the opponent color,
     * checking LASTCOLOR to see if it is a 2player game or 4.
     *
     * @return {Piece.WHITE | Piece.BLACK} | RED/BLUE if 4player
     * @requires color == {Piece.WHITE | Piece.BLACK} | RED/BLUE if 4player
     **/
    public static int getOpponentColor(int color) {
        final int COLORMASK = 1;
        if (PieceImplementation.LASTCOLOR == Sides.BLACK) // 2-player
            return COLORMASK - color;
        else {
            if (color == Sides.WHITE) {
                return Sides.RED;
            } else if (color == Sides.RED) {
                return Sides.BLACK;
            } else if (color == Sides.BLACK) {
                return Sides.BLUE;
            } else if (color == Sides.BLUE) {
                return Sides.WHITE;
            }
            throw new RuntimeException("Invalid color in getOpponentColor: " + color);

        }
        //return Piece.COLORMASK-color;
    }

    /**
     * Extracts the switch moves and make a reversed copy of them
     **/
    public static void extractSwitchMoves(List<Move> moves, Collection<Move> switchMoves, Collection<Move> reversedSwitchMoves) {
        int i;
        for (i = 0; i < moves.size(); i++) {
            Move move = moves.get(i);
            if (move.isSpecial()) {
                int destRow = move.getDestRow();
                int destCol = move.getDestCol();
                int startRow = move.getStartRow();
                int startCol = move.getStartCol();

                // add original move
                switchMoves.add(move);

                Move newmove = new Move(destCol, destRow, startCol, startRow);
                newmove.setSpecial(true);
                reversedSwitchMoves.add(newmove);
            }
        }

    }
}
