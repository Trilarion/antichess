/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model;

import org.antichess.model.pieces.PieceImplementation;
import org.antichess.model.pieces.PieceType;

import java.util.Random;

/**
 * Zobrist class handles the Zobrist algorithm for creating a zobrist key for a given board.
 **/
public class Zobrist {

    private final long[][][] zobristMatrix;
    private final Board board;
    private final long[] turn;
    private long key;

    /**
     * Creates a Zobrist matrix for a specific board. Initializes key=0.
     *
     * @requires board is full with pieces
     */
    public Zobrist(Board board) {

        //create the randomGenerator
        Random randomGenerator = new Random();

        //get how many squares we have in the board
        int squareCount = (Board.getMAXROW() + 1 - Board.getMINROW()) * (Board.getMAXCOL() + 1 - Board.getMINCOL());

        //create the zobrist matrix and fill it up
        zobristMatrix = new long[PieceImplementation.LASTPIECE + 1 - PieceImplementation.FIRSTPIECE][PieceImplementation.LASTCOLOR + 1 - PieceImplementation.FIRSTCOLOR][squareCount];

        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++)
            for (int j = PieceImplementation.FIRSTCOLOR; j <= PieceImplementation.LASTCOLOR; j++)
                for (int k = 0; k < squareCount; k++)
                    zobristMatrix[i][j][k] = randomGenerator.nextLong();

        //initialize black constant
        turn = new long[4];
        turn[0] = 0;
        turn[1] = randomGenerator.nextLong();
        turn[2] = randomGenerator.nextLong();
        turn[3] = randomGenerator.nextLong();

        //initialize the board link
        this.board = board;

        //initialize the key
        PieceImplementation piece;
        int place;
        key = 0;
        for (int i = Board.getMINCOL(); i <= Board.getMAXCOL(); i++)
            for (int j = Board.getMINROW(); j <= Board.getMAXROW(); j++)
                if ((piece = board.getPieceAt(i, j)) != null)
                    if (piece.getType() != PieceType.EMPTY) {
                        place = j * (Board.getMAXROW() + 1 - Board.getMINROW()) + (i - Board.getMINCOL());
                        key ^= zobristMatrix[piece.getType().getValue() - PieceImplementation.FIRSTPIECE][piece.getColor() - PieceImplementation.FIRSTCOLOR][place];
                    }
    }

    /**
     * Returns a zobrist key for the board
     *
     * @throws RuntimeException if this is a different board
     * @requires board is the board that initially zobristMatrix is created for
     * nextTurn is the next player to move
     * @return a zobrist key for that board
     */
    public long getKey(Board board, int nextTurn) {
        if (board != this.board)
            throw new RuntimeException("Zobrist.getKey():this is a different board");
        return (key ^ turn[nextTurn]);

    }

    /**
     * Returns a zobrist key for the board
     *
     * @throws RuntimeException if this is a different board
     * @requires board is the board that initially zobristMatrix is created for
     * nextTurn is the next player to move
     * @return a zobrist key for that board
     */
    public long getKey(String boardStr, int nextTurn) {
        long newKey = 0;
        int size = boardStr.length();
        char p;
        char pUp;
        int color;
        PieceType pieceType = PieceType.EMPTY;

        for (int i = 0; i < size; i++) {
            // Get Piece Type
            pUp = boardStr.toUpperCase().charAt(i);
            p = boardStr.charAt(i);

            // Get Color
            if (p == pUp) // White are upper case
                color = Sides.WHITE;
            else
                color = Sides.BLACK;

            if (pUp == 'P') {
                pieceType = PieceType.PAWN;
            } else if (pUp == 'K') {
                pieceType = PieceType.KING;
            } else if (pUp == 'N') {
                pieceType = PieceType.KNIGHT;
            } else if (pUp == 'R') {
                pieceType = PieceType.ROOK;
            } else if (pUp == 'Q') {
                pieceType = PieceType.QUEEN;
            } else if (pUp == 'B') {
                pieceType = PieceType.BISHOP;
            } else if (pUp != '-')
                throw new RuntimeException("Unrecognized piece type:" + pUp);

            if (pUp != '-')
                newKey ^= zobristMatrix[pieceType.getValue()][color][i];
        }

        return (newKey ^ turn[nextTurn]);

    }

    /**
     * MUST BE CALLED WHEN BOARD IS CHANGED THROUGH A MOVE OR UNDOMOVE
     * Updates the key for the board update
     *
     * @throws RuntimeException if this is a different board
     */
    public void deletePiece(Board board, PieceType type, int color, int col, int row) {
        if (board != this.board)
            throw new RuntimeException("Zobrist.getKey():this is a different board");
        key ^= zobristMatrix[type.getValue() - PieceImplementation.FIRSTPIECE][color - PieceImplementation.FIRSTCOLOR][row * (Board.getMAXROW() + 1 - Board.getMINROW()) + (col - Board.getMINCOL())];
    }

    /**
     * MUST BE CALLED WHEN BOARD IS CHANGED THROUGH A MOVE OR UNDOMOVE
     * Updates the key for the board update
     *
     * @throws RuntimeException if this is a different board
     */
    public void addPiece(Board board, PieceType type, int color, int col, int row) {
        if (board != this.board)
            throw new RuntimeException("Zobrist.getKey():this is a different board");
        key ^= zobristMatrix[type.getValue() - PieceImplementation.FIRSTPIECE][color - PieceImplementation.FIRSTCOLOR][row * (Board.getMAXROW() + 1 - Board.getMINROW()) + (col - Board.getMINCOL())];
    }

}
	    
