/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model.pieces;

import org.antichess.model.Board;
import org.antichess.model.Move;

/**
 * This class contains the rules that apply to Knight.
 **/
public class Knight extends PieceImplementation {
    /**
     * Creates a new piece.
     *
     * @param letter the character representation of the piece:
     * @param board  the board the piece will be assigned to
     * @param row    starting row of piece
     * @param col    starting col of piece
     * @param color  color
     * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
     **/
    public Knight(char letter, Board board, int col, int row, int color) {
        super(PieceType.KNIGHT, letter, board, col, row, color);
        int[] myDCol = {-2, -2, -1, -1, 1, 1, 2, 2};
        int[] myDRow = {-1, 1, -2, 2, -2, 2, -1, 1};
        setDcol(myDCol);
        setDrow(myDRow);
        setSlide(false);
        setOffsets(8);
    }

    /**
     * Creates a copy to a new board
     */
    public PieceImplementation copyPiece(Board board) {
        PieceImplementation piece = new Knight(getChar(), board, getCol(), getRow(), getColor());
        piece.setLastLocation(getLastCol(), getLastRow());
        piece.setMoved(getMoved());
        return piece;
    }

    @Override
    public void makeOtherMove(Move move) {
        throw new RuntimeException();
    }
}
