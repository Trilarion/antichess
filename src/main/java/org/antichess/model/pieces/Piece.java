/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.model.pieces;

import org.antichess.model.Board;
import org.antichess.model.Move;

/**
 *
 */
public interface Piece { // TODO extract more methods from PieceImplementation

    Piece copyPiece(Board board);

    /**
     * Makes a move that is unique to a particular piece.
     * To be overridden by subclasses
     **/
    void makeOtherMove(Move move);
}
