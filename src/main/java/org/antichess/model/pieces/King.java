/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model.pieces;

import org.antichess.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * This class contains the rules that apply to King.
 * It implements castling as a special move.
 **/
public class King extends PieceImplementation {
    private final int[] cDCol = {-1, -1, -1, 0, 0, 1, 1, 1, // 8 standard directions
            -2, -2, -1, -1, 1, 1, 2, 2}; //knight
    private final int[] cDRow = {-1, 0, 1, -1, 1, -1, 0, 1, // 8 standard directions
            -1, 1, -2, 2, -2, 2, -1, 1}; //knight
    // number of opponent type that can attack from this direction
    // first index is color of king: white =0, black =1
    // second index is number of opponent type
    private final int[][] cPieceCount = {
            // WHITE KING
            {3, 3, 4, 3, 3, 3, 3, 4,
                    1, 1, 1, 1, 1, 1, 1, 1},
            // BLACK KING
            {4, 3, 3, 3, 3, 4, 3, 3,
                    1, 1, 1, 1, 1, 1, 1, 1}};
    // the piece types that can attack from a particular direction
    // first index is direction index
    // second index is piece type
    private final PieceType[][] cPieceType = {
            // (col, row)
            // -1,-1
            {PieceType.QUEEN, PieceType.KING, PieceType.BISHOP, PieceType.PAWN},
            // -1, 0
            {PieceType.QUEEN, PieceType.KING, PieceType.ROOK},
            // -1, 1
            {PieceType.QUEEN, PieceType.KING, PieceType.BISHOP, PieceType.PAWN},
            // 0, -1
            {PieceType.QUEEN, PieceType.KING, PieceType.ROOK},
            // 0, 1
            {PieceType.QUEEN, PieceType.KING, PieceType.ROOK},
            // 1, -1
            {PieceType.QUEEN, PieceType.KING, PieceType.BISHOP, PieceType.PAWN},
            // 1, 0
            {PieceType.QUEEN, PieceType.KING, PieceType.ROOK},
            // 1, 1
            {PieceType.QUEEN, PieceType.KING, PieceType.BISHOP, PieceType.PAWN},
            // KNIGHT
            {PieceType.KNIGHT}, {PieceType.KNIGHT}, {PieceType.KNIGHT}, {PieceType.KNIGHT},
            {PieceType.KNIGHT}, {PieceType.KNIGHT}, {PieceType.KNIGHT}, {PieceType.KNIGHT}};
    // the piece slide info
    // first index is direction index
    // second index is piece slide
    private final int[][] cSlide = {
            // (col, row)
            // -1,-1
            {1, 0, 1, 0},
            // -1, 0
            {1, 0, 1},
            // -1, 1
            {1, 0, 1, 0},
            // 0, -1
            {1, 0, 1},
            // 0, 1
            {1, 0, 1},
            // 1, -1
            {1, 0, 1, 0},
            // 1, 0
            {1, 0, 1},
            // 1, 1
            {1, 0, 1, 0},
            // KNIGHT
            {0}, {0}, {0}, {0},
            {0}, {0}, {0}, {0}};

    /**
     * Creates a new piece.
     *
     * @param letter the character representation of the piece:
     * @param board  the board the piece will be assigned to
     * @param row    starting row of piece
     * @param col    starting col of piece
     * @param color  color
     * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
     **/
    public King(char letter, Board board, int col, int row, int color) {
        super(PieceType.KING, letter, board, col, row, color);
        int[] myDCol = {-1, -1, -1, 0, 0, 1, 1, 1};
        int[] myDRow = {-1, 0, 1, -1, 1, -1, 0, 1};
        setDcol(myDCol);
        setDrow(myDRow);
        setSlide(false);
        setOffsets(8);
    }

    /**
     * Creates a copy to a new board
     */
    public PieceImplementation copyPiece(Board board) {
        PieceImplementation piece = new King(getChar(), board, getCol(), getRow(), getColor());
        piece.setLastLocation(getLastCol(), getLastRow());
        piece.setMoved(getMoved());
        return piece;
    }

    /**
     * Overrides getOtherMoves in Piece and retrieves castling moves.
     **/
    public List<Move> getOtherMoves() {
        // check for 4-player, if 4-player, no castling
        if (getBoard().getBoardType() == BoardType.FOURPLAYERS)
            return new ArrayList<>();
        else
            return getCastlingMoves();
    }

    /**
     * Returns a list of castling moves the King can make <br>
     * Overrides getSpecialMoves in Piece.
     *
     * @return list of moves representing the switches the King can make
     * @requires 2-player game
     **/
    private List<Move> getCastlingMoves() {
        // This checks for castling, and combines the possible castling moves
        // with the switching moves.

        // We need to check the following:
        // (1) that the King and rook have never moved.
        // (2) that the King is not under attack. You may not castle out of check.
        // (3) that the King is not passing through or arriving upon a square
        // controlled by the opponent.
        // (4) that all of the squares between the King and Rook are vacant.
        Board board = getBoard();
        int startRow = getRow();
        int startCol = getCol();
        int color = getColor();

        int k;
        int rookRow, rookCol;

        List<Move> validMoves = new ArrayList<>();
        int[] rookRows; // Rooks' starting position
        int[] rookCols = {Board.getMINCOL(), 0, Board.getMAXCOL()};

        int opponentColor = Helper.getOpponentColor(getColor());

        // Check that king has not moved. If it has, no need to check.
        if (getMoved()) return validMoves;

        // Check that king is not in check
        if (inCheck())
            return validMoves;
        // else
        //       System.out.println ("King is not in check");

        // Check color and determine Rooks' starting position
        // loc will contain the Rooks' starting position. myRow[1] is always null
        // and is a dummy variable.
        // myRow[0] contains left side Rook. (0 == dcol + 1 when dcol == -1)
        // myRow[2] contains right side Rook. (2 == dcol + 1 when dcol == 1)
        int[] myRow;
        if (color == Sides.WHITE) {
            // WHITE
            myRow = new int[]{Board.getMINROW(), 0, Board.getMINROW()};
        } else {
            myRow = new int[]{Board.getMAXROW(), 0, Board.getMAXROW()};
        }
        rookRows = myRow;

        // dcol = -1: check left side
        // dcol = 1: check right side
        int curCol;
        for (int dcol = -1; dcol < 2; dcol += 2) {

            // Get the Rook location of a side
            rookRow = rookRows[dcol + 1];
            rookCol = rookCols[dcol + 1];
            int destCol = startCol + dcol * 2;

            // See if a piece is there

            PieceImplementation rookPiece = board.getPieceAt(rookCol, rookRow);
            if (rookPiece == null)
                continue;

            // Check if it is a rook
            if (rookPiece.getType() != PieceType.ROOK)
                continue;

            // Check if it is the same color
            if (rookPiece.getColor() != color)
                continue;

            // Check if the rook has moved
            if (rookPiece.getMoved()) continue;

            // Check that the locations in between are empty and
            // that location the king passes or lands on are not attacked by opponent

            // Assume castling is possible first
            boolean canCastle = true;

            // Offset the initial column by the direction column vector
            curCol = startCol + dcol;

            // k keeps track of whether the king will go through this location
            k = 0; // k < 2 if the king will pass/land on this location

            // Loop until we reach the destination square

            while (curCol != rookCol) {
                if (curCol > Board.getMAXCOL() ||
                        curCol < Board.getMINCOL())
                    break;

                // Get the location to check
                // Check if square is valid

                // Check if location is empty
                PieceImplementation piece = board.getPieceAt(curCol, startRow);
                if (piece == null) {
                    //System.out.println ("castling: null square at " + curCol + " " + curRow);
                    canCastle = false;
                    break;
                }
                if (piece.getType() != PieceType.EMPTY) {
                    //System.out.println ("castling: nonempty square at " + curCol + " " + curRow);
                    canCastle = false;
                    break;
                }

                // Check if a location that the king pass through or
                // land on is attacked by opponent
                if (k < 2) {

                    // Iterate through all opponent pieces on the board
                    PieceImplementation[][] pieces = board.getPiecesOnBoard(opponentColor);

                    // Iterate through all pieces
                    for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
                        int pieceCount = board.getPieceCount(opponentColor, i);
                        if (pieces[i] == null)
                            continue;
                        for (int j = 0; j < pieceCount; j++) {

                            PieceImplementation opponentPiece = pieces[i][j];

                            // Check if it is a piece
                            if (opponentPiece == null)
                                continue;

                            // This location is attacked by opponent. So
                            // we cannot castle, and need not check further
                            // in this direction.
                            if (opponentPiece.canAttackLocation(curCol, startRow)) {
                                canCastle = false;
                                break;
                            }
                        }
                    }

                    // Since k=0,1, this portion is processed 2 times.
                    // destCol will contain the 2nd square away from
                    // the starting square of the king
                    //destCol = curCol;
                }

                // Advance to the next square
                curCol += dcol;
                //System.out.println ("cur col " + curCol);
                k++;
            }

            if (!canCastle) continue;

            // Add move
            Move move = new Move(startCol, startRow, destCol, startRow);
            move.setCastle(true);
            validMoves.add(move);

        }

        //System.out.println ("Castling moves: " + validMoves);
        return validMoves;
    }

    /* Checks if the king is in check.
     * This method assumes no knowledge of other pieces, and ask all opponent pieces if they are attacking this location.*/
    public boolean inCheckOld() {
        int opponentColor = Helper.getOpponentColor(getColor());
        PieceImplementation[][] pieces = getBoard().getPiecesOnBoard(opponentColor);
        int kingRow = getRow();
        int kingCol = getCol();
        Board board = getBoard();

        for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
            int pieceCount = board.getPieceCount(opponentColor, i);
            for (int j = 0; j < pieceCount; j++) {
                PieceImplementation opponentPiece = pieces[i][j];
                if (opponentPiece == null) continue;

                // If opponent can attack, then return true
                if (opponentPiece.canAttackLocation(kingCol, kingRow)) {
                    return true;
                }
            }
        }
        return false;
    }

    /* Checks if the king is in check in four player game.
     * This method assumes no knowledge of other pieces,
     * and ask all opponent pieces if they are attacking this location.*/
    public boolean inCheckAll() {

        int kingRow = getRow();
        int kingCol = getCol();
        Board board = getBoard();

        int color = getColor();
        int opponentColor = Helper.getOpponentColor(getColor());
        for (; opponentColor != color;
             opponentColor = Helper.getOpponentColor(opponentColor)) {
            PieceImplementation[][] pieces = getBoard().getPiecesOnBoard(opponentColor);
            for (int i = PieceImplementation.FIRSTPIECE; i <= PieceImplementation.LASTPIECE; i++) {
                int pieceCount = board.getPieceCount(opponentColor, i);
                for (int j = 0; j < pieceCount; j++) {
                    PieceImplementation opponentPiece = pieces[i][j];
                    if (opponentPiece == null) continue;

                    //  	System.out.println ("Checking king check with piece:");
                    //  	System.out.println (opponentPiece);
                    // 	System.out.println ("King is at " + kingCol + " " + kingRow);

                    // If opponent can attack, then return true
                    if (opponentPiece.canAttackLocation(kingCol, kingRow)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Overrides makeOtherMove in Piece. Implements castling
     **/
    public void makeOtherMove(Move move) {

        // To make castling move, swap King and Empty, Rook and Empty
        // For undo, keep piece info for King and Rook
        // To undo, swap King and Empty, Rook and Empty, restore info

        Board board = getBoard();
        if (!move.getCastle())
            return;

        int[] rookRows; // Rooks' starting position
        int[] rookCols = {Board.getMINCOL(), Board.getMAXCOL()};
        int[] rookDestCols = {3, 5};
        int[] myRow;
        if (getColor() == Sides.WHITE) {
            // WHITE
            myRow = new int[]{Board.getMINROW(), Board.getMINROW()};
        } else {
            myRow = new int[]{Board.getMAXROW(), Board.getMAXROW()};
        }
        rookRows = myRow;

        int startCol = move.getStartCol();
        int destRow = move.getDestRow();
        int destCol = move.getDestCol();

        int side = startCol - destCol;
        if (side > 0) // King moves left
            side = 0;
        else
            side = 1; // King move right

        int rookRow = rookRows[side];
        int rookCol = rookCols[side];
        int rookDestCol = rookDestCols[side];
        PieceImplementation rookPiece = board.getPieceAt(rookCol, rookRow);

        // Keep track of backup information for undo move
        // 1. Store last pieces info
        board.setLastPieces(move);

        // 2. Store piece info for king and rook
        move.storePieceInfo(this, 0);
        move.storePieceInfo(rookPiece, 1);

        // 3. Swap rook
        rookPiece.swapPieces(rookDestCol, rookRow);
        // System.out.println ("Rook dest : " + rookDestCol + " " + rookRow);

        // 4. Swap king
        swapPieces(destCol, destRow);

        // make rook last piece as well
        board.setLastPieceMoved(this, rookPiece, getColor());

    }

    /**
     * Overrides undoOtherMove in Piece. Implements undo castling
     **/
    public void undoOtherMove(Move move) {

        // To make castling move, swap King and Empty, Rook and Empty
        // For undo, keep piece info for King and Rook
        // To undo, swap King and Empty, Rook and Empty, restore info

        Board board = getBoard();

        int[] rookRows; // Rooks' starting position
        int[] rookCols = {Board.getMINCOL(), Board.getMAXCOL()};
        int[] rookDestCols = {3, 5};
        int[] myRow;
        if (getColor() == Sides.WHITE) {
            // WHITE
            myRow = new int[]{Board.getMINROW(), Board.getMINROW()};
        } else {
            myRow = new int[]{Board.getMAXROW(), Board.getMAXROW()};
        }
        rookRows = myRow;

        int startRow = move.getStartRow();
        int startCol = move.getStartCol();
        int destCol = move.getDestCol();

        int side = startCol - destCol;
        if (side > 0) // King moves left
            side = 0;
        else
            side = 1; // King move right

        int rookRow = rookRows[side];
        int rookCol = rookCols[side];
        int rookDestCol = rookDestCols[side];
        PieceImplementation rookPiece = board.getPieceAt(rookDestCol, rookRow);

        // 4. Swap king
        swapPieces(startCol, startRow);

        // 3. Swap rook
        rookPiece.swapPieces(rookCol, rookRow);
        // System.out.println ("Rook dest : " + rookDestCol + " " + rookRow);

        // 2. Restore piece info for king and rook
        move.restorePieceInfo(this, 0);
        move.restorePieceInfo(rookPiece, 1);

        // 1. restore last piece info
        board.restoreLastPieces(move);

    }

    /* Checks if the king is in check.
     * This method assumes knowledge of how other pieces move, and
     * traces from its location to other positions.
     **/
    public boolean inCheck() {

        // if board is 4player, we cannot use this, so use old checking
        if (getBoard().getBoardType() == BoardType.FOURPLAYERS) {
            return inCheckAll();
        }

        Board board = getBoard();
        int startRow = getRow();
        int startCol = getCol();
        int color = getColor();

        int destRow;
        int destCol;

        int i;
        PieceImplementation piece;

        // iterate through all possible directions
        int cOffsets = 16;
        for (i = 0; i < cOffsets; i++) {

            // reset the destination row and column to the starting location of the piece
            destRow = startRow;
            destCol = startCol;

            int slide = 0;
            for (; ; ) {

                // offset the destination row and column using the offset vector
                destRow += cDRow[i];
                destCol += cDCol[i];

                if (board.getBoardType() == BoardType.CYLINDER) {
                    // Cylinder board: we need to wrap around columns

                    if (destCol < Board.getMINCOL())
                        destCol += Board.getNUMCOLS();
                    if (destCol > Board.getMAXCOL())
                        destCol -= Board.getNUMCOLS();

                    if (destRow > Board.getMAXROW() ||
                            destRow < Board.getMINROW() ||
                            (destCol == startCol && destRow == startRow))
                        break;
                } else {
                    if (destRow > Board.getMAXROW() ||
                            destRow < Board.getMINROW() ||
                            destCol > Board.getMAXCOL() ||
                            destCol < Board.getMINCOL())
                        break;
                }
                piece = board.getPieceAt(destCol, destRow);
                // check if the square is valid
                if (piece == null)
                    continue;

                PieceType opponentType = piece.getType();
                // Check if a piece is on the destination location
                if (opponentType != PieceType.EMPTY) {
                    // location has a piece
                    // Get the piece

                    // Check if the piece is the same color
                    if (piece.getColor() == color) {
                        // If same color, we are blocked.
                        // Done with this direction as we are blocked

                    } else {
                        // If different color, we check if the opponent piece is of the type
                        // which can attack our king
                        for (int k = 0; k < cPieceCount[color][i]; k++) {
                            if (opponentType == cPieceType[i][k] && slide <= cSlide[i][k])
                                return true;
                        }
                    }
                    // Since this location has a piece, we can either be checked by it if
                    // or we are shielded. So we need not check further in this direction.
                    break;

                } else {
                    // do nothing here as location is empty
                }
                slide = 1; // more than one square
                if (i == 8) break; // skip slide check for knight
            }
        }

        return false;
    }
}
