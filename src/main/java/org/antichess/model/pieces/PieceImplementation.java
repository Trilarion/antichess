/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model.pieces;

import org.antichess.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Piece represents a piece in the org.antichess game.
 *
 * last-piece-moved: boolean // whether a
 * has-moved: boolean
 * last-pos: [row, col]
 * cur-pos: [row, col]
 * piece-type: King | Knight | Bishop | Queen | Pawn | Rook
 * color: Black | White
 * board: Board // the board the piece is on
 */
public abstract class PieceImplementation implements Piece { // TODO try to do less here and move some code elsewhere

    /**
     * Constant for first color index
     */
    public static final int FIRSTCOLOR = Sides.WHITE;
    public static final int FIRSTPIECE = PieceType.PAWN.getValue();
    /**
     * Constant value for the last piece index
     **/
    public static final int LASTPIECE = PieceType.KING.getValue();
    /**
     * Constant value for the number of pieces
     **/
    public static final int PIECECOUNT = LASTPIECE - FIRSTPIECE + 1;
    /**
     * Strings for the player color
     **/
    public static final String[] playerColorString = {"White", "Black", "Red", "Purple"};
    /**
     * Constant for last color index
     */

    public static int LASTCOLOR = Sides.BLACK;  // This assumes two player game

    private final PieceType type;       // pieceType of the piece
    private final char letter;    // letter representation of the piece // TODO do we need the letter representation of the Piece, should it be hardcoded in derived (might be used for save format)
    private final Board board;               //Board that the piece is on
    private final int color;      // the color of the piece
    private boolean slide;
    private int offsets;
    private int[] dcol;
    private int[] drow;
    private int col; //Current col of the piece
    private int row; //Current row of the piece
    private int last_col;    //Last col of the piece
    private int last_row;    //Last row of the piece
    private boolean moved; // if the piece has moved

    /**
     * Takes in a piece type and board and assigns the current piece to it.
     *
     * @param type   piecetype
     * @param letter character representing piece
     * @param board  board the piece is on
     * @param row    starting row of piece
     * @param col    starting row of piece
     * @param color  color
     * @requires {@code type = {Piece.PAWN | Piece.BISHOP | Piece.KNIGHT | Piece.ROOK | Piece.QUEEN |Piece.KING|Piece.EMPTY } && letter = {P | p | K | k | N | n | R | r | Q | q | B | b} && MINROW <=row <=MAXROW && MINCOL<= col <=MAXCOL}
     *
     * <b>NOTE: has to be taken using row and col.
     * They start from MINROW, which is 0 generally)</b>
     **/
    protected PieceImplementation(PieceType type, char letter, Board board, int col, int row, int color) {
        this.type = type;
        this.letter = letter;
        this.board = board;
        this.col = col;
        this.row = row;
        last_col = col;
        last_row = row;
        this.color = color;

        if (board != null)
            board.registerPiece(col, row, this);
    }


    /**
     * To check that a piece is not null, not empty, and of a required color
     **/
    public boolean nonEmptyPiece(int color) {
        if (type == PieceType.EMPTY)
            return false;

        return this.color == color;
    }

    /**
     * @return an integer representing the location of the piece on a 8by8 chess board. This value
     * is consistent with the 64 integer arrays that are used for piece evaluations. Refer to multiplication
     * arrays above. They are written how a 2D chess-board looks like. The first 0-7 entries represent
     * squares from a8 to h8, the next 8 entries from a7 to h7 and so on.
     * a8 is assigned to 0, h1 is assigned to 63.
     **/
    public int returnSquareValue() {
        return col + (7 - row) * 8;
    }

    // MOVE GENERATION / CHECKING METHODS

    /**
     * Gets if the piece has been moved
     *
     * @return true if the piece has been moved
     */
    public boolean getMoved() {
        return moved;
    }

    /**
     * Set if the piece has moved
     *
     * @modifies this
     * @effects sets has-moved as bool
     */
    public void setMoved(boolean bool) {
        moved = bool;
    }

    /**
     * Gets the last location column of the piece
     * See the game file format for more details about this field.
     *
     * @return a column
     **/
    public int getLastCol() {

        return last_col;
    }

    /**
     * Gets the last location row of the piece
     * See the game file format for more details about this field.
     *
     * @return a row
     **/
    public int getLastRow() {

        return last_row;
    }

    /**
     * Gets the current column of the piece
     *
     * @return a column
     **/
    public int getCol() {
        return col;
    }

    /**
     * Gets the current row of the piece
     *
     * @return a row
     **/
    public int getRow() {
        return row;
    }

    /**
     * Gets the color of the piece
     *
     * @return {Piece.WHITE | Piece.BLACK}
     */
    public int getColor() {
        return color;
    }

    /**
     * Gets the type of the piece
     *
     * @return {Piece.PAWN | Piece.BISHOP | Piece.KNIGHT |
     * Piece.ROOK | Piece.QUEEN |Piece.KING | Piece.EMPTY }
     */
    public PieceType getType() {
        return type;
    }

    /**
     * Gets the character representing the piece
     *
     * @return letter representation corresponding to the pieceType
     **/
    public char getChar() {
        return letter;
    }

    /**
     * Gets the board the Piece is on
     *
     * @return board the piece is on
     **/
    public Board getBoard() {
        return board;
    }

    /**
     * String representation
     **/
    public String toString() {
        PieceImplementation piece = this;
        return ("piece-info: " + piece.letter + "\n" + "currow: " + piece.row + " " + "curcol: " + piece.col + "\n" + "lastrow: " + piece.last_row + " " + "lastcol: " + piece.last_col + "\n" + "moved: " + piece.moved + " ");

    }

    // MAKE MOVE METHODS

    /**
     * Sets the location of a piece
     *
     * @requires a {@code location | 1 <= row, col <= Board.numberOfRows/Cols}
     * @modifies this
     * @effects sets the location of a piece
     **/
    public void setLocation(int col, int row) {
        this.col = col;
        this.row = row;

    }

    /**
     * Sets the last location of a piece
     *
     * @requires a {@code location | 1 <= row, col <= Board.numberOfRows/Cols && moved == true}
     * @modifies this
     * @effects sets the location of a piece
     **/
    public void setLastLocation(int col, int row) {
        last_row = row;
        last_col = col;

    }

    /**
     * Checks for equality. <br>
     * Overrides Object.equals
     *
     * @requires o instanceof Piece
     * @return true if object is same as this
     **/
    public boolean equals(Object o) {
        if (o == null) return false;
        if (o instanceof PieceImplementation)
            return equals((PieceImplementation) o);
        else
            return false;
    }

    public boolean equals(PieceImplementation other) {
        if (other == null) return false;
        return (other.type == type
                && other.color == color
                && other.moved == moved
                && other.row == row
                && other.col == col
                && other.last_row == last_row
                && other.last_col == last_col
                && other.board == board);
    }

    /**
     * Find all possible (not necessarily legal) Moves a particular piece can make.
     * The Moves are not necessarily legal.
     *
     * @param captureMoves    the possible captureMoves will be stored here
     * @param nonCaptureMoves the possible nonCaptureMoves will be stored here
     * @requires non null
     * @effects captureMoves and nonCaptureMoves will contain the respective moves
     **/
    public void getPossibleMoves(List<Move> captureMoves, List<Move> nonCaptureMoves) {
        getNormalMoves(captureMoves, nonCaptureMoves);
        nonCaptureMoves.addAll(getOtherMoves());

    }

    /**
     * To be overridden by subclasses if any other special moves are
     * to be made
     *
     * @return empty List
     **/
    public List<Move> getOtherMoves() {
        return new ArrayList<>();
    }

    /**
     * Gets possible normal (includes capture and noncapture,
     * but not necessarily legal) moves.
     * The Moves are not necessarily legal.
     *
     * @param nonCaptureMoves = List of nonCaptureMoves found
     * @param captureMoves    = List of captureMoves found
     * @requires non null
     * @modifies moves, captureMoves
     * @effects nonCaptureMoves will contain list of non-capture moves.
     * and captureMoves will contain a list of capture moves
     **/
    public void getNormalMoves(List<Move> captureMoves, List<Move> nonCaptureMoves) {
        // This method makes use of the direction vectors dcol and drow.
        // The number of directions a piece can move is stored in the variable offsets.
        // Whether a piece can move more than one step at a time is indicated by the variable slide.
        // For each direction, we offset the starting location by the vector,
        // and check if that location is empty.
        // If the location is empty, then this is a nonCaptureMove.
        // If the location has a piece of same color, we are blocked, and done for this direction.
        // If the location has a piece of another color, we can capture and this is a captureMove.
        // If the piece can slide, we will repeat until we encounter another piece in the direction.
        // Otherwise, we only need to check one step.

        Board board1 = board;
        int startRow = row;
        int startCol = col;
        int color = this.color;

        int destRow;
        int destCol;

        int i;
        PieceImplementation piece;
        Move move;

        // iterate through all possible directions
        for (i = 0; i < offsets; i++) {

            // reset the destination row and column to the starting location of the piece
            destRow = startRow;
            destCol = startCol;

            for (; ; ) {

                // offset the destination row and column using the offset vector
                destRow += getDrow()[i];
                destCol += getDcol()[i];

                if (board1.getBoardType() == BoardType.CYLINDER) {
                    // Cylinder board: we need to wrap around columns

                    if (destCol < Board.getMINCOL())
                        destCol += Board.getNUMCOLS();
                    if (destCol > Board.getMAXCOL())
                        destCol -= Board.getNUMCOLS();

                    if (destRow > Board.getMAXROW() ||
                            destRow < Board.getMINROW() ||
                            (destCol == startCol && destRow == startRow))
                        break;
                } else {
                    if (destRow > Board.getMAXROW() ||
                            destRow < Board.getMINROW() ||
                            destCol > Board.getMAXCOL() ||
                            destCol < Board.getMINCOL())
                        break;
                }

                piece = board1.getPieceAt(destCol, destRow);
                // check if the square is valid
                if (piece == null) {
                    // continue;
                    break;
                }

                // Check if a piece is on the destination location
                if (piece.type != PieceType.EMPTY) {
                    // location has a piece
                    // Get the piece

                    // Check if the piece is the same color
                    if (piece.color == color) {
                        // If same color, we are blocked.
                        // Done with this direction as we are blocked

                    } else {
                        // If different color, we can capture.
                        // Add move to captureMoves
                        move = new Move(startCol, startRow, destCol, destRow);
                        move.setCapture(true);
                        captureMoves.add(move);
                    }
                    // Since this location has a piece, we can either capture it if
                    // it is of a different color, or we are blocked if it is the
                    // same color. So we need not check further in this direction.
                    break;

                } else {

                    // location is empty
                    // Add move to nonCaptureMoves
                    move = new Move(startCol, startRow, destCol, destRow);
                    nonCaptureMoves.add(move);
                }

                // see if multiple steps are allowed
                if (!slide) {
                    break; // can only move one step, so exit
                }

            }
        }

    }

    /**
     * Checks if a Piece can attack a particular location
     *
     * @return true if can attack, false otherwise. If the location contains
     * a friendly piece, returns false.
     **/
    public boolean canAttackLocation(int targetCol, int targetRow) {
        Board board1 = board;
        int startRow = row;
        int startCol = col;
        int color = this.color;

        int destRow;
        int destCol;
        int i;

        // iterate through all possible directions
        for (i = 0; i < offsets; i++) {

            // reset the destination row and column to the starting location of the piece
            destRow = startRow;
            destCol = startCol;

            for (; ; ) {

                // offset the destination row and column using the offset vector
                destRow += getDrow()[i];
                destCol += getDcol()[i];

                if (board1.getBoardType() == BoardType.CYLINDER) {
                    // Cylinder board: we need to wrap around columns

                    if (destCol < Board.getMINCOL())
                        destCol += Board.getNUMCOLS();
                    if (destCol > Board.getMAXCOL())
                        destCol -= Board.getNUMCOLS();

                    if (destRow > Board.getMAXROW() ||
                            destRow < Board.getMINROW() ||
                            (destCol == startCol && destRow == startRow))
                        break;
                } else {
                    if (destRow > Board.getMAXROW() ||
                            destRow < Board.getMINROW() ||
                            destCol > Board.getMAXCOL() ||
                            destCol < Board.getMINCOL())
                        break;
                }

                PieceImplementation piece = board1.getPieceAt(destCol, destRow);
                // check if the square is valid
                if (piece == null) {
                    //continue;
                    break;
                }
                // Check if a piece is on the destination location
                if (piece.type != PieceType.EMPTY) {
                    // location has a piece
                    // Get the piece

                    // Check if the piece is the same color
                    if (piece.color == color) {
                        // If same color, we are blocked.
                        // Done with this direction as we are blocked

                    } else {
                        // If different color, we can capture.
                        // Check if it is the target location
                        if (destRow == targetRow && destCol == targetCol)
                            return true;

                    }
                    // Since this location has a piece, we can either capture it if
                    // it is of a different color, or we are blocked if it is the
                    // same color. So we need not check further in this direction.
                    break;

                } else {

                    // location is empty
                    // We can attack this location
                    if (destRow == targetRow && destCol == targetCol)
                        return true;
                }

                // see if multiple steps are allowed
                if (!slide) {
                    break; // can only move one step, so exit
                }

            }
        }
        return false;

    }

    /**
     * Makes a move, without recomputing the validMoves.
     *
     * @modifies this and this.board
     * @effects this.board is updated
     **/
    public void makeMove(Move move1, List<Move> validMoves) {
        int index = validMoves.indexOf(move1);
        // Not a valid move
        if (index == -1)
            return;

        Move move = validMoves.get(index);
        move1.setInfo(move.getInfo());
        makeGeneratedMove(move1);
    }

    /**
     * Makes a computer generated move.
     * This move must be generated from getValidMoves or one of the
     * methods in Piece, and move has information about what kind of move it is.
     * The move from a human player should be passed into makeMove.
     *
     * @requires move is a Piece.method generated move
     * @modifies this and this.board
     * @effects this.board is updated
     **/
    public void makeGeneratedMove(Move move) {
        if (move.isNormal())
            makeNormalMove(move);
        else if (move.isSpecial())
            makeSwitchMove(move);
        else if (move.isCapture())
            makeCaptureMove(move);
        else
            makeOtherMove(move);
    }

    /**
     * Undos a move that is unique to a particular piece.
     * To be overridden by subclasses
     **/
    public void undoOtherMove(Move move) {
        System.out.println("move: " + move + "\n" + move.getInfo());
        System.out.println("Board:\n" + board);
        System.exit(0);
        throw new RuntimeException("Piece.undoOtherMove should be overridden by subclass");
    }

    /**
     * Makes a normal move
     *
     * @modifies this and this.board && dummy piece
     * @effects this.board is updated
     **/
    private void makeNormalMove(Move move) {
        // Make normal move simply swaps two pieces
        // For undo, we need to keep info for first piece
        // To undo, then swap them back, and restore first piece info

        //System.out.println ("Making normal move ...");

        // Keep track of backup information for undo move
        // 1. Store last pieces info
        board.setLastPieces(move);

        // 2. Piece info
        move.storePieceInfo(this, 0);

        // 3. Make the swap
        swapPieces(move.getDestCol(), move.getDestRow());
    }

    /**
     * Undos a normal move
     *
     * @modifies this.board and pieces
     **/
    public void undoNormalMove(Move move) {
        int startCol = move.getStartCol();
        int startRow = move.getStartRow();

        // 3. Swap back
        swapPieces(startCol, startRow);

        // 2. restore piece info
        move.restorePieceInfo(this, 0);

        // 1. restore last piece info
        board.restoreLastPieces(move);

    }

    /**
     * Makes a switch move
     *
     * @modifies this and this.board
     * @effects this.board is updated
     **/
    private void makeSwitchMove(Move move) {
        // Make switch move simply swaps two pieces
        // For undo, keep piece info for both pieces
        // To undo, swap them back, and restore piece info

        //System.out.println ("Making switch move ...");

        int destCol = move.getDestCol();
        int destRow = move.getDestRow();

        // Keep track of backup information for undo move
        // 1. Store last pieces info
        board.setLastPieces(move);

        // 2. First piece
        move.storePieceInfo(this, 0);

        // Second piece
        PieceImplementation piece = board.getPieceAt(destCol, destRow);
        move.storePieceInfo(piece, 1);

        // 3. Make the swap
        swapPieces(destCol, destRow);

        // 4. Set switched to be true on board
        board.setSpecial(color, true);
    }

    // NORMAL CHESS

    /**
     * Undo a switch move
     *
     * @modifies this.board and pieces
     **/
    public void undoSwitchMove(Move move) {
        int startCol = move.getStartCol();
        int startRow = move.getStartRow();
        int destRow = move.getDestRow();
        int destCol = move.getDestCol();

        // 4. Set switched to be false
        board.setSpecial(color, false);

        // 3. Make the swap back
        swapPieces(startCol, startRow);

        // 2. Restore piece info for first piece
        move.restorePieceInfo(this, 0);

        // Second piece
        PieceImplementation piece = board.getPieceAt(destCol, destRow);
        move.restorePieceInfo(piece, 1);

        // 1. restore last piece info
        board.restoreLastPieces(move);
    }

    /**
     * Makes a capture move
     *
     * @modifies this and this.board
     * @effects this.board is updated
     **/
    public void makeCaptureMove(Move move) {
        // To make a capture move, create Empty over Capture, then swap
        // For undo, keep reference to captured piece,
        // and piece-info of capturing piece.
        // To undo, swap Empty and Capturing back, register the Captured over Empty,
        // restore info for Capturing piece.

        //System.out.println ("Making capture move ...");

        Board board1 = board;

        int destRow = move.getDestRow();
        int destCol = move.getDestCol();

        // Keep track of backup information for undo move
        // 1. Store last pieces info
        board1.setLastPieces(move);

        // 2. First piece info
        move.storePieceInfo(this, 0);

        // 3. Capture piece Reference
        move.modifiedPieces[0] = board1.getPieceAt(destCol, destRow);

        // Creates a dummy piece at the location being captured
        PieceImplementation dummy;
        //System.out.println ("Creating empty piece");

        dummy = new Empty(board1, destCol, destRow);

        // Sets the dummy piece to be at the target location
        //System.out.println ("Setting location of emptypiece");
        board.setPieceAt(destCol, destRow, dummy);
        dummy.setLocation(destCol, destRow);

        // Zobrist delete captured piece
        Zobrist z = board1.getZobrist();
        if (type != PieceType.EMPTY)
            z.deletePiece(board1, type, color, col, row);

        // 4. Swap current piece with dummy
        swapPieces(destCol, destRow);
    }

    /**
     * Undos a capture move
     *
     * @modifies this, this.board and other pieces on board
     **/
    public void undoCaptureMove(Move move) {
        int startCol = move.getStartCol();
        int startRow = move.getStartRow();
        int destRow = move.getDestRow();
        int destCol = move.getDestCol();
        Board board = this.board;
        PieceImplementation captured = move.modifiedPieces[0];

        // 4. Swap back
        swapPieces(startCol, startRow);

        // 3. Register captured piece
        //b.registerPiece (destCol, destRow, move.modifiedPieces[0]);
        board.registerPiece(destCol, destRow, captured);

        // 2. Restore piece info for capturing piece
        move.restorePieceInfo(this, 0);

        // 1. restore last piece info
        this.board.restoreLastPieces(move);

        // Zobrist add back captured piece
        Zobrist z = board.getZobrist();
        if (captured.type != PieceType.EMPTY)
            z.addPiece(board, captured.type, captured.color, captured.col, captured.row);
    }

    /**
     * Swaps this piece with another piece at another location
     *
     * @modifies this, this.board and dummy piece
     * @effects this.board is updated. this.lastPieceMoved is updated to this.
     * piece info for involved pieces are changed as well.
     **/
    protected void swapPieces(int destCol, int destRow) {
        // Swap position with the other piece at destination

        Board board = this.board;

        // Zobrist
        Zobrist z = board.getZobrist();
        if (z == null)
            throw new RuntimeException("Zobrist should not be null");

        int startRow = row;
        int startCol = col;

        // swap pieces at (startcol/row) to (destcol/row)
        PieceImplementation startPiece, otherPiece;
        //startPiece = b.getPieceAt (startCol, startRow);
        startPiece = this;
        otherPiece = board.getPieceAt(destCol, destRow);

        // If either pieces are null, do nothing
        if (startPiece == null ||
                otherPiece == null)
            return;

        // Zobrist delete pieces from original position
        if (type != PieceType.EMPTY)
            z.deletePiece(board, type, color, col, row);
        if (otherPiece.type != PieceType.EMPTY)
            z.deletePiece(board, otherPiece.type, otherPiece.color, otherPiece.col, otherPiece.row);

        // Put the pieces into the new positions
        board.setPieceAt(destCol, destRow, startPiece);
        board.setPieceAt(startCol, startRow, otherPiece);

        // Modify the piece info
        startPiece.setLastLocation(startCol, startRow);
        startPiece.setLocation(destCol, destRow);
        startPiece.moved = true;

        otherPiece.setLastLocation(destCol, destRow);
        otherPiece.setLocation(startCol, startRow);
        otherPiece.moved = true;

        board.setLastPieceMoved(this);
        if (otherPiece.type != PieceType.EMPTY) {
            board.setLastPieceMoved(this, otherPiece.color, 1);
        }

        // Zobrist add pieces to new positions
        if (type != PieceType.EMPTY)
            z.addPiece(board, type, color, col, row);
        if (otherPiece.type != PieceType.EMPTY)
            z.addPiece(board, otherPiece.type, otherPiece.color, otherPiece.col, otherPiece.row);

    }

    /**
     * whether multiple steps are allowed
     */
    public boolean isSlide() {
        return slide;
    }

    public void setSlide(boolean slide) {
        this.slide = slide;
    }

    /**
     * number of directions the piece can move
     */
    public int getOffsets() {
        return offsets;
    }

    public void setOffsets(int offsets) {
        this.offsets = offsets;
    }

    /**
     * col vector offset
     **/
    public int[] getDcol() {
        return dcol;
    }

    public void setDcol(int[] dcol) {
        this.dcol = dcol;
    }

    /**
     * row vector offset
     **/
    public int[] getDrow() {
        return drow;
    }

    public void setDrow(int[] drow) {
        this.drow = drow;
    }
}

