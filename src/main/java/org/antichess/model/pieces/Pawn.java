/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model.pieces;

import org.antichess.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * A Pawn piece in org.antichess.
 **/
public class Pawn extends PieceImplementation {

    /**
     * Creates a new piece.
     *
     * @param letter the character representation of the piece:
     * @param board  the board the piece will be assigned to
     * @param row    starting row of piece
     * @param col    starting col of piece
     * @param color  color
     * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
     **/
    public Pawn(char letter, Board board, int col, int row, int color) { // TODO can letter be deducted from the color ('P' if color is white and 'p' otherwise
        super(PieceType.PAWN, letter, board, col, row, color);

        // These vectors are used in canAttackLocation
        if (color == Sides.WHITE) {
            int[] myDCol = {-1, 1};
            int[] myDRow = {1, 1};
            setDcol(myDCol);
            setDrow(myDRow);
        } else if (color == Sides.BLACK) {
            int[] myDCol = {-1, 1};
            int[] myDRow = {-1, -1};
            setDcol(myDCol);
            setDrow(myDRow);
        } else if (color == Sides.RED) {
            int[] myDCol = {1, 1};
            int[] myDRow = {-1, 1};
            setDcol(myDCol);
            setDrow(myDRow);
        } else if (color == Sides.BLUE) {
            int[] myDCol = {-1, -1};
            int[] myDRow = {-1, 1};
            setDcol(myDCol);
            setDrow(myDRow);

        }
        setSlide(false);
        setOffsets(2);
    }

    /**
     * Creates a copy to a new board
     */
    public PieceImplementation copyPiece(Board board) {
        //System.out.println ("Pawn info: " + this);
        PieceImplementation piece = new Pawn(getChar(), board, getCol(), getRow(), getColor());
        piece.setLastLocation(getLastCol(), getLastRow());
        piece.setMoved(getMoved());
        return piece;
    }

    /**
     * Overrides Piece.getNormalMoves
     **/
    public void getNormalMoves(List<Move> captureMoves, List<Move> nonCaptureMoves) {
        captureMoves.addAll(getCaptureMoves());
        nonCaptureMoves.addAll(getNonCaptureMoves());
    }

    /**
     * Returns a list of possible noncapture (not necessarily legal) moves for a Pawn
     * The Moves are not necessarily legal.
     * Calling code should use inCheck after making a move. <br>
     *
     * @return list of moves the Pawn can make.
     **/
    private List<Move> getNonCaptureMoves() {
        // This checks for the two possible nonCaptureMove a Pawn can make.
        // It can move one step forward if it is not blocked.
        // If it has not moved, and is not blocked, it can move two steps.
        Board board = getBoard();
        int startRow = getRow();
        int startCol = getCol();
        int color;

        int destRow;
        int destCol;

        int i;

        int forwardRow = 0, forwardCol = 0;
        Move move;
        List<Move> nonCaptureMoves = new ArrayList<>();

        color = getColor();

        // Check for color and assign appropriate offsets
        if (color == Sides.WHITE) {
            // White. Pawn moves up.
            forwardRow = 1;
            forwardCol = 0;
        } else if (color == Sides.BLACK) {
            // Black. Pawn moves down.
            forwardRow = -1;
            forwardCol = 0;
        } else if (color == Sides.RED) {
            forwardRow = 0;
            forwardCol = 1;
        } else if (color == Sides.BLUE) {
            forwardRow = 0;
            forwardCol = -1;
        }

        // Check one step and two steps
        destRow = startRow;
        destCol = startCol;
        for (i = 0; i < 2; i++) {

            destRow += forwardRow;
            destCol += forwardCol;

            if (board.getBoardType() == BoardType.CYLINDER) {
                // Cylinder board: we need to wrap around columns
                if (destCol < Board.getMINCOL())
                    destCol += Board.getNUMCOLS();
                if (destCol > Board.getMAXCOL())
                    destCol -= Board.getNUMCOLS();
                if (destRow > Board.getMAXROW() ||
                        destRow < Board.getMINROW() ||
                        (destCol == startCol && destRow == startRow))
                    break;
            } else {
                if (destRow > Board.getMAXROW() ||
                        destRow < Board.getMINROW() ||
                        destCol > Board.getMAXCOL() ||
                        destCol < Board.getMINCOL())
                    break;
            }
            PieceImplementation destPiece = board.getPieceAt(destCol, destRow);

            // Check if the destination square is within bounds
            if (destPiece == null)
                break;

            // Check if the destination square is empty
            if (destPiece.getType() != PieceType.EMPTY) {
                // If first square is not empty, then we are block
                // and need not check second square
                break;
            }

            // Add move to captureMoves
            move = new Move(startCol, startRow, destCol, destRow);

            // Check if a promotion
            if ((color == Sides.WHITE && destRow == Board.getMAXROW()) ||
                    (color == Sides.BLACK && destRow == Board.getMINROW()))
                move.setPromote(true);
            // m.setPawn(true); // Let super handle normal move instead

            nonCaptureMoves.add(move);

            // Check if the Pawn has moved before. If so, it cannot move 2 steps
            if (getMoved())
                break;
        }

        return nonCaptureMoves;
    }

    /**
     * Returns a list of possible capture (not necessarily legal) moves for a Pawn
     * The Moves are not necessarily legal.
     * Calling code should use inCheck after making a move. <br>
     *
     * @return list of possible capture moves a Pawn can make
     **/
    private List<Move> getCaptureMoves() {
        // This checks the forward diagonal to see if the Pawn has anything
        // to capture. It also checks for the special capture move of
        // en passant (see below).
        Board board = getBoard();
        int startRow = getRow();
        int startCol = getCol();
        int color = getColor();

        int destRow;
        int destCol;

        int i;
        int opponentStartRow; // for en passant

        // Offset vectors
        int captureOffsets = 2;
        int[] captureRowOffset = {0, 0};
        int[] captureColOffset = {1, -1};

        Move move;
        int epRow, epCol; // en passant square where opponent pawn is
        int prevRow, prevCol; // previous position of opponent pawn
        PieceImplementation epPiece, destPiece;

        List<Move> captureMoves = new ArrayList<>();

        // check for color and assign appropriate offsets
        if (color == Sides.WHITE) {
            // White. Pawn moves up.
            int[] offset = {1, 1};
            captureRowOffset = offset;
        } else if (color == Sides.BLACK) {
            // Black. Pawn moves down.
            int[] offset = {-1, -1};
            captureRowOffset = offset;
        } else if (color == Sides.RED) {
            int[] roffset = {1, -1};
            int[] coffset = {1, 1};
            captureRowOffset = roffset;
            captureColOffset = coffset;
        } else if (color == Sides.BLUE) {
            int[] roffset = {1, -1};
            int[] coffset = {-1, -1};
            captureRowOffset = roffset;
            captureColOffset = coffset;
        }

        // iterate through capture moves
        for (i = 0; i < captureOffsets; i++) {

            // offset the starting row and column using the offset vector to
            // get the destination row and column
            destRow = startRow + captureRowOffset[i];
            destCol = startCol + captureColOffset[i];

            if (board.getBoardType() == BoardType.CYLINDER) {
                // Cylinder board: we need to wrap around columns
                if (destCol < Board.getMINCOL())
                    destCol += Board.getNUMCOLS();
                if (destCol > Board.getMAXCOL())
                    destCol -= Board.getNUMCOLS();
                if (destRow > Board.getMAXROW() ||
                        destRow < Board.getMINROW() ||
                        (destCol == startCol && destRow == startRow))
                    continue;
            } else {
                if (destRow > Board.getMAXROW() ||
                        destRow < Board.getMINROW() ||
                        destCol > Board.getMAXCOL() ||
                        destCol < Board.getMINCOL())
                    continue;
            }

            destPiece = board.getPieceAt(destCol, destRow);
            if (destPiece == null) {
                continue;
            }

            if (destPiece.getType() != PieceType.EMPTY) {

                // Check if the piece is the same color
                if (destPiece.getColor() == color) {
                    // If same color, we are blocked.
                    // Done with this direction as we are blocked
                    // Do nothing.
                } else {
                    // If different color, we can capture.
                    // Add move to captureMoves
                    move = new Move(startCol, startRow, destCol, destRow);
                    move.setCapture(true);

                    // Check promotion
                    if ((color == Sides.WHITE && destRow == Board.getMAXROW()) ||
                            (color == Sides.BLACK && destRow == Board.getMINROW()))
                        move.setPromote(true);

                    captureMoves.add(move);
                }

                // As long as there is piece in the diagonal square,
                // that means that the opponent Pawn did not advance two steps
                // in the previous move. Hence we need not check for en passant.
                // We will proceed to the next direction.
                continue;
            }

            // Check for 4player board. IF 4player, no en passant
            if (getBoard().getBoardType() == BoardType.FOURPLAYERS) {
                continue;
            }
            // En Passant
            if (color == Sides.WHITE && startRow == 4) {
                opponentStartRow = 6;

            } else if (color == Sides.BLACK && startRow == 3) {
                opponentStartRow = 1;
            } else {
                // otherwise skip
                continue;
            }

            // epSq is the square on which the opponent Pawn is
            epRow = startRow;
            epCol = destCol;
            //System.out.println ("Getting eppiece at " + epCol + " " + epRow);

            // Check if a piece is on the ep square
            epPiece = board.getPieceAt(epCol, epRow);

            // Check location is valid
            if (epPiece == null) {
                //System.out.println ("eppiece is null");
                continue;
            }

            // Check square is not empty
            if (epPiece.getType() == PieceType.EMPTY) {
                //System.out.println ("eppiece is empty");
                continue;
            }

            // Check if that piece is the same color
            if (epPiece.getColor() == color) continue;

            // Check if it is a Pawn on the epSq
            if (epPiece.getType() != PieceType.PAWN) continue;

            // If it is an opponent Pawn, we will check that it was the last piece moved, and that
            // it moved from its original position to the square using two-step advance.
            // To do this, we check the following:
            // 1)the opponent Pawn was the last piece moved
            // 2)its last position is its original position (2nd or 7th rank)
            // 3)that last position is empty (if not, it would have been a switch)

            // Check if the opponent Pawn was the last piece moved

            PieceImplementation lastPiece = board.getLastPieceMoved(epPiece.getColor());
            if (lastPiece == null)
                continue;

            if (lastPiece.getType() == PieceType.EMPTY)
                continue;
            //System.out.println ("Last piece moved: " + lastPiece);
            //System.out.println ("EP piece: " + epPiece);

            // NEED TO GET LAST PIECE MOVED FROM BOARD

            if (!lastPiece.equals(epPiece))
                continue;

            // Get the previous square of the opponent Pawn
            prevRow = epPiece.getLastRow();
            prevCol = epPiece.getLastCol();

            // Check that the previous square is the initial starting position
            if (prevRow != opponentStartRow)
                // If it didnt come from the starting position, then en passant not possible
                continue;

            // Check that the initial position is empty
            PieceImplementation prevPiece = board.getPieceAt(prevCol, prevRow);
            // If it is not empty, then en passant is not possible, we're done
            //	continue;
            if (prevPiece.getType() != PieceType.EMPTY)
                continue;

            // Add en passant move to captureMoves
            move = new Move(startCol, startRow, destCol, destRow);
            move.setEp(true);
            captureMoves.add(move);
        }

        return captureMoves;
    }

    /**
     * Overrides Piece.makeOtherMove. Implements 2-step pass or En Passant or Promotion
     **/
    public void makeOtherMove(Move move) {
        if (move.getEp()) {
            makeEpMove(move); // EP
        } else if (move.getPromote()) {
            makePromoteMove(move); // Promote
        } else {

            throw new RuntimeException("Pawn.makeOtherMove: super should handle this");

            // Normal move for pawn
            // Make a backup in move to enable undo

//       move.resetModifiedPieceCount();
//       move.setLastPiece (this.getBoard().getLastPieceMoved(this.getColor()), this.getColor());
//       move.setLastPiece (this.getBoard().getLastPieceMoved(this.getOpponentColor()), this.getOpponentColor());
//       move.addModifiedPiece (this);
//       move.addModifiedPiece (this.getBoard().getPieceAt(move.getDestCol(), move.getDestRow()));

//       swapPieces (move.getDestCol(), move.getDestRow()); // 2-step
        }
    }

    /**
     * Overrides Piece.undoOtherMove
     **/
    public void undoOtherMove(Move move) {

        if (move.getEp()) {
            undoEpMove(move); // EP
        } else if (move.getPromote()) {
            //undoPromoteMove (move); // Promote
            throw new RuntimeException("Pawn.undoOtherMove: Queen should handle undoPromote");
        } else {
            throw new RuntimeException("Pawn.undoOtherMove: super should handle this");
        }
    }

    /**
     * Makes an en passant capture
     **/
    private void makeEpMove(Move move) {
        // To make ep move, we create Empty over Captured,
        // and swap Capturing Pawn with Destination Empty
        // For undo, we keep reference to Captured, and Capturing Pawn info
        // To undo, we swap Capturing Pawn with Empty, and register Captured Pawn,
        // and restore Capturing Pawn info
        int startRow = move.getStartRow();
        int destRow = move.getDestRow();
        int destCol = move.getDestCol();
        Board board = getBoard();

        int epRow = startRow;
        int epCol = destCol;

        // Keep track of backup information for undo move
        // 1. Store last pieces info
        getBoard().setLastPieces(move);

        // 2. Store piece info for Capturing Pawn
        move.storePieceInfo(this, 0);

        // 3. Keep reference to Captured pawn
        PieceImplementation piece = getBoard().getPieceAt(epCol, epRow);
        move.modifiedPieces[0] = piece;

        // Remove opponent pawn
        PieceImplementation emptyPiece = new Empty(board, epCol, epRow);
        board.setPieceAt(epCol, epRow, emptyPiece);
        emptyPiece.setLocation(epCol, epRow);

        // 4. Advance pawn
        swapPieces(destCol, destRow);
    }

    /**
     * Undos en passant Move
     **/
    private void undoEpMove(Move move) {
        int startRow = move.getStartRow();
        int startCol = move.getStartCol();
        int destCol = move.getDestCol();
        Board board = getBoard();

        int epRow = startRow;
        int epCol = destCol;

        // 4. Swap back pawn to original square
        swapPieces(startCol, startRow);

        // 3. Register captured pawn
        board.registerPiece(epCol, epRow, move.modifiedPieces[0]);

        // 2. Restore capturing pawn pieceinfo
        move.restorePieceInfo(this, 0);

        // 1. restore last piece info
        board.restoreLastPieces(move);

    }

    /**
     * Makes a noncapture promotion
     **/
    private void makePromoteMove(Move move) {
        // To make a noncapture promote, create Queen over Pawn,
        // and swap Queen and Pawn.
        // For undo, keep reference to Pawn.
        // To undo, swap Queen and Empty, and register Pawn .

        int startRow = move.getStartRow();
        int startCol = move.getStartCol();
        int destRow = move.getDestRow();
        int destCol = move.getDestCol();
        Board board = getBoard();
        int color = getColor();

        // Keep track of backup information for undo move
        // 1. Store last pieces info
        getBoard().setLastPieces(move);

        // 2. Keep reference to pawn
        move.modifiedPieces[0] = this;

        // Creates a new queen at start location
        PieceImplementation queenPiece = new Queen((getColor() == Sides.WHITE ? 'Q' : 'q'),
                board, startCol, startRow,
                color);

        // Zobrist delete pawn
        Zobrist z = board.getZobrist();
        z.deletePiece(board, PieceType.PAWN, color, startCol, startRow);

        // Zobrist add queen
        z.addPiece(board, PieceType.QUEEN, color, startCol, startRow);

        // 3. Make the queen go into dest location
        queenPiece.swapPieces(destCol, destRow);

    }

    /**
     * Makes a capture move and checks if it is also a promotion.
     * Overrides Piece.makeCaptureMove
     *
     * @modifies {@code this and this.board}
     * @effects this.board is updated
     **/
    public void makeCaptureMove(Move move) {
        if (move.getPromote()) {
            int startRow = move.getStartRow();
            int startCol = move.getStartCol();
            int destRow = move.getDestRow();
            int destCol = move.getDestCol();
            int color = getColor();
            Board board = getBoard();

            // To make capture promotion, create Queen over Pawn,
            // create Empty over Captured,
            // swap Empty and Queen
            // For undo, keep reference to Pawn and Captured
            // To undo, register Pawn and Captured.

            // Keep track of backup information for undo move
            // 1. Store last pieces info
            getBoard().setLastPieces(move);

            // 2. Keep reference to pawn
            move.modifiedPieces[0] = this;

            // 3. Keep reference to captured
            PieceImplementation piece = board.getPieceAt(destCol, destRow);
            move.modifiedPieces[1] = piece;

            // Remove the capture piece first
            // Creates a dummy piece at the location being captured
            PieceImplementation dummy;
            //System.out.println ("Creating empty piece");
            dummy = new Empty(board, destCol, destRow);

            // Sets the dummy piece to be at the target location
            //System.out.println ("Setting location of emptypiece");
            getBoard().setPieceAt(destCol, destRow, dummy);
            dummy.setLocation(destCol, destRow);

            // Create queen over pawn
            PieceImplementation queenPiece = new Queen((getColor() == Sides.WHITE ? 'Q' : 'q'),
                    board, startCol, startRow,
                    getColor());

            // Zobrist delete pawn
            Zobrist z = board.getZobrist();
            z.deletePiece(board, PieceType.PAWN, color, startCol, startRow);

            // Zobrist add queen
            z.addPiece(board, PieceType.QUEEN, color, startCol, startRow);

            // Zobrist delete captured
            z.deletePiece(board, piece.getType(), piece.getColor(), destCol, destRow);

            // Make the queen go into dest location
            queenPiece.swapPieces(destCol, destRow);

        } else {
            // Normal capture
            super.makeCaptureMove(move);
        }
    }

    /**
     * undos a normal pawn capture move
     * /* * Overrides Piece.undoCaptureMove.
     **/
    public void undoCaptureMove(Move move) {
        super.undoCaptureMove(move);
    }

}
