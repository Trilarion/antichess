/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model.pieces;

import org.antichess.model.Board;
import org.antichess.model.Move;

/**
 * This class contains the rules that apply to Rook.
 **/
public class Rook extends PieceImplementation {
    /**
     * Creates a new piece.
     *
     * @param letter the character representation of the piece:
     * @param board  the board the piece will be assigned to
     * @param row    starting row of piece
     * @param col    starting col of piece
     * @param color  color
     * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
     **/
    public Rook(char letter, Board board, int col, int row, int color) {
        super(PieceType.ROOK, letter, board, col, row, color);
        int[] myDCol = {-1, 1, 0, 0};
        int[] myDRow = {0, 0, -1, 1};
        setDcol(myDCol);
        setDrow(myDRow);
        setSlide(true);
        setOffsets(4);
    }

    /**
     * Creates a copy to a new board
     */
    public PieceImplementation copyPiece(Board board) {
        PieceImplementation piece = new Rook(getChar(), board, getCol(), getRow(), getColor());
        piece.setLastLocation(getLastCol(), getLastRow());
        piece.setMoved(getMoved());
        return piece;
    }

    /**
     * overrides Piece.canAttackLocation
     **/
    public boolean canAttackLocation(int targetCol, int targetRow) {
        int rowDif = Math.abs(getRow() - targetRow);
        int colDif = Math.abs(getCol() - targetCol);

        // Must be either same row or same column
        if (rowDif != 0 && colDif != 0)
            return false;
        return super.canAttackLocation(targetCol, targetRow);
    }

    @Override
    public void makeOtherMove(Move move) {
        throw new RuntimeException();
    }
}
