/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.antichess.model.pieces;

import org.antichess.model.Board;
import org.antichess.model.Move;

/**
 * Empty Piece
 **/
public class Empty extends PieceImplementation {
    public Empty(Board board, int col, int row) {
        super(PieceType.EMPTY, ' ', board, col, row, -1);
    }

    public Empty(char letter, Board board, int col, int row) {
        super(PieceType.EMPTY, letter, board, col, row, -1);
    }

    /**
     * Creates a copy to a new board
     */
    public PieceImplementation copyPiece(Board board) {
        return new Empty(getChar(), board, getCol(), getRow());
    }

    @Override
    public void makeOtherMove(Move move) {
        throw new RuntimeException();
    }

    /*Overrides piece.toString*/
    public String toString() {
        PieceImplementation piece = this;
        return ("EMPTY PIECE: " + piece.getChar() + "\n" +
                "currow: " + piece.getRow() + " " +
                "curcol: " + piece.getCol() + "\n" +
                "lastrow: " + piece.getLastRow() + " " +
                "lastcol: " + piece.getLastCol());
    }
}
