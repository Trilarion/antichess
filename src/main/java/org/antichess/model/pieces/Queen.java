/*
 * AntiChess
 * Copyright (C) 2020 The AntiChess Team
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */
package org.antichess.model.pieces;

import org.antichess.model.Board;
import org.antichess.model.BoardType;
import org.antichess.model.Move;
import org.antichess.model.Zobrist;

/**
 * This class contains the rules that apply to Queen.
 **/
public class Queen extends PieceImplementation {
    /**
     * Creates a new piece.
     *
     * @param letter the character representation of the piece:
     * @param board  the board the piece will be assigned to
     * @param row    starting row of piece
     * @param col    starting col of piece
     * @param color  color
     * @requires letter = {P | p | K | k | N | n | R | r | Q | q | B | b}
     **/
    public Queen(char letter, Board board, int col, int row, int color) {
        super(PieceType.QUEEN, letter, board, col, row, color);
        int[] myDCol = {-1, -1, -1, 0, 0, 1, 1, 1};
        int[] myDRow = {-1, 0, 1, -1, 1, -1, 0, 1};
        setDcol(myDCol);
        setDrow(myDRow);
        setSlide(true);
        setOffsets(8);
    }

    /**
     * Creates a copy to a new board
     */
    public PieceImplementation copyPiece(Board board) {
        PieceImplementation piece = new Queen(getChar(), board, getCol(), getRow(), getColor());
        piece.setLastLocation(getLastCol(), getLastRow());
        piece.setMoved(getMoved());
        return piece;
    }

    public boolean canAttackLocation(int targetCol, int targetRow) {
        // If cylinder, we cannot really calculate diagonal
        if (getBoard().getBoardType() == BoardType.CYLINDER)
            return super.canAttackLocation(targetCol, targetRow);

        int rowDif = Math.abs(getRow() - targetRow);
        int colDif = Math.abs(getCol() - targetCol);

        // Must be same diagonal, same row or same column
        if (rowDif != colDif &&
                rowDif != 0 && colDif != 0)
            return false;
        return super.canAttackLocation(targetCol, targetRow);
    }

    /**
     * Undos a queen promotion move
     **/
    public void undoOtherMove(Move move) {
        int startRow = move.getStartRow();
        int startCol = move.getStartCol();
        Board board = getBoard();
        int color = getColor();

        // 3. Swap back
        swapPieces(startCol, startRow);

        // Zobrist delete queen
        Zobrist z = board.getZobrist();
        z.deletePiece(board, PieceType.QUEEN, color, startCol, startRow);

        // Zobrist add pawn
        z.addPiece(board, PieceType.PAWN, color, startCol, startRow);

        // 2. Register pawn back
        board.registerPiece(startCol, startRow, move.modifiedPieces[0]);

        // 1. restore last piece info
        board.restoreLastPieces(move);
    }

    /* * Overrides Piece.undoCaptureMove.
     * Undos promotion capture move
     **/
    public void undoCaptureMove(Move move) {
        if (move.getPromote()) {
            int startCol = move.getStartCol();
            int startRow = move.getStartRow();
            int destRow = move.getDestRow();
            int destCol = move.getDestCol();
            int color = getColor();

            // Register back both pawn and captured

            Board board = getBoard();

            // 3. registered captured back
            PieceImplementation modifiedPiece = move.modifiedPieces[1];
            board.registerPiece(destCol, destRow, modifiedPiece);

            // 2. registered pawn back
            board.registerPiece(startCol, startRow, move.modifiedPieces[0]);

            // 1. restore last piece info
            board.restoreLastPieces(move);

            // Zobrist add pawn
            Zobrist z = board.getZobrist();
            z.addPiece(board, PieceType.PAWN, color, startCol, startRow);

            // Zobrist delete queen
            z.deletePiece(board, PieceType.QUEEN, color, destCol, destRow);

            // Zobrist add captured
            z.addPiece(board, modifiedPiece.getType(), modifiedPiece.getColor(), destCol, destRow);
        } else {
            // Normal capture
            super.undoCaptureMove(move);
        }
    }

    @Override
    public void makeOtherMove(Move move) {
        throw new RuntimeException();
    }

}
