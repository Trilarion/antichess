# AntiChess - an org.antichess game

AntiChess is an [org.antichess of losing chess](https://en.wikipedia.org/wiki/Losing_chess) game, i.e. the goal is to lose
all own pieces or get stalemated, and you have to take your opponents pieces if you can.

The game is written in Java and the code is released under the open source [GPL-3.0 license](LICENSE.txt). It is based on an earlier version published in 2003 (see [history](HISTORY.md)). 

Currently, there is no official release.







  

